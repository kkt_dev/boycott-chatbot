﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LannpyaBot.Models
{
    public class Chatbot_Impression
    {
        public int ID { get; set; }
        public string EventTitle { get; set; }
        public Nullable<DateTime> ImpressionDate { get; set; }
        public string Language { get; set; }
        public Nullable<int> Count { get; set; }
        public string BotName { get; set; }
        public Nullable<Boolean> IsDeleted { get; set; }
        public Nullable<DateTime> Accesstime { get; set; }
    }
}