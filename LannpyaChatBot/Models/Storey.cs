﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LannpyaBot.Models
{
    public class StoreyInfo
    {
        public string BuildingType { get; set; }
        public int Storey { get; set; }
        public string StoreyName { get; set; }
    }
}