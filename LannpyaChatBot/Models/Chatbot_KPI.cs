﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LannpyaBot.Models
{
    public class Chatbot_KPI
    {
        public int ID { get; set; }
        public string MsgID { get; set; }
        public string UserName { get; set; }
        public string UserGender { get; set; }
        public string EventTitle { get; set; }
        public string EventName { get; set; }
        public string Language { get; set; }
        public Nullable<Boolean> IsEventEnd { get; set; }
        public string EventType { get; set; }
        public string BotName { get; set; }
        public Nullable<Boolean> IsDeleted { get; set; }
        public Nullable<DateTime> Accesstime { get; set; }
    }
}