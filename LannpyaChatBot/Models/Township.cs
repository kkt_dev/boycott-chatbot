﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LannpyaBot.Models
{
    public class HeroCardObj
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string PhotoUrl { get; set; }
        public string Action { get; set; }
        public string ActionUrl { get; set; }
        public string Value { get; set; }
    }
   
    
}