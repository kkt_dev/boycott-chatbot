﻿using Autofac;
using Microsoft.Bot.Builder.Dialogs.Internals;
using Microsoft.Bot.Builder.Scorables;
using Microsoft.Bot.Connector;
using LannpyaBot.Scorable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LannpyaBot.Module
{
    public class GlobalMessageHandlersModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder
               .Register(c => new MenuScorable(c.Resolve<IDialogTask>()))
               .As<IScorable<IActivity, double>>()
               .InstancePerLifetimeScope();

            builder
               .Register(c => new MenuScorableUN(c.Resolve<IDialogTask>()))
               .As<IScorable<IActivity, double>>()
               .InstancePerLifetimeScope();

            //builder
            //    .Register(c => new CancelScorable(c.Resolve<IDialogTask>()))
            //    .As<IScorable<IActivity, double>>()
            //    .InstancePerLifetimeScope();
        }
    }
}