﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using LannpyaBot.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using LannpyaBot.Models;

namespace LannpyaBot.Dialogs
{
    [Serializable]
    public class RootDialog : IDialog<object>
    {
        public async Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync);
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<object> result)
        {
            var msg = await result as Activity;
            var userid = msg.From.Id;
            var name = msg.From.Name;
            Chatbot_KPI cbkpi = new Chatbot_KPI();
            Chatbot_KPI evt = new KPIHelper().GetEvent(msg.Text);
            if (evt.EventTitle != null)
            {
                evt.Language = "English";
                evt.BotName = "Boycott";
                evt.MsgID = msg.From.Id;
                evt.UserName = msg.From.Name;
                var url = "api/LannpyaBot/UpSertKPI";
                Chatbot_KPI obj = await APIRequest<Chatbot_KPI>.Post(url, evt);
            }
            string evttitle = new KPIHelper().GetEventForImpression(msg.Text);
            if (evttitle != null)
            {
                Chatbot_Impression cbimp = new Chatbot_Impression();
                cbimp.Language = "English";
                cbimp.BotName = "Boycott";
                cbimp.EventTitle = evttitle;
                cbimp.ImpressionDate = DateTime.UtcNow.Date;
                var url2 = "api/LannpyaBot/UpSertImpression";
                Chatbot_Impression obj2 = await APIRequest<Chatbot_Impression>.Post(url2, cbimp);
            }
            //if (msg.Text.isStart_words())
            //{
            //    await context.Forward(new StartDialog(), this.ResumeAfterEngFFDialog, msg, CancellationToken.None);
            //    return;
            //}
            //else if (msg.Text.isStart_words_UN())
            //{
            //    await context.Forward(new StartDialog_UN(), this.ResumeAfterUNFFDialog, msg, CancellationToken.None);
            //    return;
            //}
            if (msg.Text.isWelcome_words() || msg.Text.isStart_words())
            {
                var reply = msg.CreateReply($"Hi {msg.From.Name}! Let's boycott military products.");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                     new HeroCard
                      {
                          Title = "You can see military businesses here :)",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/kkt/boycottlist/boycottlogo.png") },
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack,"🤔 Get started", value: $"catlist")
                          }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "catlist")
            {
                var reply = context.MakeMessage();
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                       new HeroCard
                      {
                          Title = $"Food and Beverages",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/kkt/boycottlist/Food%20And%20Drink.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "View Detail", value:$"fdlc")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Health Products and Services",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/kkt/boycottlist/Health%20and%20Beauty%20products.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "View Detail", value:$"hpservices")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Manufacturing and Retail",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/kkt/boycottlist/Manufacturing.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "View Detail", value:$"manandretail")
                    }
                      }.ToAttachment(),
                     new HeroCard
                      {
                          Title = $"Banking and Industrial",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/kkt/boycottlist/Banking%20and%20Finance.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "View Detail", value:$"bankandin")
                    }
                      }.ToAttachment(),
                     new HeroCard
                      {
                          Title = $"Entertainment/Tourism and Transport",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/kkt/boycottlist/Entertainment%20and%20Tourism.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "View Detail", value:$"enterandtran")
                    }
                      }.ToAttachment(),
                     new HeroCard
                      {
                          Title = $"Communications and Media",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/kkt/boycottlist/Communications.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "View Detail", value:$"commuandmedia")
                    }
                      }.ToAttachment(),
                     new HeroCard
                      {
                          Title = $"Construction and Trading",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/kkt/boycottlist/Construction.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "View Detail", value:$"consandtrade")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "fdlc")
            {
                var reply = context.MakeMessage();
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                       new HeroCard
                      {
                          Title = $"Food and Drink",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/kkt/boycottlist/FoodDrink.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "View Detail", value:$"https://generalchatbotweb.azurewebsites.net/BoycottList/Index?category=Food%20And%20Drink&userid=" + userid + "&name=" + name)
                    }
                      }.ToAttachment(),
                       new HeroCard
                      {
                          Title = $"Liquor and Cigarettes",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/kkt/boycottlist/Liquor.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "View Detail", value:$"https://generalchatbotweb.azurewebsites.net/BoycottList/Index?category=Liquor%20and%20Cigarettes&userid=" + userid + "&name=" + name)
                    }
                      }.ToAttachment(),
                    // new HeroCard
                    //  {
                    //      Title = $"Cigarettes",
                    //      Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/kkt/boycottlist/Cigarettes.png") },
                    //Buttons = new List<CardAction>
                    //      {
                    //    new CardAction(ActionTypes.OpenUrl, "View Detail", value:$"https://generalchatbotweb.azurewebsites.net/BoycottList/Index?category=Cigarettes&userid=" + userid + "&name=" + name)
                    //}
                    //  }.ToAttachment(),
                     new HeroCard
                      {
                          Title = $"Main Menu",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/kkt/electionchatbot/backk.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "View Detail", value:$"catlist")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "hpservices")
            {
                var reply = context.MakeMessage();
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                       new HeroCard
                      {
                          Title = $"Health and Beauty products",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/kkt/boycottlist/HealthBeauty.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "View Detail", value:$"https://generalchatbotweb.azurewebsites.net/BoycottList/Index?category=Health%20and%20Beauty%20products&userid=" + userid + "&name=" + name)
                    }
                      }.ToAttachment(),
                     new HeroCard
                      {
                          Title = $"Clinic and Health centres",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/kkt/boycottlist/Health%20Services.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "View Detail", value:$"https://generalchatbotweb.azurewebsites.net/BoycottList/Index?category=Health%20Services&userid=" + userid + "&name=" + name)
                    }
                      }.ToAttachment(),
                     new HeroCard
                      {
                          Title = $"Main Menu",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/kkt/electionchatbot/backk.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "View Detail", value:$"catlist")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "manandretail")
            {
                var reply = context.MakeMessage();
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                       new HeroCard
                      {
                          Title = $"Manufacturing",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/kkt/boycottlist/Manufacturing1.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "View Detail", value:$"https://generalchatbotweb.azurewebsites.net/BoycottList/Index?category=Manufacturing&userid=" + userid + "&name=" + name)
                    }
                      }.ToAttachment(),
                     new HeroCard
                      {
                          Title = $"Retail",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/kkt/boycottlist/Retail.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "View Detail", value:$"https://generalchatbotweb.azurewebsites.net/BoycottList/Index?category=Retail&userid=" + userid + "&name=" + name)
                    }
                      }.ToAttachment(),
                     new HeroCard
                      {
                          Title = $"Main Menu",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/kkt/electionchatbot/backk.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "View Detail", value:$"catlist")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "bankandin")
            {
                var reply = context.MakeMessage();
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                       new HeroCard
                      {
                          Title = $"Banking and Finance",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/kkt/boycottlist/Banking.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "View Detail", value:$"https://generalchatbotweb.azurewebsites.net/BoycottList/Index?category=Banking%20and%20Finance&userid=" + userid + "&name=" + name)
                    }
                      }.ToAttachment(),
                     new HeroCard
                      {
                          Title = $"Industrial Estates Offices",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/kkt/boycottlist/Industrial%20Estates%20Offices.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "View Detail", value:$"https://generalchatbotweb.azurewebsites.net/BoycottList/Index?category=Industrial%20Estates%20Offices&userid=" + userid + "&name=" + name)
                    }
                      }.ToAttachment(),
                     new HeroCard
                      {
                          Title = $"Main Menu",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/kkt/electionchatbot/backk.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "View Detail", value:$"catlist")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "enterandtran")
            {
                var reply = context.MakeMessage();
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                       new HeroCard
                      {
                          Title = $"Entertainment and Tourism",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/kkt/boycottlist/Entertainment.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "View Detail", value:$"https://generalchatbotweb.azurewebsites.net/BoycottList/Index?category=Entertainment%20and%20Tourism&userid=" + userid + "&name=" + name)
                    }
                      }.ToAttachment(),
                     new HeroCard
                      {
                          Title = $"Transport",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/kkt/boycottlist/Transport.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "View Detail", value:$"https://generalchatbotweb.azurewebsites.net/BoycottList/Index?category=Transport&userid=" + userid + "&name=" + name)
                    }
                      }.ToAttachment(),
                     new HeroCard
                      {
                          Title = $"Main Menu",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/kkt/electionchatbot/backk.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "View Detail", value:$"catlist")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "commuandmedia")
            {
                var reply = context.MakeMessage();
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                       new HeroCard
                      {
                          Title = $"Communications",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/kkt/boycottlist/Communication1.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "View Detail", value:$"https://generalchatbotweb.azurewebsites.net/BoycottList/Index?category=Communications&userid=" + userid + "&name=" + name)
                    }
                      }.ToAttachment(),
                     new HeroCard
                      {
                          Title = $"Media",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/kkt/boycottlist/Media.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "View Detail", value:$"https://generalchatbotweb.azurewebsites.net/BoycottList/Index?category=Media&userid=" + userid + "&name=" + name)
                    }
                      }.ToAttachment(),
                     new HeroCard
                      {
                          Title = $"Main Menu",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/kkt/electionchatbot/backk.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "View Detail", value:$"catlist")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "consandtrade")
            {
                var reply = context.MakeMessage();
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                       new HeroCard
                      {
                          Title = $"Construction",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/kkt/boycottlist/Contruction1.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "View Detail", value:$"https://generalchatbotweb.azurewebsites.net/BoycottList/Index?category=Construction&userid=" + userid + "&name=" + name)
                    }
                      }.ToAttachment(),
                       new HeroCard
                      {
                          Title = $"International trades",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/kkt/boycottlist/International%20trades.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "View Detail", value:$"https://generalchatbotweb.azurewebsites.net/BoycottList/Index?category=International%20trades&userid=" + userid + "&name=" + name)
                    }
                      }.ToAttachment(),
                     new HeroCard
                      {
                          Title = $"Trading Companies",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/kkt/boycottlist/Trading%20Companies.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "View Detail", value:$"https://generalchatbotweb.azurewebsites.net/BoycottList/Index?category=Trading%20Companies&userid=" + userid + "&name=" + name)
                    }
                      }.ToAttachment(),
                     new HeroCard
                      {
                          Title = $"Main Menu",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/kkt/electionchatbot/backk.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "View Detail", value:$"catlist")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Type == ActivityTypes.ConversationUpdate)
            {
                await context.PostAsync("Goodbye");
            }
            else if (msg.Text == "findmyid")
            {
                var reply = context.MakeMessage();
                reply.Text = msg.From.Id;
                await context.PostAsync(reply);
            }
            context.Wait(MessageReceivedAsync);
        }

        private async Task ResumeAfterEngFFDialog(IDialogContext context, IAwaitable<object> result)
        {
            var reply = context.MakeMessage();
            reply.Text = "Something else! What can I help you?";
            reply.Type = ActivityTypes.Message;
            reply.TextFormat = TextFormatTypes.Plain;
            reply.SuggestedActions = new SuggestedActions()
            {
                Actions = (new List<string> { "🤔 Start over" }).Select(a => new CardAction
                {
                    Title = a,
                    Type = ActionTypes.ImBack,
                    Value = a
                }).ToList()
            };
            await context.PostAsync(reply);
            context.Wait(MessageReceivedAsync);
        }
        private async Task ResumeAfterUNFFDialog(IDialogContext context, IAwaitable<object> result)
        {
            var reply = context.MakeMessage();
            reply.Text = "ဘာကူညီပေးရပါမလဲ ခင်ဗျာ";
            reply.Type = ActivityTypes.Message;
            reply.TextFormat = TextFormatTypes.Plain;
            reply.SuggestedActions = new SuggestedActions()
            {
                Actions = (new List<string> { "🤔 ပြန်စမည်" }).Select(a => new CardAction
                {
                    Title = a,
                    Type = ActionTypes.ImBack,
                    Value = a
                }).ToList()
            };

            await context.PostAsync(reply);
            context.Wait(MessageReceivedAsync);
        }
        private async Task ResumeAfterZGFFDialog(IDialogContext context, IAwaitable<object> result)
        {
            var reply = context.MakeMessage();
            reply.Text = "ဘာကူညီေပးရပါမလဲ ခင္ဗ်ာ";
            reply.Type = ActivityTypes.Message;
            reply.TextFormat = TextFormatTypes.Plain;
            reply.SuggestedActions = new SuggestedActions()
            {
                Actions = (new List<string> { "🤔 ျပန္စမည္" }).Select(a => new CardAction
                {
                    Title = a,
                    Type = ActionTypes.ImBack,
                    Value = a
                }).ToList()
            };

            await context.PostAsync(reply);
            context.Wait(MessageReceivedAsync);
        }
    }
}