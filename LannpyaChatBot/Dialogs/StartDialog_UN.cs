﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Connector;
using LannpyaBot.Helper;
using LannpyaBot.Models;

namespace LannpyaBot.Dialogs
{
    [Serializable]
    public class StartDialog_UN : IDialog<object>
    {
        string city, taxinfo;
        int cityid;
        public async Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync);

            //return Task.CompletedTask;
        }

        public async Task MessageReceivedAsync(IDialogContext context, IAwaitable<object> result)
        {
            await BusinessLogic(context);     
        }

        public async Task BusinessLogic(IDialogContext context)
        {
            Activity msg = context.Activity as Activity;
            
            if (msg.Text.isWelcome_words())
            {
                var reply = msg.CreateReply($"Hi {msg.From.Name}! Welcome to Lann Pya. I'm here to help you learn about your rights. \nမင်္ဂလာပါ။ လမ်းပြက ကြိုဆိုပါတယ်။ သင့်ရဲ့ အခွင့်အရေးတွေကို လေ့လာနိုင်ဖို့ ကူညီပေးပါရစေ။");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                     new HeroCard
                      {
                          Title = "How may I help you? :)",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lanpya-girl_with_text.png") },
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack,"🤔 Get started", value: $"🤔 Get started"),
                                  new CardAction(ActionTypes.ImBack,"🤔 စတင်မည်", value: $"🤔 စတင်မည်")
                          }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text.isStart_words() || msg.Text == "🤔 Get started")
            {
                await context.Forward(new StartDialog(), this.ResumeAfterEngFFDialog, msg, CancellationToken.None);
                return;
            }
            else if (msg.Text.isStart_words_UN())
            {
                var reply = context.MakeMessage();
                reply.Text = "အောက်ပါပုံရှိမြှားကို နိပ်ပါ သို့မဟုတ် ပုံကိုဘေးသို့ဆွဲပါ သို့မဟုတ် သုံးပုံထဲက တစ်ပုံကို ရွေးပါ";
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                     new HeroCard
                     {
                          Title = $"အကူအညီ ရယူရန်",
                        Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/get_help.png") },
                        Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "အကူအညီ", value:$"gethelp")
                     }
                      }.ToAttachment(),
                       new HeroCard
                      {
                          Title = $"မိမိရပိုင်ခွင့်များ အကြောင်း",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/know_your_rights1.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "ရပိုင်ခွင့်များ", value:$"learnmyrights")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"လမ်းပြအကြောင်း",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/about_lannpya_chatbot.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "လမ်းပြ", value:$"aboutlannpya")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "🙋‍ Get Help")
            {
                var reply = context.MakeMessage();
                reply.Text = "သင်သိသင့်သော အခွန်ဆိုင်ရာ အကြောင်းအရာများ";
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments.Add(
                      new HeroCard
                      {
                          Title = "အကူအညီရယူမည်",
                          Text = $"သင့် အကြံပေးချက်ကို သိပါရစေ။ အကူအညီရယူနိုင်ရင် ဖုန်းခေါ်ဆိုပါ။",
                          Images = new List<CardImage> { new CardImage(ResourceHelper.tax_img_url) },
                          Buttons = new List<CardAction>
                          {
                              new CardAction(ActionTypes.ImBack, "အကြံပေးချက်", value: "Suggestion"),
                              new CardAction(ActionTypes.OpenUrl, "ဖုန်းခေါ်မယ်", value: "tel:+95-9-796926583")
                          }
                      }.ToAttachment());
                await context.PostAsync(reply);
            }
            else if (msg.Text == "gethelp")
            {
                var reply1 = msg.CreateReply("ဘယ်လိုအကူအညီမျိုး လိုအပ်နေပါသလဲ?");
                await context.PostAsync(reply1);
                await context.PostAsync(msg.ResponseTyping());
                var reply = msg.CreateReply($"အောက်ပါပုံရှိမြှားကို နိပ်ပါ သို့မဟုတ် ပုံကိုဘေးသို့ဆွဲပါ သို့မဟုတ် ခုနစ်ပုံထဲက တစ်ပုံကို ရွေးပါ");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"နယ်မြေများ",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/see_land.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "ကြည့်မည်။", value:$"landmap")
                    }
                      }.ToAttachment(),
                     new HeroCard
                     {
                          Title = $"နိုင်ငံသား စီစစ်မှုဆိုင်ရာ စာရွက်စာတမ်းများ",
                        Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/identity_documents.png") },
                        Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "ကြည့်မည်။", value:$"citizenship_dt")
                     }
                      }.ToAttachment(),
                    //   new HeroCard
                    //  {
                    //      Title = $"Report Corruption",
                    //Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/report_corruption.png") },
                    //Buttons = new List<CardAction>
                    //      {
                    //    new CardAction(ActionTypes.ImBack, "Corruption", value:$"corruption")
                    //}
                    //  }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"ရာဇဝတ်မှု ဆိုင်ရာ ကိစ္စရပ်များ",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/criminal_justice.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "ရာဇဝတ်မှု", value:$"criminaljustice")
                    }
                      }.ToAttachment(),
                    //  new HeroCard
                    //  {
                    //      Title = $"Family Problems",
                    //Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/family_problem.png") },
                    //Buttons = new List<CardAction>
                    //      {
                    //    new CardAction(ActionTypes.ImBack, "Family", value:$"familyproblems")
                    //}
                    //  }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"ရွှေ့ပြောင်းသွားလာခြင်း",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/migration.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "ရွှေ့ပြောင်းသွားလာခြင်း", value:$"migration")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"ရှေ့နေများကို ဆက်သွယ်ရန်",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/how_to_work_with_lawyer.png") },
                    Buttons = new List<CardAction>
                          {
                        //new CardAction(ActionTypes.OpenUrl, "ရှေ့နေများ", value:$"https://lannpya.com/LawyerDirectory")
                        new CardAction(ActionTypes.ImBack, "ရှေ့နေများ", value:$"worklawyers")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"ဥပဒေတွေ လိုက်ရှာနေပါသလား။",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/find_law.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "ဥပဒေများ", value:$"findlaws")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"အကြံပေးမည်",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/feedback.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "အကြံပေးမည်", value:$"http://lannpya.com/ForMessengerBot/SuggestionForm?MsgID={msg.From.Id}&UserName={msg.From.Name}")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "corruption" || msg.Text == "familyproblems")
            {
                await context.PostAsync("This feature is coming soon. We are working on it");
                var reply = context.MakeMessage();
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                     new HeroCard
                      {
                          Title = "ဘာများအကူအညီပေးရပါမလဲ? :)",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lanpya-girl_with_text.png") },
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack,"🤔 Get started", value: $"🤔 Get started")
                          }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "findlaws")
            {
                var reply = msg.CreateReply($"ပြည်ထောင်စု ဥပဒေချုပ်ရုံးက ဖွဲ့စည်းပုံ အခြေခံ ဥပဒေ၊ မြန်မာနိုင်ငံရဲ့ တည်ဆဲဥပဒေ-နည်းဥပဒေတွေ၊ စီရင်ထုံးတွေကို  အွန်လိုင်းကနေ ရှာဖွေလို့ရတဲ့ အချက်အလက်စနစ်တခု လွှင့်တင်ထား ပါတယ်။");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                    new HeroCard
                      {
                          Text = $"ကောင်းလိုက်တာ။ အဲ့ဒီမှာ ဥပဒေတွေ သွားရှာ ကြည့်ချင်ပါတယ်။",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Database တွင်ရှာမည်", value:$"searchdatabase")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = "အစသို့ပြန်သွားရန်",
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack,"သွားမည်", value: $"🤔 စတင်မည်")
                          }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "searchdatabase")
            {
                var reply = msg.CreateReply($"ဥပဒေတွေကို မြန်မာ ဥပဒေသတင်းအချက်အလက် စနစ် ဝက်ဘ်ဆိုက်မှာ သွားရောက်ရှာဖွေနိုင်ပါတယ်။ ");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                    new HeroCard
                      {
                          Title = $"၀က်ဘ်ဆိုက်မှာ သွားကြည့်မယ်",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "သွားမည်။", value:$"https://www.mlis.gov.mm/")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = "အစသို့ပြန်သွားရန်",
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack,"သွားမည်", value: $"🤔 စတင်မည်")
                          }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "aboutlannpya")
            {
                var reply1 = msg.CreateReply($"App အကြောင်းပိုသိချင်ပါသလား? ဘာများသိချင်ပါသလဲ?");
                await context.PostAsync(reply1);
                await context.PostAsync(msg.ResponseTyping());
                var reply = msg.CreateReply($"အောက်ပါပုံရှိမြှားကို နိပ်ပါ သို့မဟုတ် ပုံကိုဘေးသို့ဆွဲပါ သို့မဟုတ် ခြောက်ခုထဲက တစ်ခုကို ရွေးပါ");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                    new HeroCard
                      {
                          Title = $"ကျွန်တော်တို့အကြောင်း",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "အသေးစိတ်", value:$"aboutus")
                    }
                      }.ToAttachment(),
                       new HeroCard
                      {
                          Title = $"ဘယ်လို ဒေါင်းလုဒ် လုပ်ရမလဲ",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "အသေးစိတ်", value:$"howtodownload")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"App သုံးရတာ ပြဿနာရှိနေပါတယ်။",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "အသေးစိတ်", value:$"havingproblem")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"App ထဲမှာ ပါချင်လို့ပါ။",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "အသေးစိတ်", value:$"registerapp")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"ပုဂ္ဂိုလ်ရေးအချက်အလက်ဆိုင်ရာ မူဝါဒ",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "မူဝါဒများ", value:$"privacypolicy")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"ချက်ဘော့အစသို့ ပြန်သွားရန်",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "သွားမည်", value:$"🤔 စတင်မည်")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "registerapp")
            {
                var reply = context.MakeMessage();
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                       new HeroCard
                      {
                          Title = $"သင်က ရှေ့နေတစ်ယောက်လား?",
                          Text="ဒီနေရာတွင် စာရင်းသွင်းပါ။",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "စာရင်းသွင်းမည်။", value:$"https://docs.google.com/forms/d/e/1FAIpQLSdUAtT4885dPbgxMK4o4fOk2M6tvnJq1nQUZBEWiqElBGqFaA/viewform")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"လူမှုအကူအညီ အဖွဲ့အစည်းမှလား?",
                          Text="ဒီနေရာတွင် စာရင်းသွင်းပါ။",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "စာရင်းသွင်းမည်။", value:$"https://docs.google.com/forms/d/e/1FAIpQLSdSk6S4VuPu2Bu3GBXCeaTSP5hAGotnHD3b6mwCp5DZBz6lhg/viewform")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"အကြံပြုချက်၊ မေးစရာများ",
                          Text="သင့်မှာ အခက်အခဲတစုံတရာရှိပါက အောက်ပါလင့်မှ ဆက်သွယ်ပါ။",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "ဆက်သွယ်မည်။", value:$"http://lannpya.com/ForMessengerBot/SuggestionForm?MsgID={msg.From.Id}&UserName={msg.From.Name}")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"ခေါင်းစဥ်များ",
                          Text="မူလခေါင်းစဥ်များ သို့ပြန်သွားမည်။",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "သွားမည်။", value:$"🤔 စတင်မည်")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "privacypolicy")
            {
                var reply1 = msg.CreateReply("သင့်ရဲ့ပုဂ္ဂိုလ်ရေးဆိုင်ရာ အချက်လက်တွေကို အကာအကွယ်ပေးနိုင်ဖို့ လမ်းပြက အမြဲကြိုးစား နေပါတယ်။");
                await context.PostAsync(reply1);
                await context.PostAsync(msg.ResponseTyping());
                var reply2 = msg.CreateReply("သို့သော် Facebook သည် သင့်ရဲ့ အချက်အလက် တွေကို ကောက်ယူနိုင်ပါတယ်။ ဒါကြောင့် Facebook မက်ဆင်ဂျာကနေ ပို့လိုက်တဲ့ အကြောင်းအရာတွေကို လုံခြုံအောင် အကာအကွယ်ပေးနိုင်ဖို့ ကျွန်တော်တို့ဘက်မှ အာမ မခံနိုင်ပါ။ ");
                await context.PostAsync(reply2);
                await context.PostAsync(msg.ResponseTyping());
                var reply = msg.CreateReply($"ကျွန်တော်တို့ရဲ့ ပုဂ္ဂိုလ်ရေးအချက်အလက်ဆိုင်ရာ မူဝါဒတွေကို သိချင်ပါသလား?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                       new HeroCard
                      {
                          Title = $"မသိချင်ပါ။ Main Menu ဆီကိုပဲ ပြန်သွားပါ။",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Menu", value:$"🤔 စတင်မည်")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"ဟုတ်ကဲ့၊ သိချင်ပါတယ်။",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "မူဝါဒများ", value:$"https://lannpya.com/TermsandConditions")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "howtodownload")
            {
                var reply = msg.CreateReply($"လမ်းပြကိုသင့်ဖုန်းထဲမှာ Download လုပ်ရမည့်နည်းလမ်းအများကြီးရှိပါတယ်။ အဲဒီအထဲမှာမှ Face book နဲ့ Google Play Store ကိုသုံးဖို့အကြံပြုလိုပါတယ်");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                       new HeroCard
                      {
                          Title = $"Playstore ကနေ ယူမယ်",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/googleplaystore.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "အဆင့်ဆင့်ကြည့်မည်", value:$"downloadfromplaystore")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Facebook ကနေ ယူမယ်",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/facebook.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "အဆင့်ဆင့်ကြည့်မည်", value:$"downloadfromfacebook")
                    }
                      }.ToAttachment()
                    //   new HeroCard
                    //  {
                    //      Title = $"Playstore ကနေ ယူမယ်",
                    //Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/a4-how-to-download-on-play-store.jpg") },
                    //Buttons = new List<CardAction>
                    //      {
                    //    new CardAction(ActionTypes.OpenUrl, "အသေးစိတ်", value:$"https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/a4-how-to-download-on-play-store.jpg"),
                    //    new CardAction(ActionTypes.ImBack, "အဆင့်ဆင့်ကြည့်မည်", value:$"downloadfromplaystore")
                    //}
                    //  }.ToAttachment(),
                    //  new HeroCard
                    //  {
                    //      Title = $"Facebook ကနေ ယူမယ်",
                    //Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/a4-how-to-download.jpg") },
                    //Buttons = new List<CardAction>
                    //      {
                    //    new CardAction(ActionTypes.OpenUrl, "အသေးစိတ်", value:$"https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/a4-how-to-download.jpg"),
                    //    new CardAction(ActionTypes.ImBack, "အဆင့်ဆင့်ကြည့်မည်", value:$"downloadfromfacebook")
                    //}
                    //  }.ToAttachment()
                };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "aboutus")
            {
                var reply = context.MakeMessage();
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                       new HeroCard
                      {
                          Title = $"လမ်းပြ",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lannpya.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "အသေးစိတ်", value:$"about_lannpya")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"MyJustice",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/mj_logo.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "အသေးစိတ်", value:$"about_myjusstice")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"ကိုးကိုးတက်ခ်",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/kkt_logo.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "အသေးစိတ်", value:$"about_koekoetech")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Acknowledgements",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/acknowledgement.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "အသေးစိတ်", value:$"about_acknowledgements")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "about_lannpya")
            {
                var reply1 = msg.CreateReply("လမ်းပြ ဟာ MyJustice နဲ့ Koe Koe Tech တို့ စမ်းသစ် တီထွင်ပူးပေါင်းကြံဆမှုကြောင့် ထွက်ပေါ်လာတဲ့ ရလဒ်တစ်ခုဖြစ်ပြီးတော့ မြန်မာ ပြည်သူလူထုကို သတင်းအချက်အလက်ပေးတာ၊ ချိတ်ဆက်ပေးတာနဲ့ စွမ်းပကားတွေ ပိုရှိလာအောင် လုပ်ပေးတဲ့နေရာမှာ အထောက်ကူဖြစ်စေဖို့နဲ့ လူမှုအသိုင်းအဝန်း၊ အစိုးရနဲ့ အစိုးရမဟုတ်သော ဝန်ဆောင်မှုပေးသူတွေအကြားမှာ ယုံကြည်မှု တည်ဆောက်ပေးဖို့ ပဲဖြစ်ပါတယ်။");
                await context.PostAsync(reply1);
                await context.PostAsync(msg.ResponseTyping());
                var reply2 = msg.CreateReply("လမ်းပြသည် ဥပဒေများ၊ အခွင့်အရေးများနှင့် ပတ်သတ်သော သတင်းအချက်အလက်များကို ပိုမိုလွတ်လပ်စွာ ရယူလာနိုင်စေရန်နှင့် တရားမျှတမှု ရှာဖွေနေသူများကို ရှေ့နေများ၊ အစိုးရနှင့် အစိုးရမဟုတ်သော အဖွဲ့အစည်းများနှင့် ချိတ်ဆက်ရာတွင် အကူအညီပေးနိုင်ပါသည်။");
                await context.PostAsync(reply2);
                await context.PostAsync(msg.ResponseTyping());
                var reply3 = msg.CreateReply("လမ်းပြဟူသော စကားရပ်အတိုင်း ကျွန်တော်တို့ App သည် မြန်မာနိုင်ငံကြီးအား ပိုမိုတရားမျှတသော၊ ပို၍ သာတူညီမျှဖြစ်သော၊ ဒီမိုကရေစီနိုင်ငံတော်အဖြစ်သို့ ရောက်ရှိအောင် လမ်းပြပေးနိုင်မည်ဟု မျှော်လင့်ပါသည်။");
                await context.PostAsync(reply3);
                await context.PostAsync(msg.ResponseTyping());
                var reply = msg.CreateReply("ဒီနေ့ပဲ လမ်းပြကို ဒေါင်းလုဒ်ရယူလိုက်ပါ!");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"လမ်းပြ app ကိုဒေါင်းရန်",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack,"ဒေါင်းမည်" , value:$"downloadlp_justice")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"မူလခေါင်းစဥ်များ သို့ပြန်သွားမည်။",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "သွားမည်။", value:$"🤔 စတင်မည်")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "about_myjusstice")
            {
                var reply1 = msg.CreateReply("MyJustice အနေဖြင့် ပြည်သူလူထုမှ မိမိတို့ရင်ဆိုင်ကြုံတွေ့နေရသော ပဋိပက္ခနှင့် အငြင်းပွားမှုများကို သာတူညီမျှ၊ မျှတ မှန်ကန်အောင် ဖြေရှင်းလာနိုင်စေရန်အတွက် လိုအပ်သော အသိပညာဗဟုသုတ၊ ယုံကြည်မှု၊ နှင့် အခွင့်အလမ်းများ ပိုမိုရရှိအောင် ဆောင်ရွက်ပေးနိုင်ရန် အတွက် ရည်သန်ပါသည်။");
                await context.PostAsync(reply1);
                await context.PostAsync(msg.ResponseTyping());
                var reply2 = msg.CreateReply("MyJustice မှ အငြင်းပွားမှုများကို ဖြေရှင်းခြင်းနှင့် တရားမျှတမှု ရရှိအောင်ဆောင်ရွက်ရာတွင်၊ အထူးသဖြင့် ယင်းတို့ကို ဆင်းရဲနွမ်းပါးသူများ၊ အားနည်းသူများနှင့် အပယ်ခံများအတွက် ဆောင်ရွက်ပေးရာတွင် ရေရှည် တည်တံ့သည့် အကျိုးကျေးဇူး ရရှိစေရန်အတွက် ဒေသခံ ရပ်ရွာ လူထုနှင့်အတူ အနီးကပ် ပူးပေါင်းဆောင်ရွက်လျှက် ရှိပါသည်။");
                await context.PostAsync(reply2);
                await context.PostAsync(msg.ResponseTyping());
                var reply3 = msg.CreateReply("MyJustice စီမံကိန်းကို ဥရောပသမဂ္ဂက ရံပုံငွေ ပံ့ပိုးပြီး ဗြိတိသျှကောင်စီမှ အကောင်အထည်ဖော် ဆောင်ရွက်လျှက်ရှိပါသည်။");
                await context.PostAsync(reply3);
                await context.PostAsync(msg.ResponseTyping());
                var reply4 = msg.CreateReply("လမ်းပြ ဟာ MyJustice နဲ့ Koe Koe Tech တို့ စမ်းသစ် တီထွင်ပူးပေါင်း ဆောင်ရွက်မှု တစ်ခုဖြစ်ပါသည်။");
                await context.PostAsync(reply4);
                await context.PostAsync(msg.ResponseTyping());
                var reply = msg.CreateReply("ဒီနေ့ပဲ လမ်းပြကို ဒေါင်းလုဒ်ရယူလိုက်ပါ!");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"Download Lannpya",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Download", value:$"downloadlp_justice")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Go to MyJustice",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "MyJustice", value:$"https://www.myjusticemyanmar.org/")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Back to Lann Pya",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Menu", value:$"🤔 စတင်မည်")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "about_koekoetech")
            {
                var reply1 = msg.CreateReply("Koe Koe Tech ကုမ္ပဏီလီမိတက်သည် ကျန်းမာရေး၊ ဥပဒေနှင့် စီမံခန့်ခွဲရေး အစရှိသည့် နယ်ပယ်များတွင် အဓိကကျသော အခက်အခဲများကို ဖြေရှင်း ပေးနိုင်မည့် စမတ်ဖုန်းအပလီကေးရှင်းများ၊ ဆော့ဖ်ဝဲများကို တီထွင်ဖန်တီးပေးနေသည့် မြန်မာအိုင်တီ လူမှုအကျိုးပြဒ လုပ်ငန်းတစ်ရပ် ဖြစ်ပါသည်။");
                await context.PostAsync(reply1);
                await context.PostAsync(msg.ResponseTyping());
                var reply2 = msg.CreateReply("TheOur mission is to develop Myanmar’s vital IT infrastructure, taking advantage of the latest technology to enable Myanmar to leap forward in its development. Our vision is a Myanmar in which citizens, government, business and NGOs are all digitally connected and can benefit from the latest technology to enhance their work and further connect up a rapidly developing country.");
                await context.PostAsync(reply2);
                await context.PostAsync(msg.ResponseTyping());
                var reply = msg.CreateReply("ဒီနေ့ပဲ လမ်းပြကို ဒေါင်းလုဒ်ရယူလိုက်ပါ!");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"လမ်းပြ ကိုဒေါင်းလုတ် လုပ်မည်",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "ဒေါင်းလုတ်", value:$"downloadlp_justice")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"ကိုကိုးတက်ခ်ဆီ သွားမယ်",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "ကိုကိုးတက်ခ်", value:$"https://www.koekoetech.com/")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Back to Lann Pya",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Menu", value:$"🤔 စတင်မည်")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "about_acknowledgements")
            {
                var reply1 = msg.CreateReply("British Councilမှ အကောင်အထည်ဖော် ဆောင်ရွက်သော MyJustice Programme နှင့် မိတ်ဖက် အဖွဲ့အစည်းဖြစ်သော လူမှုအကျိုးပြု နည်းပညာ ကုမ္ပဏီ Koe Koe Techတို့နှင့်အတူ ဥရောပသမဂ္ဂ၏ များစွာသောထောက်ပံ့ကူညီမှုတို့ဖြင့် တရားမျှတမှုအတွက် mobile applicationဖြစ်သော Lann Pay(လမ်းပြ) Applicationကို ဖန်တီးခဲ့ပါသည်။");
                await context.PostAsync(reply1);
                await context.PostAsync(msg.ResponseTyping());
                var reply2 = msg.CreateReply("Lann Pya(လမ်းပြ) Applicationကို အကောင်အထည် ဖော်ရာတွင် များစွာသော အစိုးရအဖွဲ့အစည်းများ၊ အစိုးရမဟုတ်သော အဖွဲ့အစည်းများနှင့် လူတစ်ဦး တစ်ယောက်ချင်းစီတို့က MyJusticeနှင့် Koe Koe Techတို့ကို ထောက်ပံ့ကူညီပေးခဲ့ပါသည်။");
                await context.PostAsync(reply2);
                await context.PostAsync(msg.ResponseTyping());
                var reply3 = msg.CreateReply("ထို့အပြင် လူထုအတွက် အသုံးဝင်သော အကြောင်းအရာများ ပါဝင်လာစေရေးအတွက် အကြံဉာဏ်များနှင့် တုံ့ပြန်ချက်များပေးခဲ့သော လူပုဂ္ဂိုလ် တစ်ဦးတစ်ယောက်ချင်းစီကိုလည်း ကျေးဇူးတင်ရှိပါသည်။");
                await context.PostAsync(reply3);
                await context.PostAsync(msg.ResponseTyping());
                var reply4 = msg.CreateReply("အထူးသဖြင့် ကျွန်ုပ်တို့နှင့် ပူးပေါင်းပါဝင်ပေးခဲ့ပါသော ပြည်ထောင်စုရှေ့နေချုပ်ရုံး၊ ပြည်ထောင်စု တရားလွှတ်တော်ချုပ်ရုံး၊ မြန်မာနိုင်ငံရဲတပ်ဖွဲ့၊ International Development Law Organization (IDLO)၊ အပြည်ပြည်ဆိုင်ရာအလုပ်သမားများအဖွဲ့ချုပ် (ILO)၊ ရွှေ့ပြောင်းသွားလာမှုဆိုင်ရာ အပြည်ပြည်ဆိုင်ရာ အဖွဲ့(IMO)၊ ကမ္ဘာ့ကုလသမဂ္ဂ လူဦးရေ ရံပုံငွေအဖွဲ့(UNFPA)၊ မြန်မာနိုင်ငံလွတ်လပ်သောရှေ့နေများအဖွဲ့၊ မန္တလေး ရှေ့နေကြီးများအသင်း၊ မော်လမြိုင်ရှေ့နေအသင်း၊ ရှမ်းပြည်ရှေ့နေအသင်း၊ စစ်ကိုင်းနှင့် ပဲခူး တိုင်းဒေသကြီး ဥပဒေဆိုင်ရာအထောက်အကူပေးရေး အဖွဲ့၊ မွန်ပြည်နယ် ဥပဒေဆိုင်ရာ အထောက်အကူ‌ ပေးရေးအဖွဲ့၊ ရန်ကုန်ရှေ့နေအသင်း၊ ရန်ကုန်တိုင်းဒေသကြီး တရားဥပဒေစိုးမိုးမှု ဌာနနှင့် တရားမျှတမှုဆိုင်ရာ ကိစ္စရပ်များ ပေါင်းစပ် ညှိနှိုင်းရေးအဖွဲ့၊ International Bridges to Justice(IBJ) အဖွဲ့၊ မော်လမြိုင် တရားမျှတမှုစင်တာ၊ ရန်ကုန် တရားမျှတမှုစင်တာနှင့် နော်ဝေးဒုက္ခသည်များ ဆိုင်ရာ ကောင်စီ(NRC)၊ Myanmar Book Aid & Preservation Foundation၊ Eden Ministry၊ Justice Base၊ Braveheart Foundation၊ နှင့် Azuri Creative.တို့ကို အထူးကျေးဇူးတင်ရှိပါသည်။");
                await context.PostAsync(reply4);
                await context.PostAsync(msg.ResponseTyping());
                var reply = msg.CreateReply("ဒီနေ့ပဲ လမ်းပြကို ဒေါင်းလုဒ်ရယူလိုက်ပါ!");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"လမ်းပြကို ဒေါင်းလုတ် လုပ်မည်",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack,"ဒေါင်းလုတ်", value:$"downloadlp_justice")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Main menu သို့ ပြန်သွားမည်",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Menu", value:$"🤔 စတင်မည်")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text.Contains("problem"))
            {
                await HavingProblem_UN(msg.Text, msg, context);
            }
            else if (msg.Text == "downloadfromplaystore")
            {
                var reply = msg.CreateReply("အဆင့်(၁) သင့်ဖုန်းထဲတွင် Google Play Store ကိုရှာပါ သတိပြုရန်မှာ Google Play Store ကိုအသုံးပြုရန်အတွက် အင်တာနက်ချိတ်ဆက်ရန်လိုအပ်ပါသည်။ Play Store အပလီကေးရှင်းတွင် သင့်ရဲ့ Gmail account ကို (Sign in) ရိုက်နှိပ်၀င်ရောက်ထားရမည်။");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"Playstore",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/googleplaystore.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "ဆက်သွားမည်", value:$"stepone_dl_ps")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "stepone_dl_ps")
            {
                var reply = msg.CreateReply("အဆင့်(၂) အပလီကေးရှင်းမျက်နှာပြင်၏ ထိပ်ဆုံးနေရာရှိ Search box တွင် 'Lann Pya' ဟုရိုက်ထည့်ပါ။ ");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"Playstore တွင်ရှာမည်",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/g1.PNG") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "ဆက်သွားမည်", value:$"steptwo_dl_ps")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "steptwo_dl_ps")
            {
                var reply = msg.CreateReply("အဆင့်(၃) ကျလာသည့်စာရင်းထဲတွင် 'Lann Pya' ကိုနှိပ်ပါ၊ ပြီးနောက် 'Install' ကိုနှိပ်ပြီး သင့်ဖုန်းတွင် အပလီကေးရှင်းကိုစတင် download လုပ်ပါ။ ");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"ရှာတွေ့ပြီ",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/g2.PNG") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "ဆက်သွားမည်", value:$"stepthree_dl_ps")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "stepthree_dl_ps")
            {
                var reply = msg.CreateReply("အဆင့်(၄) download လုပ်ပြီးပါက ယခုသင့်ဖုန်းထဲတွင် လမ်းပြအပလီကေးရှင်းကို အသုံးပြုနိုင်ပြီဖြစ်ပါသည်။ အပလီကေးရှင်းကို အသုံးပြုနိုင်ရန် လမ်းပြ icon ပုံကိုနှိပ်ပါ။");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"Lannpya app ကိုသွင်းမည်",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lannpya_logo1.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "ပြီးပြီ", value:$"fisnished_download")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "downloadfromfacebook")
            {
                var reply = context.MakeMessage();
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"Facebook",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/facebook.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "ဆက်သွားမည်", value:$"stepone_dl_fb")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "stepone_dl_fb")
            {
                var reply = msg.CreateReply("အဆင့် (၁) Facebook search box တွင် Lann Pya ဟုရိုက်ထည့်၍ ကျွန်န်ုပ်တို့၏  Facebook စာမျက်နှာကို ၀င်ရောက်လေ့လာပါ။");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"Facebook တွင်ရှာမည်",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/f1.PNG") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "ဆက်သွားမည်", value:$"steptwo_dl_fb")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "steptwo_dl_fb")
            {
                var reply = msg.CreateReply("အဆင့် (၂) သင့်ဖုန်းသို့ download စတင်လုပ်ဆောင်နိုင်ရန် လမ်းပြဖေ့စ်ဘုတ် စာမျက်နှာ၏အပေါ်နားရှိ 'Use App' ခလုတ်ကိုနှိပ်ပါ။");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"ရှာတွေပြီ",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/f2.PNG") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "ဆက်သွားမည်", value:$"stepthree_dl_fb")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "stepthree_dl_fb")
            {
                var reply = msg.CreateReply("အဆင့်(၃) Download ပြုလုပ်ပြီးပါက “Install” ကိုနှိပ်ပါ။ ယခုသင့်ဖုန်းတွင် လမ်းပြကိုစတင်အသုံးပြုနိုင်ပါပြီ ");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"Lannpya app ကိုသွင်းမည်",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lannpya_logo1.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "ပြီးပြီ", value:$"fisnished_download")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "fisnished_download")
            {
                var reply = msg.CreateReply("လမ်းပြ app ဒေါင်းလုတ် လုပ်ခြင်းအတွက် ကျေးဇူးတင်ပါသည် :)");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = "ဒေါင်းလုတ်လုပ်နည်း ပြန်ကြည့်မည်",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "ကြည့်မည်", value:$"howtodownload")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Main menu ကိုပြန်သွားမည်",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Menu", value:$"🤔 စတင်မည်")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            context.Wait(MessageReceivedAsync);
        }

        public async Task Citizenship_DT_UN(string text = null, Activity msg = null, IDialogContext context = null)
        {
            if (text == "citizenship_dt")
            {
                var reply = msg.CreateReply("ဘယ်အကြောင်းအရာကို လေ့လာလိုပါသလဲ");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"နိုင်ငံသားဖြစ်ခွင့်နှင့် နေထိုင်ကြောင်းကဒ်များ",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/idcard.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "အသေးစိတ်", value:$"residency_citizenship")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "residency_citizenship")
            {
                var reply = msg.CreateReply("Justice Base အဖွဲ့၏ အကူအညီဖြင့် သင့်ရဲ့ နိုင်ငံသားဖြစ်မှုအနေထားကို စမ်းစစ်နိုင်တဲ့ ကိရိယာတွေ ရှိပါတယ် ");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          //Title = $"Citizenship and residency cards",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/JusticeBase_logo.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "အသေးစိတ်", value:$"identityinformation_citizenship")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"မူ၀ါဒများ",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/idcard.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "မူ၀ါဒများ", value:$"privacypolicy_citizenship")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "privacypolicy_citizenship")
            {
                var reply1 = msg.CreateReply("သင်သိထားရမယ်အချက်တစ်ခုရှိပါတယ်။ အဲ့ဒါက သင့်ရဲ့ပုဂ္ဂိုလ်ရေးဆိုင်ရာ အချက်လက်တွေကို အကာအကွယ်ပေးနိုင်ဖို့ လမ်းပြက အမြဲကြိုးစားနေပါတယ်။ သို့သော် Facebook သည် သင့်ရဲ့အချက်အလက် တွေကို ကောက်ယူနိုင်ပါတယ်။");
                await context.PostAsync(reply1);
                await context.PostAsync(msg.ResponseTyping());
                var reply2 = msg.CreateReply("ဒါကြောင့် Facebook မက်ဆင်ဂျာကနေ ပို့လိုက်တဲ့ အကြောင်းအရာတွေကို လုံခြုံအောင် အကာအကွယ်ပေးနိုင်ဖို့ ကျွန်တော်တို့ဘက်မှ အာမ မခံနိုင်ပါ။");
                await context.PostAsync(reply2);
                await context.PostAsync(msg.ResponseTyping());
                var reply = msg.CreateReply("ဆက်လက်လုပ်ဆောင်လိုပါသလား ");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text = $"ဆက်မလုပ်တော့ပါ။ ဒါပေမယ့် ကျွန်ုပ်ရဲ့ နိုင်ငံသား ဖြစ်မှုအခြေအနေကို သိလိုပါတယ်။",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "ကြည့်မည်။", value:$"identityinformation_citizenship")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Text = $"သင့်ရဲ့ ပုဂ္ဂိုလ်ရေးဆိုင်ရာ အချက်လက်တွေနဲ့ ပတ်သတ်ပြီး စိတ်ပူတဲ့ဆိုရင် မေးခွန်းပုံစံ တခုလုံးကို အောက်မှ ဒေါင်းလုဒ် လုပ်နိုင်ပါတယ် ",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "မူ၀ါဒများ", value:$"https://lannpya.com/TermsandConditions")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "identityinformation_citizenship")
            {
                var reply = msg.CreateReply("ဘယ်အကြောင်းအရာကို လေ့လာလိုပါသလဲ");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          //Title = $"Citizenship and residency cards",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/identity_documents.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "အသေးစိတ်", value:$"liketoknow_citizenship")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "liketoknow_citizenship")
            {
                var reply = msg.CreateReply("သင်သည် တိုင်:ရင်းသားတစ်ဦးဟုတ်ပါသလား?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="citizenship_tyt_Yes" },
                        new CardAction(){ Title = "No", Type=ActionTypes.ImBack, Value="citizenship_tyt_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "citizenship_tyt_Yes" || text == "citizenship_law_Yes" || text == "citizenship_second_Yes" || text == "citizenship_associate_Yes" || text == "citizenship_grandparents_Yes")
            {
                var reply = msg.CreateReply("သင်သည် နိုင်ငံသားဖြစ်သည်။");
                await context.PostAsync(reply);
                await context.PostAsync(msg.ResponseTyping());
                var reply1 = msg.CreateReply("ကောင်းပြီ။ ဘာဆက်လုပ်သင့်ပါသလဲ?");
                await context.PostAsync(reply1);
                await context.PostAsync(msg.ResponseTyping());
                var reply3 = msg.CreateReply("အကယ်လို့ သင်ရဲ့နိုင်ငံသားဖြစ်မှုအနေအထားကို စိတ်ပူတယ်ဆိုရင် သင့်ကိုကူညီပေးနိုင်မယ့် ရှေ့နေကိုဖြစ်ဖြစ်၊ အခြားဝန်ဆောင်မှု ပေးနေတဲ့သူကိုဖြစ်ဖြစ် ဆက်သွယ်တာ အကောင်းဆုံးပါ။ သူတို့ကို ရှာဖို့ အခုပဲ လမ်းပြကို ဒေါင်းလုဒ်လုပ်လိုက်ပါ။");
                reply3.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply3.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"လမ်းပြ app ကိုသွင်းမည်",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/download_lannpya.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "အသေးစိတ်", value:$"downloadlp_justice")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = "အစသို့ပြန်သွားရန်",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lannpya_logo1.png") },
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack,"သွားမည်", value: $"🤔 စတင်မည်")
                          }
                      }.ToAttachment(),
                    new HeroCard
                      {
                          Title = $"Start Identity",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/identity_documents.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "အသေးစိတ်", value:$"liketoknow_citizenship")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply3);
            }
            else if (text == "citizenship_tyt_No")
            {
                var reply = context.MakeMessage();
                reply.Text = "၁၉၄၈ ခုနှစ် မြန်မာနိုင်ငံသားဥပဒေအရ ၁၉၈၂ ခုနှစ် အောက်တိုဘာ ၁၅ ရက် မတိုင်ခင်က နိုင်ငံသားဖြစ်ခဲ့ပါသလား?";
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="citizenship_law_Yes" },
                        new CardAction(){ Title = "No", Type=ActionTypes.ImBack, Value="citizenship_law_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
                //var reply = msg.CreateReply($"Where you already a citizen on 15 October 1982 or under the 1948 Citizenship Law?");
                //reply.Type = ActivityTypes.Message;
                //reply.TextFormat = TextFormatTypes.Plain;
                //reply.SuggestedActions = new SuggestedActions()
                //{
                //    Actions = (new List<string> { "🙂 Yes", "😦 No" }).Select(a => new CardAction
                //    {
                //        Title = a,
                //        Type = ActionTypes.ImBack,
                //        Value = "citizenship_law_" + a
                //    }).ToList()
                //};
                //await context.PostAsync(reply);
            }
            else if (text == "citizenship_law_No")
            {
                var reply = context.MakeMessage();
                reply.Text = "သင့်မိဘနှစ်ပါးအနက် တစ်ပါးသည် နိုင်ငံသားဖြစ်ပါသလား?";
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="citizenship_parents_Yes" },
                        new CardAction(){ Title = "No", Type=ActionTypes.ImBack, Value="citizenship_parents_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "citizenship_parents_Yes")
            {
                var reply = context.MakeMessage();
                reply.Text = $"ကျန်မိဘတစ်ပါးသည် နိုင်ငံသား (သို့) ဧည့်နိုင်ငံသား (သို့) နိုင်ငံသားပြုခွင့်ရသူဖြစ်ပါသလား?";
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="citizenship_second_Yes" },
                        new CardAction(){ Title = "No", Type=ActionTypes.ImBack, Value="citizenship_second_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "citizenship_second_No")
            {
                var reply = context.MakeMessage();
                reply.Text = $"သင့်မိဘနှစ်ဦးအနက် ကျန်တစ်ဦးသည် နိုင်ငံခြားသား ဖြစ်ပါသလား?";
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="citizenship_foreigner_Yes" },
                        new CardAction(){ Title = "No", Type=ActionTypes.ImBack, Value="citizenship_foreigner_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "citizenship_foreigner_Yes" || text == "citizenship_bothparent_No" || text == "citizenship_seondforengner_Yes")
            {
                var reply = msg.CreateReply("နိုင်ငံသားပြုခွင့်ရသူ တစ်ဦးဖြစ်စေရန် လျှောက်ထားနိုင်သည်။");
                await context.PostAsync(reply);
                await context.PostAsync(msg.ResponseTyping());
                var reply4 = msg.CreateReply("လက်ထပ်ထိမ်းမြားခြင့်ဖြင့် နိုင်ငံသားပြုခွင့်ရသူ တစ်ဦးဖြစ်ရန်မှာ");
                await context.PostAsync(reply4);
                await context.PostAsync(msg.ResponseTyping());
                var reply5 = msg.CreateReply("နိုင်ငံခြားသားမှတ်ပုံတင်ကတ်ကို ၁၉၈၂ ခု အောက်တိုဘာလ ၁၅ ရက် မတိုင်ခင်က ရရှိပြီးသူဖြစ်ရမည်။");
                await context.PostAsync(reply5);
                await context.PostAsync(msg.ResponseTyping());
                var reply6 = msg.CreateReply("၁၉၈၂ ခုနှစ် အောက်တိုဘာလ မတိုင်ခင်က လက်ထပ်ပြီးသူဖြစ်ရမည်။");
                await context.PostAsync(reply6);
                await context.PostAsync(msg.ResponseTyping());
                var reply7 = msg.CreateReply("သင့်ခင်ပွန်း (သို့) ဇနီးသည် သည် နိုင်ငံသား (သို့) နိုင်ငံသားပြုခွင့်ရသူ (သို့) ဧည့်နိုင်ငံသားဖြစ်ရမည်။");
                await context.PostAsync(reply7);
                await context.PostAsync(msg.ResponseTyping());
                var reply8 = msg.CreateReply("သင်သည် ထိုသူ၏ လင်ယောကျ်ား (သို့) ဇနီးမယားဖြစ်ရမည်။");
                await context.PostAsync(reply8);
                await context.PostAsync(msg.ResponseTyping());
                var reply9 = msg.CreateReply("လက်ထပ်ပြီးသည့် သက်တမ်း ၃ နှစ်ရှိရမည်။");
                await context.PostAsync(reply9);
                await context.PostAsync(msg.ResponseTyping());
                var reply10 = msg.CreateReply("မြန်မာနိုင်ငံ၌ အနည်းဆုံး ၃ နှစ်နေထိုင်ပြီးသူ ဖြစ်ရမည်။");
                await context.PostAsync(reply10);
                await context.PostAsync(msg.ResponseTyping());
                var reply1 = msg.CreateReply("ကောင်းပြီ။ ဘာဆက်လုပ်သင့်ပါသလဲ?");
                await context.PostAsync(reply1);
                await context.PostAsync(msg.ResponseTyping());
                var reply3 = msg.CreateReply("အကယ်လို့ သင်ရဲ့နိုင်ငံသားဖြစ်မှုအနေအထားကို စိတ်ပူတယ်ဆိုရင် သင့်ကိုကူညီပေးနိုင်မယ့် ရှေ့နေကိုဖြစ်ဖြစ်၊ အခြားဝန်ဆောင်မှု ပေးနေတဲ့သူကိုဖြစ်ဖြစ် ဆက်သွယ်တာ အကောင်းဆုံးပါ။ သူတို့ကို ရှာဖို့ အခုပဲ လမ်းပြကို ဒေါင်းလုဒ်လုပ်လိုက်ပါ။");
                reply3.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply3.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"လမ်းပြ app ကိုသွင်းမည်",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/download_lannpya.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "အသေးစိတ်", value:$"downloadlp_justice")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = "အစသို့ပြန်သွားရန်",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lannpya_logo1.png") },
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack,"သွားမည်", value: $"🤔 စတင်မည်")
                          }
                      }.ToAttachment(),
                    new HeroCard
                      {
                          Title = $"Start Identity",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/identity_documents.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "အသေးစိတ်", value:$"liketoknow_citizenship")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply3);
            }
            else if (text == "citizenship_foreigner_No")
            {
                var reply = context.MakeMessage();
                reply.Text = $"သင့်မိဘနှစ်ပါးအနက် ကျန်တစ်ဦး၏ မိဘများသည် နိုင်ငံသားဖြစ်ခွင့်ရသူများ (သို့) ဧည့်နိုင်ငံသားများ ဖြစ်ကြပါသလား?";
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="citizenship_associate_Yes" },
                        new CardAction(){ Title = "No", Type=ActionTypes.ImBack, Value="citizenship_associate_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "citizenship_associate_No" || text == "citizenship_oneparent_No" || text == "citizenship_seondforengner_No")
            {
                var reply = msg.CreateReply("သင်၏ဥပဒေဆိုင်ရာ ဖြစ်တည်မှုများနှင့် ပတ်သက်သည့် အကြံဥာဏ်များကို ရယူနိုင်ရန် ကြိုးစားသင့်သည်။");
                await context.PostAsync(reply);
                await context.PostAsync(msg.ResponseTyping());
                var reply1 = msg.CreateReply("ကောင်းပြီ။ ဘာဆက်လုပ်သင့်ပါသလဲ?");
                await context.PostAsync(reply1);
                await context.PostAsync(msg.ResponseTyping());
                var reply3 = msg.CreateReply("အကယ်လို့ သင်ရဲ့နိုင်ငံသားဖြစ်မှုအနေအထားကို စိတ်ပူတယ်ဆိုရင် သင့်ကိုကူညီပေးနိုင်မယ့် ရှေ့နေကိုဖြစ်ဖြစ်၊ အခြားဝန်ဆောင်မှု ပေးနေတဲ့သူကိုဖြစ်ဖြစ် ဆက်သွယ်တာ အကောင်းဆုံးပါ။ သူတို့ကို ရှာဖို့ အခုပဲ လမ်းပြကို ဒေါင်းလုဒ်လုပ်လိုက်ပါ။");
                reply3.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply3.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"လမ်းပြ app ကိုသွင်းမည်",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/download_lannpya.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "အသေးစိတ်", value:$"downloadlp_justice")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = "အစသို့ပြန်သွားရန်",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lannpya_logo1.png") },
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack,"သွားမည်", value: $"🤔 စတင်မည်")
                          }
                      }.ToAttachment(),
                    new HeroCard
                      {
                          Title = $"Start Identity",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/identity_documents.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "အသေးစိတ်", value:$"liketoknow_citizenship")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply3);
            }
            else if (text == "citizenship_parents_No")
            {
                var reply = context.MakeMessage();
                reply.Text = $"သင့်မိဘနှစ်ပါးအနက် တစ်ဦးသည် နိုင်ငံသားပြုခွင့်ရသူ (သို့) ဧည့်နိုင်ငံသားဖြစ်ပါသလား?";
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="citizenship_oneparent_Yes" },
                        new CardAction(){ Title = "No", Type=ActionTypes.ImBack, Value="citizenship_oneparent_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "citizenship_oneparent_Yes")
            {
                var reply = context.MakeMessage();
                reply.Text = $"ကျန်မိဘတစ်ပါသည် နိုင်ငံသားပြုခွင့်ရသူ (သို့) ဧည့်နိုင်ငံသားဖြစ်ပါသလား?";
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="citizenship_secondparent_Yes" },
                        new CardAction(){ Title = "No", Type=ActionTypes.ImBack, Value="citizenship_secondparent_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "citizenship_secondparent_Yes")
            {
                var reply = context.MakeMessage();
                reply.Text = $"သင့်မိဘနှစ်ဦးအနက် တစ်ဦး၏မိဘများသည် နိုင်ငံသားပြုခွင့်ရသူများ (သို့) ဧည့်နိုင်ငံသားမျာ ဖြစ်ကြပါသလား?";
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="citizenship_grandparents_Yes" },
                        new CardAction(){ Title = "No", Type=ActionTypes.ImBack, Value="citizenship_grandparents_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "citizenship_grandparents_No")
            {
                var reply = context.MakeMessage();
                reply.Text = $"သင့်မိဘနှစ်ပါးလုံးသည် ဧည့်နိုင်ငံသားများ ဖြစ်ကြပါသလား?";
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="citizenship_bothparent_Yes" },
                        new CardAction(){ Title = "No", Type=ActionTypes.ImBack, Value="citizenship_bothparent_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "citizenship_bothparent_Yes")
            {
                var reply = msg.CreateReply("သင်သည် ဧည့်နိုင်ငံသားတစ်ဦး ဖြစ်နိုင်သည်။");
                await context.PostAsync(reply);
                await context.PostAsync(msg.ResponseTyping());
                var reply4 = msg.CreateReply("ဧည့်နိုင်ငံသားတစ်ဦး ဖြစ်ရန်မှာ");
                await context.PostAsync(reply4);
                await context.PostAsync(msg.ResponseTyping());
                var reply5 = msg.CreateReply("၁၉၈၂ ခုနှစ် မတိုင်ခင်က နိုင်ငံသားဖြစ်ရန် လျှောက်ထားခဲ့ရမည်။");
                await context.PostAsync(reply5);
                await context.PostAsync(msg.ResponseTyping());
                var reply6 = msg.CreateReply("သင့်မိဘနှစ်ပါးလုံးသည် ဧည့်နိုင်ငံသားများဖြစ်ကြပြီး သင်နှင့်ပတ်သက်သည့် အသေးစိတ်အချက်အလက်များနှင့် သင်၏နာမည်တို့ကို သင်မိဘများ၏ စာရွက်စာတမ်းများ၌ ထည့်သွင့်ခဲ့ရမည်။");
                await context.PostAsync(reply6);
                await context.PostAsync(msg.ResponseTyping());
                var reply7 = msg.CreateReply("အကယ်၍ သင်သည် သင့်မိဘများကြောင့် ဧည့်နိုင်ငံသားအဖြစ်ကို ရရှိခဲ့ပါက ထိုဧည့်နိုင်ငံသားအဖြစ် ဆက်လက်ရပ်တည်နိုင်ရန် သင်အသက် ၁၈ နှစ်ပြည့်သည့်အခါ ဧည့်နိုင်ငံသားနှင့် ဆိုင်သည့် အခြားအချက်အလက်များကို ဖြည့်စွက်ပေးရပါမည်။");
                await context.PostAsync(reply7);
                await context.PostAsync(msg.ResponseTyping());
                var reply1 = msg.CreateReply("ကောင်းပြီ။ ဘာဆက်လုပ်သင့်ပါသလဲ?");
                await context.PostAsync(reply1);
                await context.PostAsync(msg.ResponseTyping());
                var reply3 = msg.CreateReply("အကယ်လို့ သင်ရဲ့နိုင်ငံသားဖြစ်မှုအနေအထားကို စိတ်ပူတယ်ဆိုရင် သင့်ကိုကူညီပေးနိုင်မယ့် ရှေ့နေကိုဖြစ်ဖြစ်၊ အခြားဝန်ဆောင်မှု ပေးနေတဲ့သူကိုဖြစ်ဖြစ် ဆက်သွယ်တာ အကောင်းဆုံးပါ။ သူတို့ကို ရှာဖို့ အခုပဲ လမ်းပြကို ဒေါင်းလုဒ်လုပ်လိုက်ပါ။");
                reply3.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply3.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"လမ်းပြ app ကိုသွင်းမည်",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/download_lannpya.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "အသေးစိတ်", value:$"downloadlp_justice")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = "အစသို့ပြန်သွားရန်",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lannpya_logo1.png") },
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack,"သွားမည်", value: $"🤔 စတင်မည်")
                          }
                      }.ToAttachment(),
                    new HeroCard
                      {
                          Title = $"Start Identity",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/identity_documents.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "အသေးစိတ်", value:$"liketoknow_citizenship")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply3);
            }
            else if (text == "citizenship_secondparent_No")
            {
                var reply = context.MakeMessage();
                reply.Text = $"သင့်မိဘနှစ်ဦးအနက် ကျန်တစ်ဦးသည် နိုင်ငံခြားသားဖြစ်ပါသလား?";
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="citizenship_seondforengner_Yes" },
                        new CardAction(){ Title = "No", Type=ActionTypes.ImBack, Value="citizenship_seondforengner_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
        }

        public async Task LearnMyRight_UN(string text = null, Activity msg = null, IDialogContext context = null)
        {
            if (msg.Text == "learnmyrights")
            {
                var reply = context.MakeMessage();
                reply.Text = "အောက်ပါပုံရှိမြှားကို နိပ်ပါ သို့မဟုတ် ပုံကိုဘေးသို့ဆွဲပါ သို့မဟုတ် ရှစ်ခုထဲက တစ်ခုကို ရွေးပါ";
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"ရာဇဝတ်ကြောင်းအရ စွပ်စွဲခံရသူ",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/criminal_accused.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "စွပ်စွဲခံရသူ", value:$"criminallyaccused_rights")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"တိုင်ကြားသူနှင့်မျက်မြင်သက်သေများ",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/complainant_and_witnesses.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "သက်သေ", value:$"complainantwitnesses_rights")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"အမျိုးသမီးအခွင့်အရေးများ",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/woman_rights.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "အမျိုးသမီး", value:$"womenrights")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"ကလေးသူငယ်အခွင့်အရေး",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/child_right.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "ကလေး", value:$"childrights")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"LGBTQI အခွင့်အရေးများ",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lgbtq_rights.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "LGBTQI", value:$"lgbtqirights")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"မြေယာအခွင့်အရေးများ",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/land_rights.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "မြေယာ", value:$"landrights")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"အလုပ်သမားအခွင့်အရေး",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/labour_rights.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "အလုပ်သမား", value:$"labourrights")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = "အစသို့ပြန်သွားရန်",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lannpya_logo1.png") },
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack,"သွားမည်", value: $"🤔 စတင်မည်")
                          }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "criminallyaccused_rights")
            {
                var reply = msg.CreateReply("ရာဇဝတ်မှုနဲ့ စွပ်စွဲခံထားရတဲ့သူမှာ ရှေ့နေငှားခွင့်ရှိတယ် ဆိုတာ သင်သိပါသလား?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="ရွေးပါ :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "သိတယ်", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Yes" },
                        new CardAction(){ Title = "တကယ်လား?", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Really" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "criminallyaccused_rights_Yes" || msg.Text == "criminallyaccused_rights_Really")
            {
                if (msg.Text == "criminallyaccused_rights_Really")
                {
                    await context.PostAsync("ဟုတ်ပါတယ်!");
                }
                await context.PostAsync("ရာဇဝတ်မှုနဲ့ စွပ်စွဲခံထားရသူတိုင်း ရှေ့နေဖြင့် ခုခံချေပခွင့်ရှိကြောင်း မြန်မာနိုင်ငံရဲ့ဖွဲ့စည်းပုံ အခြေခံဥပဒေပုဒ်မ ၃၇၅ က အာမခံချက်ပေးထား ပါတယ်။");
                var reply = msg.CreateReply("ပိုပြီးလေ့လာလိုပါသလား?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="ရွေးပါ :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "လေ့လာမယ်", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Learnmore_Yes" },
                        new CardAction(){ Title = "မလေ့လာဘူး", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Learnmore_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "criminallyaccused_rights_Learnmore_Yes")
            {
                await context.PostAsync("ကောင်းလိုက်တာ! လမ်းပြ App မှာ သင့်အခွင့်အရေး နဲ့ဆိုင်တဲ့ အကြောင်းအရာတွေ အများကြီး ပါတယ်နော်။");
                var reply = context.MakeMessage();
                reply.Text = "ပိုမိုလေ့လာဖို့ လမ်းပြ app ကို ဒေါင်းလုတ်ရယူ လိုက်ပါ";
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                    new HeroCard
                      {
                          Title = $"လမ်းပြ app ကိုသွင်းမည်",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/download_lannpya.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "သွင်းမည်", value:$"downloadlp_justice")
                    }
                      }.ToAttachment(),
                    new HeroCard
                      {
                          Title = "အစသို့ပြန်သွားရန်",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lannpya_logo1.png") },
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack,"သွားမည်", value: $"🤔 စတင်မည်")
                          }
                      }.ToAttachment(),
                    new HeroCard
                      {
                          Title = $"မိမိရပိုင်ခွင့်များ အကြောင်း ကိုပြန်သွားရန်",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/know_your_rights1.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "သွားမည်", value:$"learnmyrights")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "criminallyaccused_rights_Learnmore_No")
            {
                await context.PostAsync("လမ်းပြ App မှာ သင့်အခွင့်အရေး နဲ့ဆိုင်တဲ့ အကြောင်းအရာတွေ အများကြီး ပါတယ်နော်။");
                var reply1 = context.MakeMessage();
                reply1.Text = "ပိုမိုလေ့လာဖို့ လမ်းပြ app ကို ဒေါင်းလုတ်ရယူ လိုက်ပါ";
                await context.PostAsync(reply1);
                await context.PostAsync(msg.ResponseTyping());
                await context.PostAsync("အိုကေ နောက်မှတွေ့မယ်နော်");
                var reply = context.MakeMessage();
                reply.Text = "တာ့တာ !";
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = "အစသို့ပြန်သွားရန်",
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack,"သွားမည်", value: $"🤔 စတင်မည်")
                          }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = "လမ်းပြ app ကိုသွင့်မည်",
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack, "ဒေါင်းလုတ်", value: $"downloadlp_justice")
                          }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "complainantwitnesses_rights")
            {
                var reply = msg.CreateReply("သင်ဟာ ရာဇဝတ်မှုတစ်ခုခုကြောင့် နစ်နာဆုံးရှုံး သွားပြီဆိုရင် သင့်ရဲ့နစ်နာဆုံးရှုံးမှုတွေအတွက် လျော်ကြေးငွေကို တောင်းဆိုနိုင်တယ် ဆိုတာ သင်သိပါသလား?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="ရွေးပါ :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "သိတယ်", Type=ActionTypes.ImBack, Value="complainantwitnesses_rights_Yes" },
                        new CardAction(){ Title = "တကယ်လား?", Type=ActionTypes.ImBack, Value="complainantwitnesses_rights_Really" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "complainantwitnesses_rights_Yes" || msg.Text == "complainantwitnesses_rights_Really")
            {
                if (msg.Text == "complainantwitnesses_rights_Really")
                {
                    await context.PostAsync("ဟုတ်ပါတယ်!");
                }
                await context.PostAsync("ရာဇဝတ်ကျင့်ထုံးဥပဒေပုဒ်မ ၅၄၅ (က)-(ခ) အရ၊ တရားရုံးက စွပ်စွဲခံရသူကို ပြစ်မှုထင်ရှားစီရင်လိုက်ပြီ ဆိုရင်၊ တိုင်ကြားသူ တရားလိုက တရားစရိတ် အပါအဝင် အခြားဆုံးရှုံးမှုတွေအတွက် လျော်ကြေးငွေ ကိုလည်း");
                var reply = msg.CreateReply("ပိုပြီးလေ့လာလိုပါသလား?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="ရွေးပါ :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "လေ့လာမယ်", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Learnmore_Yes" },
                        new CardAction(){ Title = "မလေ့လာဘူး", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Learnmore_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "womenrights")
            {
                var reply = msg.CreateReply("သင့်ရဲ့ မိသားစုဝင် ဒါမှမဟုတ် သင့်ရဲ့ အနီးကပ် လက်တွဲဖော်က ဖြစ်ဖြစ် သင့်ကို ထိခိုက် နာကျင်အောင် လုပ်တာအပါအဝင် အမျိုးသမီးတွေကို ကျား - မ အခြေပြုအကြမ်းဖက်မှုတွေကနေ အကာအကွယ် ပေးထားတဲ့ ဥပဒေတွေ ရှိတယ်ဆိုတာ သင်သိပါသလား?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="ရွေးပါ :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "သိတယ်", Type=ActionTypes.ImBack, Value="womenrights_Yes" },
                        new CardAction(){ Title = "တကယ်လား?", Type=ActionTypes.ImBack, Value="womenrights_Really" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "womenrights_Yes" || msg.Text == "womenrights_Really")
            {
                if (msg.Text == "womenrights_Really")
                {
                    await context.PostAsync("ဟုတ်ပါတယ်!");
                }
                await context.PostAsync("ဥပမာ ရာဇသတ်ကြီးကို ပြင်ဆင်ထားတဲ့ဥပဒေ(၂၀၁၉) ခုနှစ်ရဲ့ ပုဒ်မ ၃၇၆(၂) အရ မုဒိမ်းမှုရဲ့ အဓိပ္ပါယ် ဖွင့်ဆိုချက်မှာ ယောကျ်ား တစ်ယောက်က သူ့ဇနီးကို သူမရဲ့ သဘောဆန္ဒ မပါပဲ လိင်ဆက်ဆံခြင်းပါ ပါဝင်လာ ပါတယ်။");
                var reply = msg.CreateReply("ပိုပြီးသိလိုပါသလား?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="ရွေးပါ :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "သိချင်တယ်", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Learnmore_Yes" },
                        new CardAction(){ Title = "မသိချင်ဘူး", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Learnmore_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "childrights")
            {
                var reply = msg.CreateReply("မြန်မာနိုင်ငံမှာရှိတဲ့ ကလေးငယ်တိုင်းဟာ မွေးဖွားသည်နှင့် ခွဲခြားဆက်ဆံမှု တစုံတရာမရှိစေပဲ မွေးဖွားခြင်းကို အခမဲ့ တရားဝင် မှတ်ပုံတင်ခွင့် ရှိတယ်ဆိုတာ သင်သိပါသလား?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="ရွေးပါ :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "သိတယ်", Type=ActionTypes.ImBack, Value="childrights_Yes" },
                        new CardAction(){ Title = "တကယ်လား?", Type=ActionTypes.ImBack, Value="childrights_Really" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "childrights_Yes" || msg.Text == "childrights_Really")
            {
                if (msg.Text == "childrights_Really")
                {
                    await context.PostAsync("ဟုတ်ပါတယ်!");
                }
                await context.PostAsync("အသစ်ပြဌာန်းလိုက်တဲ့ ကလေးသူငယ် အခွင့်အ‌ရေးများဆိုင်ရာဥပဒေပုဒ်မ ၂၁ အရ ကလေးသူငယ်ဆိုင်ဟာ မိမိရဲ့ မွေးဖွားခြင်းကို တရားဝင် မွေးစာရင်းမှာ မှတ်ပုံတင်သွင်းခွင့်ရှိပြီး၊ မှတ်ပုံတင်ပြီးတဲ့ အခါ မိဘ ဒါမဟုတ် အုပ်ထိန်းသူဟာ မွေးစာရင်းကို လက်ခံရရှိမှာဖြစ်ပါတယ်။");
                await context.PostAsync("မွေးဖွားခြင်းမှတ်ပုံတင်ဟာ (မွေးစာရင်း) ကျန်းမာရေး အထောက်အပံ့ရရှိဖို့၊ ကျောင်းအပ်ဖို့၊ ပစ္စည်းဥစ္စာနဲ့ မြေယာ ပိုင်ဆိုင်ခွင့်ရဖို့ အပါအဝင် အသက်အရွယ်အလိုက်ပေးအပ်ထားတဲ့ အခွင့်အရေး တွေကို ရရှိဖို့ အတွက် အထောက်အကူ ဖြစ်စေပါတယ်။");
                var reply = msg.CreateReply("ပိုပြီးသိလိုပါသလား?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="ရွေးပါ :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "သိချင်တယ်", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Learnmore_Yes" },
                        new CardAction(){ Title = "မသိချင်ဘူး", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Learnmore_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "lgbtqirights")
            {
                var reply = msg.CreateReply("မြန်မာနိုင်ငံမှာ မိမိကိုယ်မိမိ လိင်တူချစ်သူ (အမျိုးသား / အမျိုးသမီး)၊ ဆန့်ကျင်ဘက်လိင် အဖြစ် ပြောင်းလဲနေထိုင်သူတွေ၊ ဒွိလိင်ချစ်သူတွေ တနည်းအားဖြင့် LBGTQI တွေကို ခွဲခြား ဆက်ဆံရာမှာ အသုံးပြုတဲ့ ဥပဒေတွေ ရှိနေတယ် ဆိုတာ သင်သိပါသလား?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="ရွေးပါ :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "သိတယ်", Type=ActionTypes.ImBack, Value="lgbtqirights_Yes" },
                        new CardAction(){ Title = "တကယ်လား?", Type=ActionTypes.ImBack, Value="lgbtqirights_Really" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "lgbtqirights_Yes" || msg.Text == "lgbtqirights_Really")
            {
                if (msg.Text == "lgbtqirights_Really")
                {
                    await context.PostAsync("စိတ်မကောင်းပါဘူး၊ ဟုတ်ပါတယ်။");
                }
                await context.PostAsync("ဥပမာ၊ ရဲအရာရှိတွေက ရဲအက်ဥပ‌ဒေ (၁၉၄၅) ကိုသုံးပြီး LBGTQI အသိုင်းအဝန်းကို ပစ်မှတ်ထား လေ့ရှိပါသည်။");
                var reply = msg.CreateReply("ပိုပြီးသိလိုပါသလား?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="ရွေးပါ :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "သိချင်တယ်", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Learnmore_Yes" },
                        new CardAction(){ Title = "မသိချင်ဘူး", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Learnmore_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "landrights")
            {
                var reply = msg.CreateReply("အစိုးရအနေနဲ့ အများ ပြည်သူအကျိုးအတွက် သင့်ပိုင်တဲ့မြေကို ဥပဒေနဲ့အညီ သိမ်းလို့ရတယ်ဆိုတာ သင်သိပါသလား");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="ရွေးပါ :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "သိတယ်", Type=ActionTypes.ImBack, Value="landrights_Yes" },
                        new CardAction(){ Title = "တကယ်လား?", Type=ActionTypes.ImBack, Value="landrights_Really" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "landrights_Yes" || msg.Text == "landrights_Really")
            {
                if (msg.Text == "landrights_Really")
                {
                    await context.PostAsync("ဟုတ်ပါတယ်");
                }
                await context.PostAsync("အစိုးရအနေနဲ့ သင့်ရဲ့မြေကို သိမ်းယူပြီး အများပြည်သူ အကျိုးစီးပွားအတွက် အသုံးပြုနိုင်ပါတယ်။ သို့သော် မြေသိမ်းခံရသည့်အတွက် ကြုံတွေ့ခဲ့သော ဆုံးရှုံး နစ်နာမှုတွေအတွက် အစိုးရဆီကနေ လျော်ကြေး ရခွင့် ရှိပါတယ်။");
                var reply = msg.CreateReply("ပိုပြီးသိလိုပါသလား?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="ရွေးပါ :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "သိချင်တယ်", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Learnmore_Yes" },
                        new CardAction(){ Title = "မသိချင်ဘူး", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Learnmore_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "labourrights")
            {
                var reply = msg.CreateReply("အလုပ်သမားကို လုပ်ငန်းခွင်ဘေးအန္တရာယ် ကင်းရှင်းအောင်အကာအကွယ်ပေးရမယ့် တာဝန်ဟာ အလုပ်ရှင်မှာရှိတယ် ဆိုတာ သင်သိပါသလား?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="ရွေးပါ :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "သိတယ်", Type=ActionTypes.ImBack, Value="labourrights_Yes" },
                        new CardAction(){ Title = "တကယ်လား?", Type=ActionTypes.ImBack, Value="labourrights_Really" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "labourrights_Yes" || msg.Text == "labourrights_Really")
            {
                if (msg.Text == "labourrights_Really")
                {
                    await context.PostAsync("ဟုတ်ပါတယ်");
                }
                await context.PostAsync("လုပ်ငန်းခွင်ဘေးအန္တရာယ်ကင်းရှင်းရေးနှင့် ကျန်းမာရေး ဆိုင်ရာဥပဒေ(၂၀၁၉) ပုဒ်မ ၂၆ (ဃ) - (င)အရ အလုပ်ရှင်ဟာ မိမိအလုပ်သမားတွေ လုပ်ငန်းခွင် ဘေးအန္တရာယ်ကင်းရှင်းစေဖို့ အကာအကွယ်ပေးရမှာ ဖြစ်ပြီး ယင်းတွင် ကိုယ်ခန္ဓာကာကွယ်ရေးဝတ်စုံများ၊ ပစ္စည်းများနဲ့ အထောက်အကူပြုပစ္စည်းများကို အခမဲ့ လုံလောက်စွာထုတ်ပေးရမှာ ဖြစ်ပါတယ်။");
                var reply = msg.CreateReply("ပိုမိုသိရှိလိုပါသလား?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="ရွေးပါ :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "သိချင်တယ်", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Learnmore_Yes" },
                        new CardAction(){ Title = "မသိချင်ဘူး", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Learnmore_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }

        }

        public async Task HavingProblem_UN(string text = null, Activity msg = null, IDialogContext context = null)
        {
            if (text == "havingproblem")
            {
                var reply = context.MakeMessage();
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                       new HeroCard
                      {
                          Title = $"လမ်းပြ App ကို Install လုပ်ရန်/Update မြှင့်တင်ရန် အခက်အခဲ ရှိသည်။",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "အခက်အခဲ", value:$"device_problem")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"နည်းပညာအခက်အခဲနှင့် ကြုံတွေ့နေရသည်။",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "နည်းပညာ", value:$"technical_problem")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "device_problem")
            {
                var reply = context.MakeMessage();
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="သင့်ဖုန်းက Android ဖြစ်ပါသလား။",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "မှန်သည်။", Type=ActionTypes.ImBack, Value="android_problem_Yes" },
                        new CardAction(){ Title = "မှားသည်။", Type=ActionTypes.ImBack, Value="android_problem_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "android_problem_Yes")
            {
                var reply = context.MakeMessage();
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                       new HeroCard
                      {
                          Title = $"ကျွန်ုပ်ဖုန်းတွင် သိမ်းဆည်းစရာ နေရာအလုံအလောက်မရှိပါ။",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack,"နေရာအခက်အခဲ", value:$"space_problem")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"လမ်းပြ App ကို Update လုပ်၍ မရပါ။",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "မရပါ", value:$"updating_problem")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "space_problem")
            {
                var reply = msg.CreateReply("Setting ထဲမှ Apps ကိုသွား အသုံးမပြုသော App များကို Uninstall လုပ်ပြီး သိမ်းဆည်းစရာနေရာယူပါ။");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                       new HeroCard
                      {
                          Title = $"အဆင်မပြေဖြစ်နေသေးပါက Feedback ပေးပို့ပါ။",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/feedback.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "ပေးပို့မည်", value:$"http://lannpya.com/ForMessengerBot/SuggestionForm?MsgID={msg.From.Id}&UserName={msg.From.Name}")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = "အစသို့ပြန်သွားရန်",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lannpya_logo1.png") },
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack,"သွားမည်", value: $"🤔 စတင်မည်")
                          }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"လမ်းပြအကြောင်း ကို ကြည့်ရန်",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/about_lannpya_chatbot.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "ကြည့်မည်", value:$"aboutlannpya")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "updating_problem" || text == "appversion_problem")
            {
                var reply = context.MakeMessage();
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                       new HeroCard
                      {
                          Title = $"playstore သို့သွားမည်",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/go_to_playstore.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "သွားမည်", value:$"https://play.google.com/store/apps/details?id=com.koekoetech.myjustice")
                    }
                      }.ToAttachment(),
                       new HeroCard
                      {
                          Title = $"အဆင်မပြေဖြစ်နေသေးပါက Feedback ပေးပို့ပါ။",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/feedback.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "ပို့မည်", value:$"http://lannpya.com/ForMessengerBot/SuggestionForm?MsgID={msg.From.Id}&UserName={msg.From.Name}")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = "အစသို့ပြန်သွားရန်",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lannpya_logo1.png") },
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack,"သွားမည်", value: $"🤔 စတင်မည်")
                          }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Go to About Lannpya",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/about_lannpya_chatbot.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "သွားမည်", value:$"aboutlannpya")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "android_problem_No")
            {
                var reply = msg.CreateReply("လမ်းပြ App သည် Android device များတွင်သာ ရရှိနိုင်ဦးမည်။ သို့ရာတွင် လမ်းပြ Webpage မှာမူ မည်သည့်Operating system တွင်မဆို ကြည့်ရှုနိုင်ပြီဖြစ်သည်။");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                    new HeroCard
                      {
                          Title = $"Lannpya website သို့သွားမည်",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/go_to_lannpya_website.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "သွားမည်", value:$"https://www.lannpya.com/")
                    }
                      }.ToAttachment(),
                       new HeroCard
                      {
                          Title = $"အဆင်မပြေဖြစ်နေသေးပါက Feedback ပေးပို့ပါ။",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/feedback.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "ပို့မည်", value:$"http://lannpya.com/ForMessengerBot/SuggestionForm?MsgID={msg.From.Id}&UserName={msg.From.Name}")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = "အစသို့ပြန်သွားရန်",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lannpya_logo1.png") },
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack,"သွားမည်", value: $"🤔 စတင်မည်")
                          }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Go to About Lannpya",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/about_lannpya_chatbot.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "သွားမည်", value:$"aboutlannpya")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "technical_problem")
            {
                var reply = context.MakeMessage();
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                    new HeroCard
                      {
                          Title = $"(၁) App အတွင်း အဆင်မပြေမှုများ",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "အဆင်မပြေပါ", value:$"appcrashes_problem")
                    }
                      }.ToAttachment(),
                       new HeroCard
                      {
                          Title = $"(၂) ဘာသာစကားပြုပြင်ပြောင်းလဲလိုသည်။",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "ဘာသာစကား", value:$"language_problem")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"3. (၃) notification settings ကို ပြောင်းလဲချင်သည်။",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "အချက်ပြ", value:$"notification_problem")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"(၄) လမ်းပြ App လမ်းညွှန်မှ Phone ခေါ်၍ မရပါ။",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "မရပါ", value:$"phonecall_problem")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "appcrashes_problem")
            {
                var reply = context.MakeMessage();
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                    new HeroCard
                      {
                          Title = $"က-ဖုန်း၏ Android version သည် နောက်ဆုံးVersion ဟုတ်၊မဟုတ် စစ်ပါ။",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "စစ်မည်", value:$"androidversion_problem")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"ခ- လမ်းပြAppရဲ့ Version သည် နောက်ဆုံး Version ဖြစ်၊မဖြစ် စစ်ပါ။",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "စစ်မည်", value:$"appversion_problem")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "androidversion_problem" || text == "language_problem" || text == "notification_problem" || text == "phonecall_problem")
            {
                var reply = context.MakeMessage();
                if (text == "androidversion_problem") { reply.Text = "Phone setting ထဲမှ About phone ထဲမှ အနည်းဆုံး Android version 4.4 ဖြစ်၊မဖြစ်စစ်ပါ။"; }
                else if (text == "language_problem") { reply.Text = "က- လမ်းပြ App ကိုဖွင့်ပါ Setting ကိုသွားပါ(ညာဘက်ထောင့်စွန်းတွင်ရှိ) & ပထမဆုံး Option ကို ရွေးပါ:ထို့နောက် Language setting သို့သွားပါ။"; }
                else if (text == "notification_problem") { reply.Text = "က- လမ်းပြIconကို ကြာကြာနှိပ်ထားပါ။"; }
                else { reply.Text = "က- Phone setting ထဲမှ App setting ထဲမှ လမ်းပြ App ခွင့်ပြုချက်များ ဖုန်းခေါ်ဆိုခြင်း(Accept ကို နှိပ်ပါ။"; }
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                    new HeroCard
                      {
                          Title = $"အဆင်မပြေဖြစ်နေသေးပါက Feedback ပေးပို့ပါ။",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/feedback.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "ပေးပို့မည်", value:$"http://lannpya.com/ForMessengerBot/SuggestionForm?MsgID={msg.From.Id}&UserName={msg.From.Name}")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = "အစသို့ပြန်သွားရန်",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lannpya_logo1.png") },
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack,"သွားမည်", value: $"🤔 စတင်မည်")
                          }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Go to About Lannpya",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/about_lannpya_chatbot.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "သွားမည်", value:$"aboutlannpya")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
        }

        private async Task ResumeAfterCalculateTaxEngDialog(IDialogContext context, IAwaitable<object> result)
        {
            var taxName = taxinfo.Split('_')[1];
            var reply = context.MakeMessage();
            reply.Text = "အခွန်နဲ့ ပတ်သပ်ပြီး သင်သိသင့်သော အကြောင်းအရာအားလုံး ဒီမှာပါရှိပါတယ်။";
            reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
            reply.Attachments = new List<Attachment> {

                       new HeroCard
                      {
                          Title = $"သင့်ရဲ့ {taxName} အခွန် ကို ပေးဆောင်ပါ။",
                          Text = "ဒီမှာ သင့်အခွန်ကို ပေးဆောင်ဖို့ လွယ်ကူအဆင်ပြေေစေတဲ့ နည်းလမ်းပါ။ ကြည့်ကြည့်လိုက်ရအောင်။",
                          Images = new List<CardImage> { new CardImage(ResourceHelper.taxexpenditure_img_url) },
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack, "ပေးဆောင်မည်", value:$"paytax_{taxinfo}")
                          }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = "အခွန်သုံးစွဲစရိတ်",
                          //Text = "Learn about where your money is going?",
                          Text = "လူကြီးမင်းပိုက်ဆံ ဘယ်နေရာအတွက်အသုံးချနေလဲဆိုတာ လေ့လာမယ်။",
                          Images = new List<CardImage> { new CardImage(ResourceHelper.tax_img_url) },
                          Buttons = new List<CardAction>
                          {
                             new CardAction(ActionTypes.OpenUrl, "လေ့လာမယ်။", value: "http://google.com")
                          }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = "အစကပြန်စမယ်",
                          Text = "အခွန်အကြောင်း ပိုမိုလေ့လာမည်။",
                          Images = new List<CardImage> { new CardImage(ResourceHelper.tax_img_url) },
                          Buttons = new List<CardAction>
                          {
                             new CardAction(ActionTypes.ImBack, "🤔 ပြန်စမယ်", value: "🤔 ပြန်စမယ်")
                          }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = "အကူအညီ ရယူမည်။",
                          Text = "သင်ရှုပ်ထွေးနေလား။",
                          Images = new List<CardImage> { new CardImage(ResourceHelper.tax_img_url) },
                          Buttons = new List<CardAction>
                          {
                             new CardAction(ActionTypes.ImBack, "🙋‍ အကူအညီ", value: "🙋‍ အကူအညီ")
                          }
                      }.ToAttachment()

                  };
            await context.PostAsync(reply);
        }
        private async Task ResumeAfterEngFFDialog(IDialogContext context, IAwaitable<object> result)
        {
            var reply = context.MakeMessage();
            reply.Text = "တခြားသိလိုသည့်အကြောင်းအရာများရှိသေးလား! ဘာကူညီပေးရမလဲ?";
            reply.Type = ActivityTypes.Message;
            reply.TextFormat = TextFormatTypes.Plain;
            reply.SuggestedActions = new SuggestedActions()
            {
                Actions = (new List<string> { "🤔 ပြန်စမယ်", "🙋‍ အကူအညီ" }).Select(a => new CardAction
                {
                    Title = a,
                    Type = ActionTypes.ImBack,
                    Value = a
                }).ToList()
            };

            await context.PostAsync(reply);
            context.Wait(MessageReceivedAsync);
        }
        private async Task ResumeAfterZGFFDialog(IDialogContext context, IAwaitable<object> result)
        {
            var reply = context.MakeMessage();
            reply.Text = "ဘာကူညီေပးရပါမလဲ ခင္ဗ်ာ";
            reply.Type = ActivityTypes.Message;
            reply.TextFormat = TextFormatTypes.Plain;
            reply.SuggestedActions = new SuggestedActions()
            {
                Actions = (new List<string> { "🤔 ျပန္စမည္" }).Select(a => new CardAction
                {
                    Title = a,
                    Type = ActionTypes.ImBack,
                    Value = a
                }).ToList()
            };

            await context.PostAsync(reply);
            context.Wait(MessageReceivedAsync);
        }
        //private async Task ResumeAfterPayPropertyTaxEngDialog(IDialogContext context, IAwaitable<object> result)
        //{

        //    var argument = await result as PayTaxEngFormFlow_UN;

        //    var reply = context.MakeMessage();
        //    reply.Text = "သင်သိသော အခွန်ဆိုင်ရာ အကြောင်းအရာများ။";
        //    reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
        //    reply.Attachments = new List<Attachment> {
        //              new HeroCard
        //              {
        //                    Title = "MPU ဖြင့်ပေးချေမည်။",
        //                    Subtitle = $"{argument.TaxName} အတွက်ပေးချေမှု စတင်မည်။",
        //                    Text = "စတင်မယ်",
        //                    Images = new List<CardImage> { new CardImage(ResourceHelper.mpu_img_url) },
        //                    Buttons = new List<CardAction>
        //                    {
        //                        new CardAction(ActionTypes.OpenUrl, "ပေးဆောင်မည်", value:$"http://kktmpupayment.azurewebsites.net/mpupayment/index?userdefined1={argument.TaxId}&amount={argument.Amount}&productDescription={argument.TaxName}&categorycode={argument.UserId}&userdefined2={context.Activity.From.Id}"),
        //                        new CardAction(ActionTypes.OpenUrl, "ဆက်သွယ်ရန်", value:"tel:+95-9796926583")
        //                    }
        //              }.ToAttachment(),
        //              new HeroCard
        //              {
        //                    Title = "Wave Money ဖြင့်ပေးချေမည်။",
        //                    Subtitle = $"{argument.TaxName} အတွက်ပေးချေမှု စတင်မည်။",
        //                    Text = "စတင်မယ်",
        //                    Images = new List<CardImage> { new CardImage(ResourceHelper.wavemoney_img_url) },
        //                    Buttons = new List<CardAction>
        //                    {
        //                        new CardAction(ActionTypes.OpenUrl, "ပေးဆောင်မည်", value:$"http://kktmpupayment.azurewebsites.net/mpupayment/index?userdefined1={argument.TaxId}&amount={argument.Amount}&productDescription={argument.TaxName}&categorycode={argument.UserId}&userdefined2={context.Activity.From.Id}"),
        //                        new CardAction(ActionTypes.OpenUrl, "ဆက်သွယ်ရန်", value:"tel:+95-9796926583")
        //                    }
        //              }.ToAttachment(),

        //          };

        //    await context.PostAsync(reply);
        //    context.Wait(MessageReceivedAsync);
        //}
    }
}