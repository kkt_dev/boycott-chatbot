﻿using LannpyaBot.Helper;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace LannpyaBot.Dialogs
{
    [Serializable]
    public class MenuEngDialog : IDialog<object>
    {
        //string city;
        public async Task StartAsync(IDialogContext context)
        {
            var obj = new StartDialog();
            await obj.BusinessLogic(context);
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<object> result)
        {
            var msg = await result as Activity;
            if (msg.Text.isStart_words_UN())
            {
                await context.Forward(new StartDialog_UN(), this.ResumeAfterUNFFDialog, msg, CancellationToken.None);
                return;
            }
            else if (msg.Text.isStart_words())
            {
                await context.Forward(new StartDialog(), this.ResumeAfterEngFFDialog, msg, CancellationToken.None);
                return;
            }

            context.Wait(MessageReceivedAsync);
        }
        private async Task ResumeAfterUNFFDialog(IDialogContext context, IAwaitable<object> result)
        {
            var reply = context.MakeMessage();
            reply.Text = "ဘာကူညီပေးရပါမလဲ ရှင့်";
            reply.Type = ActivityTypes.Message;
            reply.TextFormat = TextFormatTypes.Plain;
            reply.SuggestedActions = new SuggestedActions()
            {
                Actions = (new List<string> { "🤔 ပြန်စမည်" }).Select(a => new CardAction
                {
                    Title = a,
                    Type = ActionTypes.ImBack,
                    Value = a
                }).ToList()
            };

            await context.PostAsync(reply);
            context.Wait(MessageReceivedAsync);
        }

        private async Task ResumeAfterEngFFDialog(IDialogContext context, IAwaitable<object> result)
        {
            var reply = context.MakeMessage();
            reply.Text = "How can I help you?";
            reply.Type = ActivityTypes.Message;
            reply.TextFormat = TextFormatTypes.Plain;
            reply.SuggestedActions = new SuggestedActions()
            {
                Actions = (new List<string> { "🤔 ပြန်စမည်" }).Select(a => new CardAction
                {
                    Title = a,
                    Type = ActionTypes.ImBack,
                    Value = a
                }).ToList()
            };

            await context.PostAsync(reply);
            context.Wait(MessageReceivedAsync);
        }
    }
}