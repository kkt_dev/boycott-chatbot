﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Connector;
using LannpyaBot.Helper;
using LannpyaBot.Models;

namespace LannpyaBot.Dialogs
{
    [Serializable]
    public class StartDialog : IDialog<object>
    {
        string city, taxinfo;
        int cityid;
        public async Task StartAsync(IDialogContext context)
        {


            context.Wait(MessageReceivedAsync);

            //return Task.CompletedTask;
        }

        public async Task MessageReceivedAsync(IDialogContext context, IAwaitable<object> result)
        { 
            await BusinessLogic(context);
        }

        public async Task BusinessLogic(IDialogContext context)
        {
            Activity msg = context.Activity as Activity;
            
            if (msg.Text.isWelcome_words())
            {
                var reply = msg.CreateReply($"Hi {msg.From.Name}! Welcome to Lann Pya. I'm here to help you learn about your rights. \nမင်္ဂလာပါ။ လမ်းပြက ကြိုဆိုပါတယ်။ သင့်ရဲ့ အခွင့်အရေးတွေကို လေ့လာနိုင်ဖို့ ကူညီပေးပါရစေ။");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                     new HeroCard
                      {
                          Title = "How may I help you? :)",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lanpya-girl_with_text.png") },
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack,"🤔 Get started", value: $"🤔 Get started"),
                                  new CardAction(ActionTypes.ImBack,"🤔 စတင်မည်", value: $"🤔 စတင်မည်")
                          }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "🙋‍ Get Help")
            {
                var reply = context.MakeMessage();
                reply.Text = "Everything about Tax you should know.";
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments.Add(
                      new HeroCard
                      {
                          Title = "Get support now",
                          Text = $"Let us know your feedback and call us for support",
                          Images = new List<CardImage> { new CardImage(ResourceHelper.tax_img_url) },
                          Buttons = new List<CardAction>
                          {
                              new CardAction(ActionTypes.ImBack, "Complaint or Suggestion", value: "Suggestion"),
                              new CardAction(ActionTypes.OpenUrl, "Call now", value: "tel:+95-9-796926583")
                          }
                      }.ToAttachment());
                await context.PostAsync(reply);
            }
            ////else if (msg.Text.ToLower().isFeedback_words())
            ////{
            ////    var form = new FormDialog<SuggestionEngFormFlowDialog>(new SuggestionEngFormFlowDialog(), () => SuggestionEngFormFlowDialog.BuildForm(msg.From.Name, msg.From.Id), FormOptions.None, null);
            ////    await context.Forward(form, ResumeAfterEngFFDialog, msg, CancellationToken.None);
            ////    return;
            ////}
            ////else if (msg.Text.isStart_words_UN())
            ////{
            ////    await context.Forward(new StartDialog_UN(), this.ResumeAfterUNFFDialog, msg, CancellationToken.None);
            ////    return;
            ////}
            else if (msg.Text.isStart_words())
            {
                var reply = context.MakeMessage();
                reply.Text = "Please swipe or click on the arrow below to select from one of 3 options.";
                //reply.Text = "Please select from the following 3 options";
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                     new HeroCard
                     {
                          Title = $"Get Help!",
                        Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/get_help.png") },
                        Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Help", value:$"gethelp")
                     }
                      }.ToAttachment(),
                       new HeroCard
                      {
                          Title = $"Learn my rights",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/know_your_rights1.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Rights", value:$"learnmyrights")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"About LannPya",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/about_lannpya_chatbot.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "LannPya", value:$"aboutlannpya")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "gethelp")
            {
                var reply1 = msg.CreateReply("What kind of help do you need?");
                await context.PostAsync(reply1);
                await context.PostAsync(msg.ResponseTyping());
                var reply = msg.CreateReply("Please swipe or click on the arrow below to select from one of 7 options.");
                //var reply = msg.CreateReply($"Please select from the following 6 options");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"Land",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/see_land.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Land", value:$"landmap")
                    }
                      }.ToAttachment(),
                    //   new HeroCard
                    //  {
                    //      Title = $"Report Corruption",
                    //Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/report_corruption.png") },
                    //Buttons = new List<CardAction>
                    //      {
                    //    new CardAction(ActionTypes.ImBack, "Corruption", value:$"corruption")
                    //}
                    //  }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Criminal Justice",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/criminal_justice.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Criminal", value:$"criminaljustice")
                    }
                      }.ToAttachment(),
                    //  new HeroCard
                    //  {
                    //      Title = $"Family Problems",
                    //Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/family_problem.png") },
                    //Buttons = new List<CardAction>
                    //      {
                    //    new CardAction(ActionTypes.ImBack, "Family", value:$"familyproblems")
                    //}
                    //  }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"How to work with Lawyers",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/how_to_work_with_lawyer.png") },
                    Buttons = new List<CardAction>
                          {
                        //new CardAction(ActionTypes.OpenUrl, "Lawyers", value:$"https://lannpya.com/LawyerDirectory")
                        new CardAction(ActionTypes.ImBack, "Lawyers", value:"worklawyers")
                    }
                      }.ToAttachment(),
                      new HeroCard
                     {
                          Title = $"Identity Documents",
                        Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/identity_documents.png") },
                        Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Identity", value:$"citizenship_dt")
                     }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Migration",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/migration.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Migration", value:$"migration")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Find Laws",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/find_law.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Laws", value:$"findlaws")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Feedback",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/feedback.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "Feedback", value:$"http://lannpya.com/ForMessengerBot/SuggestionForm?MsgID={msg.From.Id}&UserName={msg.From.Name}")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "corruption" || msg.Text == "familyproblems")
            {
                await context.PostAsync("This feature is coming soon. We are working on it");
                var reply = context.MakeMessage();
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                     new HeroCard
                      {
                          Title = "How may I help you? :)",
                          //Text = Rabbit.Uni2Zg("How may I help you? :)"),
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lanpya-girl_with_text.png") },
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack,"🤔 Get started", value: $"🤔 Get started")
                          }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "findlaws")
            {
                var reply = msg.CreateReply($"If you are looking for a specific Myanmar law, the Union Attorney General’s office has an online searchable database including the constitution, laws, regulations, and rulings.");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                    new HeroCard
                      {
                          Text = $"Great! I'd like to search for a law on the database.",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Search database", value:$"searchdatabase")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Back to main menu",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Menu", value:$"🤔 Get started")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "searchdatabase")
            {
                var reply = msg.CreateReply($"You can access the online database on the Myanmar Law Information System (MLIS) website");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                    new HeroCard
                      {
                          Title = $"Go to website",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "Go", value:$"https://www.mlis.gov.mm/")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Back to main menu",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Menu", value:$"🤔 Get started")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "aboutlannpya")
            {
                var reply1 = msg.CreateReply($"Want to know more about the app? What would you like to know ? ");
                await context.PostAsync(reply1);
                var reply = msg.CreateReply("Please swipe or click on the arrow below to select from one of 6 options.");
                await context.PostAsync(msg.ResponseTyping());
                //var reply = msg.CreateReply($"Please select from the following 5 options");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                    new HeroCard
                      {
                          Title = $"About Us",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "About us", value:$"aboutus")
                    }
                      }.ToAttachment(),
                       new HeroCard
                      {
                          Title = $"How to download",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Download help", value:$"howtodownload")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"I'm having problems with the app",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Problems", value:$"havingproblem")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Register on the app directory",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Register", value:$"registerapp")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Privacy policy",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Privacy", value:$"privacypolicy")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Back to Lann Pya",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Start over", value:$"🤔 Get started")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "registerapp")
            {
                var reply = context.MakeMessage();
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                       new HeroCard
                      {
                          Title = $"I'm a lawyer.",
                          Text="Click the link below to register",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "Register as Lawyer", value:$"https://docs.google.com/forms/d/e/1FAIpQLSdUAtT4885dPbgxMK4o4fOk2M6tvnJq1nQUZBEWiqElBGqFaA/viewform")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"I am a CSO representative",
                          Text="Click the link below to register",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "Register as CSO", value:$"https://docs.google.com/forms/d/e/1FAIpQLSdSk6S4VuPu2Bu3GBXCeaTSP5hAGotnHD3b6mwCp5DZBz6lhg/viewform")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Feedback",
                          Text="I'm experiencing difficulty completing the form online.",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "Feedback", value:$"http://lannpya.com/ForMessengerBot/SuggestionForm?MsgID={msg.From.Id}&UserName={msg.From.Name}")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Main menu",
                          Text="Go back to main menu",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Menu", value:$"🤔 Get started")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "privacypolicy")
            {
                var reply1 = msg.CreateReply("Lann Pya is dedicated to protecting your privacy.");
                await context.PostAsync(reply1);
                await context.PostAsync(msg.ResponseTyping());
                var reply2 = msg.CreateReply("However, Facebook may collect your data so we cannot guarantee the security of any communication sent to us through Facebook messenger.");
                await context.PostAsync(reply2);
                await context.PostAsync(msg.ResponseTyping());
                var reply = msg.CreateReply($"Would you like to learn more about our privacy policies?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                    new HeroCard
                      {
                          Title = $"Yes, I'd like to learn more.",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "Privacy", value:$"https://lannpya.com/TermsandConditions")
                    }
                      }.ToAttachment(),
                       new HeroCard
                      {
                          Title = $"No, thanks. Please take me back to the main menu.",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Menu", value:$"🤔 Get started")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "howtodownload")
            {
                var reply1 = msg.CreateReply("There are many ways to download Lann Pya on your phone. We suggest using Facebook or Google Play Store.");
                await context.PostAsync(reply1);
                await context.PostAsync(msg.ResponseTyping());
                var reply = msg.CreateReply($"Step-by-step instructions");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                       new HeroCard
                      {
                          Title = $"Download from Playstore",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/googleplaystore.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "How to downlaod", value:$"downloadfromplaystore")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Download from Facebook",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/facebook.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "How to download", value:$"downloadfromfacebook")
                    }
                      }.ToAttachment()
                    //   new HeroCard
                    //  {
                    //      Title = $"Download from Playstore",
                    //Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/a4-how-to-download-on-play-store.jpg") },
                    //Buttons = new List<CardAction>
                    //      {
                    //    new CardAction(ActionTypes.OpenUrl, "Detail", value:$"https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/a4-how-to-download-on-play-store.jpg"),
                    //    new CardAction(ActionTypes.ImBack, "How to downlaod", value:$"downloadfromplaystore")
                    //}
                    //  }.ToAttachment(),
                    //  new HeroCard
                    //  {
                    //      Title = $"Download from Facebook",
                    //Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/a4-how-to-download.jpg") },
                    //Buttons = new List<CardAction>
                    //      {
                    //    new CardAction(ActionTypes.OpenUrl, "Detail", value:$"https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/a4-how-to-download.jpg"),
                    //    new CardAction(ActionTypes.ImBack, "How to download", value:$"downloadfromfacebook")
                    //}
                    //  }.ToAttachment()
                };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "aboutus")
            {
                var reply = context.MakeMessage();
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                       new HeroCard
                      {
                          Title = $"LannPya",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lannpya.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Lannpya", value:$"about_lannpya")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"MyJustice",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/mj_logo.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "MyJustice", value:$"about_myjusstice")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Koe Koe Tech",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/kkt_logo.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Koe Koe Tech", value:$"about_koekoetech")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Acknowledgements",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/acknowledgement.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Acknowledgements", value:$"about_acknowledgements")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "about_lannpya")
            {
                var reply1 = msg.CreateReply("LannPya is the result of an innovative collaboration between MyJustice and Koe Koe Tech that we hope will help to inform, connect, and empower the people of Myanmar, and build trust between communities, governmental and non - governmental legal service providers.");
                await context.PostAsync(reply1);
                await context.PostAsync(msg.ResponseTyping());
                var reply2 = msg.CreateReply("LannPya can help democratize access to essential information about law and rights and connect justice seekers to lawyers and governmental and non - governmental institutions that can help them.");
                await context.PostAsync(reply2);
                await context.PostAsync(msg.ResponseTyping());
                var reply3 = msg.CreateReply("LannPya means Show the Way in Myanmar language, and that’s what we hope this app can do: help to show the way to a more just, equitable, and democratic Myanmar.");
                await context.PostAsync(reply3);
                await context.PostAsync(msg.ResponseTyping());
                var reply = msg.CreateReply("Download LannPya today!");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"Download Lannpya",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Download", value:$"downloadlp_justice")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Back to Lann Pya",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Menu", value:$"🤔 Get started")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "about_myjusstice")
            {
                var reply1 = msg.CreateReply("MyJustice aims to equip the people of Myanmar with knowledge, confidence and opportunities so that they can resolve conflicts fairly, equitably and justly.");
                await context.PostAsync(reply1);
                await context.PostAsync(msg.ResponseTyping());
                var reply2 = msg.CreateReply("We work closely with local communities to achieve a lasting impact on the way disputes are resolved and justice is delivered, especially for poor, vulnerable and marginalised people.");
                await context.PostAsync(reply2);
                await context.PostAsync(msg.ResponseTyping());
                var reply3 = msg.CreateReply("MyJustice is funded by the European Union and implemented by the British Council.");
                await context.PostAsync(reply3);
                await context.PostAsync(msg.ResponseTyping());
                var reply4 = msg.CreateReply("The Lann Pya App is an innovative collaboration between MyJustice and Koe Koe Tech.");
                await context.PostAsync(reply4);
                await context.PostAsync(msg.ResponseTyping());
                var reply = msg.CreateReply("Download LannPya today!");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"Download Lannpya",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Download", value:$"downloadlp_justice")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Go to MyJustice",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "MyJustice", value:$"https://www.myjusticemyanmar.org/")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Back to Lann Pya",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Menu", value:$"🤔 Get started")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "about_koekoetech")
            {
                var reply1 = msg.CreateReply("Koe Koe Tech is a Myanmar IT Social Enterprise which focuses on developing software to address key social issues in the fields of Health, Law and Governance, with considerable experience and an intimate knowledge of the local cultural context. Founded in 2013 by Dr. Yar Zar Minn Htoo and Michael Lwin Esq., KKT employs 145 staff, 92.4% of whom are Myanmar nationals, with more than two-thirds female. We are intentionally Myanmar-centric and diverse by design: we believe that the best solutions are locally made and driven, and that diversity is a strength for Myanmar, not a weakness.");
                await context.PostAsync(reply1);
                await context.PostAsync(msg.ResponseTyping());
                var reply2 = msg.CreateReply("TheOur mission is to develop Myanmar’s vital IT infrastructure, taking advantage of the latest technology to enable Myanmar to leap forward in its development. Our vision is a Myanmar in which citizens, government, business and NGOs are all digitally connected and can benefit from the latest technology to enhance their work and further connect up a rapidly developing country.");
                await context.PostAsync(reply2);
                await context.PostAsync(msg.ResponseTyping());
                var reply = msg.CreateReply("Download LannPya today!");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"Download Lannpya",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Download", value:$"downloadlp_justice")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Go to Koe Koe Tech",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "Koe Koe Tech", value:$"https://www.koekoetech.com/")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Back to Lann Pya",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Menu", value:$"🤔 Get started")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "about_acknowledgements")
            {
                var reply1 = msg.CreateReply("The Lann Pya Access to Justice Mobile Application was developed with the generous support of the European Union through the MyJustice Programme implemented by the British Council in partnership with social enterprise Koe Koe Tech Co.Ltd.");
                await context.PostAsync(reply1);
                await context.PostAsync(msg.ResponseTyping());
                var reply2 = msg.CreateReply("MyJustice and Koe Koe Tech were supported by numerous governmental and non - governmental organisations and individuals in the development of the LannPya application");
                await context.PostAsync(reply2);
                await context.PostAsync(msg.ResponseTyping());
                var reply3 = msg.CreateReply("We also greatly appreciate the many individuals who provided user testing and feedback on content to strengthen the usefulness of the app for ordinary people.");
                await context.PostAsync(reply3);
                await context.PostAsync(msg.ResponseTyping());
                var reply4 = msg.CreateReply("In particular, we wish to acknowledge the contributions of: Union Attorney General’s Office, Office of the Supreme Court, Myanmar Police Force, International Development Law Organization(IDLO), International Labour Organization(ILO), International Organization for Migration(IOM), United Nations Population Fund(UNFPA), Independent Lawyers’ Association of Myanmar, Mandalay Bar Association, Mawlamyaing Bar Association, Shan State Bar Association, Yangon Bar Association, Sagaing Region Legal Aid Board, Mandalay Region Legal Aid Board, Bago Region Legal Aid Board, Mon State Legal Aid Board, Yangon Justice Affairs and Rule of Law Centres Coordination Body, International Bridges to Justice (IBJ), Mawlamyaing Justice Centre, Norwegian Refugee Council(NRC), Yangon Justice Centre, Myanmar Book Aid & amp; Preservation Foundation, Eden Ministry, Justice Base, Braveheart Foundation, and Azuri Creative.");
                await context.PostAsync(reply4);
                await context.PostAsync(msg.ResponseTyping());
                var reply = msg.CreateReply("Download LannPya today!");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"Download Lannpya",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Download", value:$"downloadlp_justice")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Back to Lann Pya",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Menu", value:$"🤔 Get started")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text.Contains("problem"))
            {
                await HavingProblem(msg.Text, msg, context);
            }
            else if (msg.Text == "downloadfromplaystore")
            {
                var reply1 = msg.CreateReply("Step One: Look for the Google Play Store on your phone.");
                await context.PostAsync(reply1);
                await context.PostAsync(msg.ResponseTyping());
                var reply = msg.CreateReply("Remember, to use Google Play Store, you need to be connected to the internet. You also must be signed into your Gmail account on the Play Store app.");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"Playstore",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/googleplaystore.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Next", value:$"stepone_dl_ps")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "stepone_dl_ps")
            {
                var reply = msg.CreateReply("Step Two: Type 'Lann Pya' into the search box at the top of the app screen.");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"Search in Playstore",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/g1.PNG") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Next", value:$"steptwo_dl_ps")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "steptwo_dl_ps")
            {
                var reply = msg.CreateReply("Step Three: Click on Lann Pya in the search results, then click “Install” to begin downloading the app on your phone.");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"Found in Playstore",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/g2.PNG") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Next", value:$"stepthree_dl_ps")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "stepthree_dl_ps")
            {
                var reply = msg.CreateReply("Step Four: After it has finished downloading, you will now have Lann Pya on your phone! Click the Lann Pya icon to use the app.");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"Install Lannpya",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lannpya_logo1.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Done", value:$"fisnished_download")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "downloadfromfacebook")
            {
                var reply = context.MakeMessage();
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"Facebook",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/facebook.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Next", value:$"stepone_dl_fb")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "stepone_dl_fb")
            {
                var reply = msg.CreateReply("Step One: Visit our Facebook page by typing “Lann Pya” into the Facebook search box.");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"Search in Facebook",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/f1.PNG") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Next", value:$"steptwo_dl_fb")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "steptwo_dl_fb")
            {
                var reply = msg.CreateReply("Step Two: Click the “Use app” button located near the top on our Lann Pya Facebook page to begin downloading the app onto your phone.");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"Found in Facebook",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/f2.PNG") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Next", value:$"stepthree_dl_fb")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "stepthree_dl_fb")
            {
                var reply = msg.CreateReply("Step Three: After it has finished downloading, click “Install.” You can now use Lann Pya on your phone!");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"Install Lannpya",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lannpya_logo1.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Done", value:$"fisnished_download")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "fisnished_download")
            {
                var reply = msg.CreateReply("Thank you for downloading Lannpya :)");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"Learn how to download again",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Go", value:$"howtodownload")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Back to main menu",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Menu", value:$"🤔 Get started")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            context.Wait(MessageReceivedAsync);
        }

        //private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<object> result)
        //{
        //    //Chatbot_KPI cbkpi = new Chatbot_KPI();
        //    //string[] evt = new MyExtenstion().GetEvent(msg.Text);
        //    //cbkpi.EventTitle = evt[0];   
        //    Activity msg = await result as Activity;           
        //    if (msg.Text.isWelcome_words())
        //    {
        //        var reply = msg.CreateReply($"Hi {msg.From.Name}! Welcome to Lann Pya. I'm here to help you learn about your rights. \nမင်္ဂလာပါ။ လမ်းပြက ကြိုဆိုပါတယ်။ သင့်ရဲ့ အခွင့်အရေးတွေကို လေ့လာနိုင်ဖို့ ကူညီပေးပါရစေ။");
        //        reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
        //        reply.Attachments = new List<Attachment> {
        //             new HeroCard
        //              {
        //                  Title = "How may I help you? :)",
        //                  Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lanpya-girl_with_text.png") },
        //                  Buttons = new List<CardAction>
        //                  {
        //                          new CardAction(ActionTypes.ImBack,"🤔 Get started", value: $"🤔 Get started"),
        //                          new CardAction(ActionTypes.ImBack,"🤔 စတင်မည်", value: $"🤔 စတင်မည်")
        //                  }
        //              }.ToAttachment()
        //          };
        //        await context.PostAsync(reply);
        //    }
        //    else if (msg.Text == "🙋‍ Get Help")
        //    {
        //        var reply = context.MakeMessage();
        //        reply.Text = "Everything about Tax you should know.";
        //        reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
        //        reply.Attachments.Add(
        //              new HeroCard
        //              {
        //                  Title = "Get support now",
        //                  Text = $"Let us know your feedback and call us for support",
        //                  Images = new List<CardImage> { new CardImage(ResourceHelper.tax_img_url) },
        //                  Buttons = new List<CardAction>
        //                  {
        //                      new CardAction(ActionTypes.ImBack, "Complaint or Suggestion", value: "Suggestion"),
        //                      new CardAction(ActionTypes.OpenUrl, "Call now", value: "tel:+95-9-796926583")
        //                  }
        //              }.ToAttachment());
        //        await context.PostAsync(reply);
        //    }
        //    ////else if (msg.Text.ToLower().isFeedback_words())
        //    ////{
        //    ////    var form = new FormDialog<SuggestionEngFormFlowDialog>(new SuggestionEngFormFlowDialog(), () => SuggestionEngFormFlowDialog.BuildForm(msg.From.Name, msg.From.Id), FormOptions.None, null);
        //    ////    await context.Forward(form, ResumeAfterEngFFDialog, msg, CancellationToken.None);
        //    ////    return;
        //    ////}
        //    ////else if (msg.Text.isStart_words_UN())
        //    ////{
        //    ////    await context.Forward(new StartDialog_UN(), this.ResumeAfterUNFFDialog, msg, CancellationToken.None);
        //    ////    return;
        //    ////}
        //    else if (msg.Text.isStart_words())
        //    {
        //        var reply = context.MakeMessage();
        //        reply.Text = "Please swipe or click on the arrow below to select from one of 3 options.";
        //        //reply.Text = "Please select from the following 3 options";
        //        reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
        //        reply.Attachments = new List<Attachment> {
        //             new HeroCard
        //             {
        //                  Title = $"Get Help!",
        //                Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/get_help.png") },
        //                Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Help", value:$"gethelp")
        //             }
        //              }.ToAttachment(),
        //               new HeroCard
        //              {
        //                  Title = $"Learn my rights",
        //            Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/know_your_rights1.png") },
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Rights", value:$"learnmyrights")
        //            }
        //              }.ToAttachment(),
        //              new HeroCard
        //              {
        //                  Title = $"About LannPya",
        //            Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/about_lannpya_chatbot.png") },
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "LannPya", value:$"aboutlannpya")
        //            }
        //              }.ToAttachment()
        //          };
        //        await context.PostAsync(reply);
        //    }
        //    else if (msg.Text == "gethelp")
        //    {
        //        var reply1 = msg.CreateReply("What kind of help do you need?");
        //        await context.PostAsync(reply1);
        //        await context.PostAsync(msg.ResponseTyping());
        //        var reply = msg.CreateReply("Please swipe or click on the arrow below to select from one of 7 options.");
        //        //var reply = msg.CreateReply($"Please select from the following 6 options");
        //        reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
        //        reply.Attachments = new List<Attachment> {
        //              new HeroCard
        //              {
        //                  Title = $"Land",
        //            Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/see_land.png") },
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Land", value:$"landmap")
        //            }
        //              }.ToAttachment(),
        //            //   new HeroCard
        //            //  {
        //            //      Title = $"Report Corruption",
        //            //Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/report_corruption.png") },
        //            //Buttons = new List<CardAction>
        //            //      {
        //            //    new CardAction(ActionTypes.ImBack, "Corruption", value:$"corruption")
        //            //}
        //            //  }.ToAttachment(),
        //              new HeroCard
        //              {
        //                  Title = $"Criminal Justice",
        //            Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/criminal_justice.png") },
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Criminal", value:$"criminaljustice")
        //            }
        //              }.ToAttachment(),
        //            //  new HeroCard
        //            //  {
        //            //      Title = $"Family Problems",
        //            //Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/family_problem.png") },
        //            //Buttons = new List<CardAction>
        //            //      {
        //            //    new CardAction(ActionTypes.ImBack, "Family", value:$"familyproblems")
        //            //}
        //            //  }.ToAttachment(),
        //              new HeroCard
        //              {
        //                  Title = $"How to work with Lawyers",
        //            Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/how_to_work_with_lawyer.png") },
        //            Buttons = new List<CardAction>
        //                  {
        //                //new CardAction(ActionTypes.OpenUrl, "Lawyers", value:$"https://lannpya.com/LawyerDirectory")
        //                new CardAction(ActionTypes.ImBack, "Lawyers", value:"worklawyers")
        //            }
        //              }.ToAttachment(),
        //              new HeroCard
        //             {
        //                  Title = $"Identity Documents",
        //                Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/identity_documents.png") },
        //                Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Identity", value:$"citizenship_dt")
        //             }
        //              }.ToAttachment(),
        //              new HeroCard
        //              {
        //                  Title = $"Migration",
        //            Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/migration.png") },
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Migration", value:$"migration")
        //            }
        //              }.ToAttachment(),
        //              new HeroCard
        //              {
        //                  Title = $"Find Laws",
        //            Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/find_law.png") },
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Laws", value:$"findlaws")
        //            }
        //              }.ToAttachment(),
        //              new HeroCard
        //              {
        //                  Title = $"Feedback",
        //                  Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/feedback.png") },
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.OpenUrl, "Feedback", value:$"http://lannpya.com/ForMessengerBot/SuggestionForm?MsgID={msg.From.Id}&UserName={msg.From.Name}")
        //            }
        //              }.ToAttachment()
        //          };
        //        await context.PostAsync(reply);
        //    }
        //    else if (msg.Text == "corruption" || msg.Text == "familyproblems")
        //    {
        //        await context.PostAsync("This feature is coming soon. We are working on it");
        //        var reply = context.MakeMessage();
        //        reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
        //        reply.Attachments = new List<Attachment> {
        //             new HeroCard
        //              {
        //                  Title = "How may I help you? :)",
        //                  //Text = Rabbit.Uni2Zg("How may I help you? :)"),
        //                  Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lanpya-girl_with_text.png") },
        //                  Buttons = new List<CardAction>
        //                  {
        //                          new CardAction(ActionTypes.ImBack,"🤔 Get started", value: $"🤔 Get started")
        //                  }
        //              }.ToAttachment()
        //          };
        //        await context.PostAsync(reply);
        //    }
        //    else if (msg.Text.Contains("rights"))
        //    {
        //        await LearnMyRight(msg.Text, msg, context);
        //    }
        //    else if (msg.Text.Contains("landmap"))
        //    {
        //        await new LandDialog().LandMap(context, result);
        //    }
        //    else if (msg.Text.Contains("justice"))
        //    {
        //        await new CriminalDialog().JusticeMap(context, result);
        //    }
        //    else if (msg.Text.Contains("migration"))
        //    {
        //        await new MigrationDialog().MigrationMap(context, result);
        //    }
        //    else if (msg.Text.Contains("worklawyers"))
        //    {
        //        await new LawyerDialog().LawyerMap(context, result);
        //    }
        //    else if (msg.Text == "findlaws")
        //    {
        //        var reply = msg.CreateReply($"If you are looking for a specific Myanmar law, the Union Attorney General’s office has an online searchable database including the constitution, laws, regulations, and rulings.");
        //        reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
        //        reply.Attachments = new List<Attachment> {
        //            new HeroCard
        //              {
        //                  Text = $"Great! I'd like to search for a law on the database.",
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Search database", value:$"searchdatabase")
        //            }
        //              }.ToAttachment(),
        //              new HeroCard
        //              {
        //                  Title = $"Back to main menu",
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Menu", value:$"🤔 Get started")
        //            }
        //              }.ToAttachment()
        //          };
        //        await context.PostAsync(reply);
        //    }
        //    else if (msg.Text == "searchdatabase")
        //    {
        //        var reply = msg.CreateReply($"You can access the online database on the Myanmar Law Information System (MLIS) website");
        //        reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
        //        reply.Attachments = new List<Attachment> {
        //            new HeroCard
        //              {
        //                  Title = $"Go to website",
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.OpenUrl, "Go", value:$"https://www.mlis.gov.mm/")
        //            }
        //              }.ToAttachment(),
        //              new HeroCard
        //              {
        //                  Title = $"Back to main menu",
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Menu", value:$"🤔 Get started")
        //            }
        //              }.ToAttachment()
        //          };
        //        await context.PostAsync(reply);
        //    }
        //    else if (msg.Text == "aboutlannpya")
        //    {
        //        var reply1 = msg.CreateReply($"Want to know more about the app? What would you like to know ? ");
        //        await context.PostAsync(reply1);
        //        var reply = msg.CreateReply("Please swipe or click on the arrow below to select from one of 5 options.");
        //        await context.PostAsync(msg.ResponseTyping());
        //        //var reply = msg.CreateReply($"Please select from the following 5 options");
        //        reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
        //        reply.Attachments = new List<Attachment> {
        //            new HeroCard
        //              {
        //                  Title = $"About Us",
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "About us", value:$"aboutus")
        //            }
        //              }.ToAttachment(),
        //               new HeroCard
        //              {
        //                  Title = $"How to download",
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Download help", value:$"howtodownload")
        //            }
        //              }.ToAttachment(),
        //              new HeroCard
        //              {
        //                  Title = $"I'm having problems with the app",
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Problems", value:$"havingproblem")
        //            }
        //              }.ToAttachment(),
        //              new HeroCard
        //              {
        //                  Title = $"Register on the app directory",
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Register", value:$"registerapp")
        //            }
        //              }.ToAttachment(),
        //              new HeroCard
        //              {
        //                  Title = $"Privacy policy",
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Privacy", value:$"privacypolicy")
        //            }
        //              }.ToAttachment(),
        //              new HeroCard
        //              {
        //                  Title = $"Back to Lann Pya",
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Start over", value:$"🤔 Get started")
        //            }
        //              }.ToAttachment()
        //          };
        //        await context.PostAsync(reply);
        //    }
        //    else if (msg.Text.Contains("citizenship"))
        //    {
        //        //await Citizenship_DT(msg.Text, msg, context);
        //        await new IdentityDialog().IdentityMap(context, result);
        //    }
        //    else if (msg.Text == "registerapp")
        //    {
        //        var reply = context.MakeMessage();
        //        reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
        //        reply.Attachments = new List<Attachment> {
        //               new HeroCard
        //              {
        //                  Title = $"I'm a lawyer.",
        //                  Text="Click the link below to register",
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.OpenUrl, "Register as Lawyer", value:$"https://docs.google.com/forms/d/e/1FAIpQLSdUAtT4885dPbgxMK4o4fOk2M6tvnJq1nQUZBEWiqElBGqFaA/viewform")
        //            }
        //              }.ToAttachment(),
        //              new HeroCard
        //              {
        //                  Title = $"I am a CSO representative",
        //                  Text="Click the link below to register",
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.OpenUrl, "Register as CSO", value:$"https://docs.google.com/forms/d/e/1FAIpQLSdSk6S4VuPu2Bu3GBXCeaTSP5hAGotnHD3b6mwCp5DZBz6lhg/viewform")
        //            }
        //              }.ToAttachment(),
        //              new HeroCard
        //              {
        //                  Title = $"Feedback",
        //                  Text="I'm experiencing difficulty completing the form online.",
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.OpenUrl, "Feedback", value:$"http://lannpya.com/ForMessengerBot/SuggestionForm?MsgID={msg.From.Id}&UserName={msg.From.Name}")
        //            }
        //              }.ToAttachment(),
        //              new HeroCard
        //              {
        //                  Title = $"Main menu",
        //                  Text="Go back to main menu",
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Menu", value:$"🤔 Get started")
        //            }
        //              }.ToAttachment()
        //          };
        //        await context.PostAsync(reply);
        //    }
        //    else if (msg.Text == "privacypolicy")
        //    {
        //        var reply1 = msg.CreateReply("Lann Pya is dedicated to protecting your privacy.");
        //        await context.PostAsync(reply1);
        //        await context.PostAsync(msg.ResponseTyping());
        //        var reply2 = msg.CreateReply("However, Facebook may collect your data so we cannot guarantee the security of any communication sent to us through Facebook messenger.");
        //        await context.PostAsync(reply2);
        //        await context.PostAsync(msg.ResponseTyping());
        //        var reply = msg.CreateReply($"Would you like to learn more about our privacy policies?");
        //        reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
        //        reply.Attachments = new List<Attachment> {
        //            new HeroCard
        //              {
        //                  Title = $"Yes, I'd like to learn more.",
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.OpenUrl, "Privacy", value:$"https://lannpya.com/TermsandConditions")
        //            }
        //              }.ToAttachment(),
        //               new HeroCard
        //              {
        //                  Title = $"No, thanks. Please take me back to the main menu.",
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Menu", value:$"🤔 Get started")
        //            }
        //              }.ToAttachment()
        //          };
        //        await context.PostAsync(reply);
        //    }
        //    else if (msg.Text == "howtodownload")
        //    {
        //        var reply = msg.CreateReply($"There are many ways to download Lann Pya on your phone. We suggest using Facebook or Google Play Store.");
        //        reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
        //        reply.Attachments = new List<Attachment> {
        //               new HeroCard
        //              {
        //                  Title = $"Download from Playstore",
        //            Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/googleplaystore.png") },
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "How to downlaod", value:$"downloadfromplaystore")
        //            }
        //              }.ToAttachment(),
        //              new HeroCard
        //              {
        //                  Title = $"Download from Facebook",
        //            Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/facebook.png") },
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "How to download", value:$"downloadfromfacebook")
        //            }
        //              }.ToAttachment()
        //            //   new HeroCard
        //            //  {
        //            //      Title = $"Download from Playstore",
        //            //Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/a4-how-to-download-on-play-store.jpg") },
        //            //Buttons = new List<CardAction>
        //            //      {
        //            //    new CardAction(ActionTypes.OpenUrl, "Detail", value:$"https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/a4-how-to-download-on-play-store.jpg"),
        //            //    new CardAction(ActionTypes.ImBack, "How to downlaod", value:$"downloadfromplaystore")
        //            //}
        //            //  }.ToAttachment(),
        //            //  new HeroCard
        //            //  {
        //            //      Title = $"Download from Facebook",
        //            //Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/a4-how-to-download.jpg") },
        //            //Buttons = new List<CardAction>
        //            //      {
        //            //    new CardAction(ActionTypes.OpenUrl, "Detail", value:$"https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/a4-how-to-download.jpg"),
        //            //    new CardAction(ActionTypes.ImBack, "How to download", value:$"downloadfromfacebook")
        //            //}
        //            //  }.ToAttachment()
        //        };
        //        await context.PostAsync(reply);
        //    }
        //    else if (msg.Text == "aboutus")
        //    {
        //        var reply = context.MakeMessage();
        //        reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
        //        reply.Attachments = new List<Attachment> {
        //               new HeroCard
        //              {
        //                  Title = $"LannPya",
        //            Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lannpya.png") },
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Lannpya", value:$"about_lannpya")
        //            }
        //              }.ToAttachment(),
        //              new HeroCard
        //              {
        //                  Title = $"MyJustice",
        //            Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/mj_logo.png") },
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "MyJustice", value:$"about_myjusstice")
        //            }
        //              }.ToAttachment(),
        //              new HeroCard
        //              {
        //                  Title = $"Koe Koe Tech",
        //            Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/kkt_logo.png") },
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Koe Koe Tech", value:$"about_koekoetech")
        //            }
        //              }.ToAttachment(),
        //              new HeroCard
        //              {
        //                  Title = $"Acknowledgements",
        //            Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/acknowledgement.png") },
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Acknowledgements", value:$"about_acknowledgements")
        //            }
        //              }.ToAttachment()
        //          };
        //        await context.PostAsync(reply);
        //    }
        //    else if (msg.Text == "about_lannpya")
        //    {
        //        var reply1 = msg.CreateReply("LannPya is the result of an innovative collaboration between MyJustice and Koe Koe Tech that we hope will help to inform, connect, and empower the people of Myanmar, and build trust between communities, governmental and non - governmental legal service providers.");
        //        await context.PostAsync(reply1);
        //        await context.PostAsync(msg.ResponseTyping());
        //        var reply2 = msg.CreateReply("LannPya can help democratize access to essential information about law and rights and connect justice seekers to lawyers and governmental and non - governmental institutions that can help them.");
        //        await context.PostAsync(reply2);
        //        await context.PostAsync(msg.ResponseTyping());
        //        var reply3 = msg.CreateReply("LannPya means Show the Way in Myanmar language, and that’s what we hope this app can do: help to show the way to a more just, equitable, and democratic Myanmar.");
        //        await context.PostAsync(reply3);
        //        await context.PostAsync(msg.ResponseTyping());
        //        var reply = msg.CreateReply("Download LannPya today!");
        //        reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
        //        reply.Attachments = new List<Attachment> {
        //              new HeroCard
        //              {
        //                  Title = $"Download Lannpya",
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Download", value:$"downloadlp_justice")
        //            }
        //              }.ToAttachment(),
        //              new HeroCard
        //              {
        //                  Title = $"Back to Lann Pya",
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Menu", value:$"🤔 Get started")
        //            }
        //              }.ToAttachment()
        //          };
        //        await context.PostAsync(reply);
        //    }
        //    else if (msg.Text == "about_myjusstice")
        //    {
        //        var reply1 = msg.CreateReply("MyJustice aims to equip the people of Myanmar with knowledge, confidence and opportunities so that they can resolve conflicts fairly, equitably and justly.");
        //        await context.PostAsync(reply1);
        //        await context.PostAsync(msg.ResponseTyping());
        //        var reply2 = msg.CreateReply("We work closely with local communities to achieve a lasting impact on the way disputes are resolved and justice is delivered, especially for poor, vulnerable and marginalised people.");
        //        await context.PostAsync(reply2);
        //        await context.PostAsync(msg.ResponseTyping());
        //        var reply3 = msg.CreateReply("MyJustice is funded by the European Union and implemented by the British Council.");
        //        await context.PostAsync(reply3);
        //        await context.PostAsync(msg.ResponseTyping());
        //        var reply4 = msg.CreateReply("The Lann Pya App is an innovative collaboration between MyJustice and Koe Koe Tech.");
        //        await context.PostAsync(reply4);
        //        await context.PostAsync(msg.ResponseTyping());
        //        var reply = msg.CreateReply("Download LannPya today!");
        //        reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
        //        reply.Attachments = new List<Attachment> {
        //              new HeroCard
        //              {
        //                  Title = $"Download Lannpya",
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Download", value:$"downloadlp_justice")
        //            }
        //              }.ToAttachment(),
        //              new HeroCard
        //              {
        //                  Title = $"Go to MyJustice",
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.OpenUrl, "MyJustice", value:$"https://www.myjusticemyanmar.org/")
        //            }
        //              }.ToAttachment(),
        //              new HeroCard
        //              {
        //                  Title = $"Back to Lann Pya",
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Menu", value:$"🤔 Get started")
        //            }
        //              }.ToAttachment()
        //          };
        //        await context.PostAsync(reply);
        //    }
        //    else if (msg.Text == "about_koekoetech")
        //    {
        //        var reply1 = msg.CreateReply("Koe Koe Tech is a Myanmar IT Social Enterprise which focuses on developing software to address key social issues in the fields of Health, Law and Governance, with considerable experience and an intimate knowledge of the local cultural context. Founded in 2013 by Dr. Yar Zar Minn Htoo and Michael Lwin Esq., KKT employs 145 staff, 92.4% of whom are Myanmar nationals, with more than two-thirds female. We are intentionally Myanmar-centric and diverse by design: we believe that the best solutions are locally made and driven, and that diversity is a strength for Myanmar, not a weakness.");
        //        await context.PostAsync(reply1);
        //        await context.PostAsync(msg.ResponseTyping());
        //        var reply2 = msg.CreateReply("TheOur mission is to develop Myanmar’s vital IT infrastructure, taking advantage of the latest technology to enable Myanmar to leap forward in its development. Our vision is a Myanmar in which citizens, government, business and NGOs are all digitally connected and can benefit from the latest technology to enhance their work and further connect up a rapidly developing country.");
        //        await context.PostAsync(reply2);
        //        await context.PostAsync(msg.ResponseTyping());
        //        var reply = msg.CreateReply("Download LannPya today!");
        //        reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
        //        reply.Attachments = new List<Attachment> {
        //              new HeroCard
        //              {
        //                  Title = $"Download Lannpya",
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Download", value:$"downloadlp_justice")
        //            }
        //              }.ToAttachment(),
        //              new HeroCard
        //              {
        //                  Title = $"Go to MyJustice",
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.OpenUrl, "MyJustice", value:$"https://www.koekoetech.com/")
        //            }
        //              }.ToAttachment(),
        //              new HeroCard
        //              {
        //                  Title = $"Back to Lann Pya",
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Menu", value:$"🤔 Get started")
        //            }
        //              }.ToAttachment()
        //          };
        //        await context.PostAsync(reply);
        //    }
        //    else if (msg.Text == "about_acknowledgements")
        //    {
        //        var reply1 = msg.CreateReply("The Lann Pya Access to Justice Mobile Application was developed with the generous support of the European Union through the MyJustice Programme implemented by the British Council in partnership with social enterprise Koe Koe Tech Co.Ltd.");
        //        await context.PostAsync(reply1);
        //        await context.PostAsync(msg.ResponseTyping());
        //        var reply2 = msg.CreateReply("MyJustice and Koe Koe Tech were supported by numerous governmental and non - governmental organisations and individuals in the development of the LannPya application");
        //        await context.PostAsync(reply2);
        //        await context.PostAsync(msg.ResponseTyping());
        //        var reply3 = msg.CreateReply("We also greatly appreciate the many individuals who provided user testing and feedback on content to strengthen the usefulness of the app for ordinary people.");
        //        await context.PostAsync(reply3);
        //        await context.PostAsync(msg.ResponseTyping());
        //        var reply4 = msg.CreateReply("In particular, we wish to acknowledge the contributions of: Union Attorney General’s Office, Office of the Supreme Court, Myanmar Police Force, IDLO, Independent Lawyers’ Association of Myanmar, Mandalay Bar Association, Mawlamyaing Bar Association, Shan State Bar Association, Yangon Bar Association, Yangon Justice Affairs and Rule of Law Centres Coordination Body, International Bridges to Justice, Mawlamyaing Justice Centre, Norwegian Refugee Council, Yangon Justice Centre");
        //        await context.PostAsync(reply4);
        //        await context.PostAsync(msg.ResponseTyping());
        //        var reply = msg.CreateReply("Download LannPya today!");
        //        reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
        //        reply.Attachments = new List<Attachment> {
        //              new HeroCard
        //              {
        //                  Title = $"Download Lannpya",
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Download", value:$"downloadlp_justice")
        //            }
        //              }.ToAttachment(),
        //              new HeroCard
        //              {
        //                  Title = $"Back to Lann Pya",
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Menu", value:$"🤔 Get started")
        //            }
        //              }.ToAttachment()
        //          };
        //        await context.PostAsync(reply);
        //    }
        //    else if (msg.Text.Contains("problem"))
        //    {
        //        await HavingProblem(msg.Text, msg, context);
        //    }
        //    else if (msg.Text == "downloadfromplaystore")
        //    {
        //        var reply1 = msg.CreateReply("Step One: Look for the Google Play Store on your phone.");
        //        await context.PostAsync(reply1);
        //        await context.PostAsync(msg.ResponseTyping());
        //        var reply = msg.CreateReply("Remember, to use Google Play Store, you need to be connected to the internet. You also must be signed into your Gmail account on the Play Store app.");
        //        reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
        //        reply.Attachments = new List<Attachment> {
        //              new HeroCard
        //              {
        //                  Title = $"Playstore",
        //                  Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/googleplaystore.png") },
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Next", value:$"stepone_dl_ps")
        //            }
        //              }.ToAttachment()
        //          };
        //        await context.PostAsync(reply);
        //    }
        //    else if (msg.Text == "stepone_dl_ps")
        //    {
        //        var reply = msg.CreateReply("Step Two: Type “Lann Pya” into the search box at the top of the app screen.");
        //        reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
        //        reply.Attachments = new List<Attachment> {
        //              new HeroCard
        //              {
        //                  Title = $"Search in Playstore",
        //                  Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/g1.PNG") },
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Next", value:$"steptwo_dl_ps")
        //            }
        //              }.ToAttachment()
        //          };
        //        await context.PostAsync(reply);
        //    }
        //    else if (msg.Text == "steptwo_dl_ps")
        //    {
        //        var reply = msg.CreateReply("Step Three: Click on Lann Pya in the search results, then click “Install” to begin downloading the app on your phone.");
        //        reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
        //        reply.Attachments = new List<Attachment> {
        //              new HeroCard
        //              {
        //                  Title = $"Found in Playstore",
        //                  Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/g2.PNG") },
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Next", value:$"stepthree_dl_ps")
        //            }
        //              }.ToAttachment()
        //          };
        //        await context.PostAsync(reply);
        //    }
        //    else if (msg.Text == "stepthree_dl_ps")
        //    {
        //        var reply = msg.CreateReply("Step Four: After it has finished downloading, you will now have Lann Pya on your phone! Click the Lann Pya icon to use the app.");
        //        reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
        //        reply.Attachments = new List<Attachment> {
        //              new HeroCard
        //              {
        //                  Title = $"Install Lannpya",
        //                  Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lannpya_logo1.png") },
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Done", value:$"fisnished_download")
        //            }
        //              }.ToAttachment()
        //          };
        //        await context.PostAsync(reply);
        //    }
        //    else if (msg.Text == "downloadfromfacebook")
        //    {
        //        var reply = context.MakeMessage();
        //        reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
        //        reply.Attachments = new List<Attachment> {
        //              new HeroCard
        //              {
        //                  Title = $"Facebook",
        //                  Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/facebook.png") },
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Next", value:$"stepone_dl_fb")
        //            }
        //              }.ToAttachment()
        //          };
        //        await context.PostAsync(reply);
        //    }
        //    else if (msg.Text == "stepone_dl_fb")
        //    {
        //        var reply = msg.CreateReply("Step One: Visit our Facebook page by typing “Lann Pya” into the Facebook search box.");
        //        reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
        //        reply.Attachments = new List<Attachment> {
        //              new HeroCard
        //              {
        //                  Title = $"Search in Facebook",
        //                  Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/f1.PNG") },
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Next", value:$"steptwo_dl_fb")
        //            }
        //              }.ToAttachment()
        //          };
        //        await context.PostAsync(reply);
        //    }
        //    else if(msg.Text== "steptwo_dl_fb")
        //    {
        //        var reply = msg.CreateReply("Step Two: Click the “Use app” button located near the top on our Lann Pya Facebook page to begin downloading the app onto your phone.");
        //        reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
        //        reply.Attachments = new List<Attachment> {
        //              new HeroCard
        //              {
        //                  Title = $"Found in Facebook",
        //                  Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/f2.PNG") },
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Next", value:$"stepthree_dl_fb")
        //            }
        //              }.ToAttachment()
        //          };
        //        await context.PostAsync(reply);
        //    }
        //    else if(msg.Text== "stepthree_dl_fb")
        //    {
        //        var reply = msg.CreateReply("Step Three: After it has finished downloading, click “Install.” You can now use Lann Pya on your phone!");
        //        reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
        //        reply.Attachments = new List<Attachment> {
        //              new HeroCard
        //              {
        //                  Title = $"Install Lannpya",
        //                  Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lannpya_logo1.png") },
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Done", value:$"fisnished_download")
        //            }
        //              }.ToAttachment()
        //          };
        //        await context.PostAsync(reply);
        //    }
        //    else if(msg.Text== "fisnished_download")
        //    {
        //        var reply = msg.CreateReply("Thank you for learning how to download :)");
        //        reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
        //        reply.Attachments = new List<Attachment> {
        //              new HeroCard
        //              {
        //                  Title = $"Learn how to download again",
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Go", value:$"howtodownload")
        //            }
        //              }.ToAttachment(),
        //              new HeroCard
        //              {
        //                  Title = $"Back to main menu",
        //            Buttons = new List<CardAction>
        //                  {
        //                new CardAction(ActionTypes.ImBack, "Menu", value:$"🤔 Get started")
        //            }
        //              }.ToAttachment()
        //          };
        //        await context.PostAsync(reply);
        //    }
        //    context.Wait(MessageReceivedAsync);
        //}

        public async Task Citizenship_DT(string text = null, Activity msg = null, IDialogContext context = null)
        {
            if (text == "citizenship_dt")
            {
                var reply = msg.CreateReply("What would you like to know?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"Citizenship and residency cards",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/idcard.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "View Detail", value:$"residency_citizenship")
                    }
                      }.ToAttachment()
                    //  new HeroCard
                    //  {
                    //      Title = $"Privacy policy",
                    //Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lannpya_privacy.png") },
                    //Buttons = new List<CardAction>
                    //      {
                    //    new CardAction(ActionTypes.ImBack, "Privacy", value:$"privacypolicy_citizenship")
                    //}
                    //  }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "privacypolicy_citizenship")
            {
                var reply1 = msg.CreateReply("There's just one thing you should know: Although Lann Pya is dedicated to protecting your privacy, Facebook may collect your information.");
                await context.PostAsync(reply1);
                await context.PostAsync(msg.ResponseTyping());
                var reply2 = msg.CreateReply("We cannot guarantee the security of any communications sent to us through Facebook Messenger.");
                await context.PostAsync(reply2);
                await context.PostAsync(msg.ResponseTyping());
                var reply = msg.CreateReply("Do you want to continue?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text = $"No but I still want to know my citizenship status.",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Citizenship", value:$"identityinformation_citizenship")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Text = $"If you have concerns about your privacy, you can download the whole questionnaire below.",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "Privacy", value:$"https://lannpya.com/TermsandConditions")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "residency_citizenship")
            {
                var reply = msg.CreateReply("We have lots of information about identity papers, such as citizenship and residency documents, thanks to Justice Base");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"Identity information",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/JusticeBase_logo.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "View Detail", value:$"identityinformation_citizenship")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "identityinformation_citizenship")
            {
                var reply = msg.CreateReply("What would you like to know?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          //Title = $"Citizenship and residency cards",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/identity_documents.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "View Detail", value:$"liketoknow_citizenship")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "liketoknow_citizenship")
            {
                var reply = msg.CreateReply("Are you Taingyinthar?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="citizenship_tyt_Yes" },
                        new CardAction(){ Title = "No", Type=ActionTypes.ImBack, Value="citizenship_tyt_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "citizenship_tyt_Yes" || text == "citizenship_law_Yes" || text == "citizenship_second_Yes" || text == "citizenship_associate_Yes" || text == "citizenship_grandparents_Yes")
            {
                var reply = msg.CreateReply("You are a citizen");
                await context.PostAsync(reply);
                var reply1 = msg.CreateReply("Great. What do I do now?");
                await context.PostAsync(reply1);
                var reply3 = msg.CreateReply("If you would like to learn more about your rights or connect with lawyers and other service providers who can help you with your identity papers, download Lann Pya now.");
                reply3.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply3.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"Install Lannpya now",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/download_lannpya.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "View Detail", value:$"downloadlp_justice")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = "Back to main menu",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lannpya_logo1.png") },
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack,"Menu", value: $"🤔 Get started")
                          }
                      }.ToAttachment(),
                    new HeroCard
                      {
                          Title = $"Start Identity",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/identity_documents.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "View Detail", value:$"liketoknow_citizenship")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply3);
            }
            else if (text == "citizenship_tyt_No")
            {
                var reply = context.MakeMessage();
                reply.Text = "Where you already a citizen on 15 October 1982 or under the 1948 Citizenship Law?";
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="citizenship_law_Yes" },
                        new CardAction(){ Title = "No", Type=ActionTypes.ImBack, Value="citizenship_law_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
                //var reply = msg.CreateReply($"Where you already a citizen on 15 October 1982 or under the 1948 Citizenship Law?");
                //reply.Type = ActivityTypes.Message;
                //reply.TextFormat = TextFormatTypes.Plain;
                //reply.SuggestedActions = new SuggestedActions()
                //{
                //    Actions = (new List<string> { "🙂 Yes", "😦 No" }).Select(a => new CardAction
                //    {
                //        Title = a,
                //        Type = ActionTypes.ImBack,
                //        Value = "citizenship_law_" + a
                //    }).ToList()
                //};
                //await context.PostAsync(reply);
            }
            else if (text == "citizenship_law_No")
            {
                var reply = context.MakeMessage();
                reply.Text = "Is one of your parents a citizen?";
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="citizenship_parents_Yes" },
                        new CardAction(){ Title = "No", Type=ActionTypes.ImBack, Value="citizenship_parents_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "citizenship_parents_Yes")
            {
                var reply = context.MakeMessage();
                reply.Text = $"Is your SECOND parent a citizen, a naturalised citizen, or an associate citizen?";
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="citizenship_second_Yes" },
                        new CardAction(){ Title = "No", Type=ActionTypes.ImBack, Value="citizenship_second_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "citizenship_second_No")
            {
                var reply = context.MakeMessage();
                reply.Text = $"Is your SECOND parent a foreigner?";
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="citizenship_foreigner_Yes" },
                        new CardAction(){ Title = "No", Type=ActionTypes.ImBack, Value="citizenship_foreigner_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "citizenship_foreigner_Yes" || text == "citizenship_bothparent_No" || text == "citizenship_seondforengner_Yes")
            {
                var reply = msg.CreateReply("You can apply for naturalised citizenship");
                await context.PostAsync(reply);
                var reply4 = msg.CreateReply("You may be eligible for Naturalised Citizenship by Marriage if:");
                await context.PostAsync(reply4);
                var reply5 = msg.CreateReply("You got a Foreign Registration Card before 15 October 1982, and");
                await context.PostAsync(reply5);
                var reply6 = msg.CreateReply("You were married before October 1982, and");
                await context.PostAsync(reply6);
                var reply7 = msg.CreateReply("Your husband or wife is a citizen, naturalised citizen or an associate citizen, and");
                await context.PostAsync(reply7);
                var reply8 = msg.CreateReply("You are their only husband or wife, and");
                await context.PostAsync(reply8);
                var reply9 = msg.CreateReply("You have been married for three years, and");
                await context.PostAsync(reply9);
                var reply10 = msg.CreateReply("You have lived in Myanmar for at least three years.");
                await context.PostAsync(reply10);
                var reply1 = msg.CreateReply("Great. What do I do now?");
                await context.PostAsync(reply1);
                var reply3 = msg.CreateReply("If you would like to learn more about your rights or connect with lawyers and other service providers who can help you with your identity papers, download Lann Pya now.");
                reply3.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply3.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"Install Lannpya now",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/download_lannpya.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "View Detail", value:$"downloadlp_justice")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = "Back to main menu",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lannpya_logo1.png") },
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack,"Menu", value: $"🤔 Get started")
                          }
                      }.ToAttachment(),
                    new HeroCard
                      {
                          Title = $"Start Identity",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/identity_documents.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "View Detail", value:$"liketoknow_citizenship")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply3);
            }
            else if (text == "citizenship_foreigner_No")
            {
                var reply = context.MakeMessage();
                reply.Text = $"Does your SECOND parent, have two parents, who are either a naturalised citizen or an associate citizen?";
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="citizenship_associate_Yes" },
                        new CardAction(){ Title = "No", Type=ActionTypes.ImBack, Value="citizenship_associate_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "citizenship_associate_No" || text == "citizenship_oneparent_No" || text == "citizenship_seondforengner_No")
            {
                var reply = msg.CreateReply("You should seek advice about your legal status");
                await context.PostAsync(reply);
                var reply1 = msg.CreateReply("Great. What do I do now?");
                await context.PostAsync(reply1);
                var reply3 = msg.CreateReply("If you would like to learn more about your rights or connect with lawyers and other service providers who can help you with your identity papers, download Lann Pya now.");
                reply3.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply3.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"Install Lannpya now",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/download_lannpya.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "View Detail", value:$"downloadlp_justice")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = "Back to main menu",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lannpya_logo1.png") },
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack,"Menu", value: $"🤔 Get started")
                          }
                      }.ToAttachment(),
                    new HeroCard
                      {
                          Title = $"Start Identity",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/identity_documents.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "View Detail", value:$"liketoknow_citizenship")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply3);
            }
            else if (text == "citizenship_parents_No")
            {
                var reply = context.MakeMessage();
                reply.Text = $"Is one of your parents a naturalised citizen or an associate citizen?";
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="citizenship_oneparent_Yes" },
                        new CardAction(){ Title = "No", Type=ActionTypes.ImBack, Value="citizenship_oneparent_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "citizenship_oneparent_Yes")
            {
                var reply = context.MakeMessage();
                reply.Text = $"Is your SECOND parent a naturalised citizen or an associate citizen?";
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="citizenship_secondparent_Yes" },
                        new CardAction(){ Title = "No", Type=ActionTypes.ImBack, Value="citizenship_secondparent_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "citizenship_secondparent_Yes")
            {
                var reply = context.MakeMessage();
                reply.Text = $"Are your grandparents on your SECOND parent's side both naturalised citizens or associate citizens?";
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="citizenship_grandparents_Yes" },
                        new CardAction(){ Title = "No", Type=ActionTypes.ImBack, Value="citizenship_grandparents_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "citizenship_grandparents_No")
            {
                var reply = context.MakeMessage();
                reply.Text = $"Are both your parents associate citizens?";
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="citizenship_bothparent_Yes" },
                        new CardAction(){ Title = "No", Type=ActionTypes.ImBack, Value="citizenship_bothparent_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "citizenship_bothparent_Yes")
            {
                var reply = msg.CreateReply("You may be an associate citizen");
                await context.PostAsync(reply);
                var reply4 = msg.CreateReply("You may be eligible for Associate Citizenship if:");
                await context.PostAsync(reply4);
                var reply5 = msg.CreateReply("You applied for citizenship before 1982, or");
                await context.PostAsync(reply5);
                var reply6 = msg.CreateReply("Your parents are both Associate Citizens and when you were born your name and details were on your parents' documents.");
                await context.PostAsync(reply6);
                var reply7 = msg.CreateReply("If you already have Associate Citizenship because of your parents, when you turn 18 years old you have to complete some more requirements to continue to be an Associate Citizen.");
                await context.PostAsync(reply7);
                var reply1 = msg.CreateReply("Great. What do I do now?");
                await context.PostAsync(reply1);
                var reply3 = msg.CreateReply("If you would like to learn more about your rights or connect with lawyers and other service providers who can help you with your identity papers, download Lann Pya now.");
                reply3.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply3.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"Install Lannpya now",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/download_lannpya.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "View Detail", value:$"downloadlp_justice")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = "Back to main menu",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lannpya_logo1.png") },
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack,"Menu", value: $"🤔 Get started")
                          }
                      }.ToAttachment(),
                    new HeroCard
                      {
                          Title = $"Start Identity",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/identity_documents.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "View Detail", value:$"liketoknow_citizenship")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply3);
            }
            else if (text == "citizenship_secondparent_No")
            {
                var reply = context.MakeMessage();
                reply.Text = $"Is your second parent a foreigner?";
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="citizenship_seondforengner_Yes" },
                        new CardAction(){ Title = "No", Type=ActionTypes.ImBack, Value="citizenship_seondforengner_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
        }

        public async Task LearnMyRight(string text = null, Activity msg = null, IDialogContext context = null)
        {
            if (msg.Text == "learnmyrights")
            {
                var reply = context.MakeMessage();
                reply.Text = "Please swipe or click on the arrow below to select from one of 8 options.";
                //reply.Text = "Please select from the following 8 options";
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = $"Criminally Accused",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/criminal_accused.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Accused", value:$"criminallyaccused_rights")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Complainant & Witnesses",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/complainant_and_witnesses.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "C & W", value:$"complainantwitnesses_rights")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Women's Rights",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/woman_rights.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Women's Rights", value:$"womenrights")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Child Rights",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/child_right.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Child Rights", value:$"childrights")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"LGBTQI Rights",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lgbtq_rights.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "LGBTQI Rights", value:$"lgbtqirights")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Land Rights",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/land_rights.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Land Rights", value:$"landrights")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Labour Rights",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/labour_rights.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Labour Rights", value:$"labourrights")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = "Back to main menu",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lannpya_logo1.png") },
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack,"Menu", value: $"🤔 Get started")
                          }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "criminallyaccused_rights")
            {
                var reply = msg.CreateReply("Did you know that any person accused of a crime has the right to a lawyer?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Yes" },
                        new CardAction(){ Title = "Really?", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Really" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "criminallyaccused_rights_Yes" || msg.Text == "criminallyaccused_rights_Really")
            {
                if (msg.Text == "criminallyaccused_rights_Really")
                {
                    await context.PostAsync("That's right!");
                }
                await context.PostAsync("Myanmar's Constitution 375 guarantees the right to defense for all people accused of crime");
                await context.PostAsync(msg.ResponseTyping());
                var reply = msg.CreateReply("Want to learn more?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Learnmore_Yes" },
                        new CardAction(){ Title = "No", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Learnmore_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "criminallyaccused_rights_Learnmore_Yes")
            {
                await context.PostAsync("Great! the LannPya App contains lots of information about your rights.");
                var reply = context.MakeMessage();
                await context.PostAsync(msg.ResponseTyping());
                reply.Text = "To learn more, download the app";
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                    new HeroCard
                      {
                          Title = $"Install Lannpya now",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/download_lannpya.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Install", value:$"downloadlp_justice")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = "Back to main menu",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lannpya_logo1.png") },
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack,"Menu", value: $"🤔 Get started")
                          }
                      }.ToAttachment(),
                    new HeroCard
                      {
                          Title = $"Back to learn my rights",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/know_your_rights1.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Right", value:$"learnmyrights")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "criminallyaccused_rights_Learnmore_No")
            {
                await context.PostAsync("Ok see you again soon!");
                var reply = context.MakeMessage();
                reply.Text = "Bye !";
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Title = "Back to main menu",
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack,"Menu", value: $"🤔 Get started")
                          }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "complainantwitnesses_rights")
            {
                var reply = msg.CreateReply("Did you know that if you are a victim of a crime, you may be able to receive financial compensation for your loss?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="complainantwitnesses_rights_Yes" },
                        new CardAction(){ Title = "Really?", Type=ActionTypes.ImBack, Value="complainantwitnesses_rights_Really" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "complainantwitnesses_rights_Yes" || msg.Text == "complainantwitnesses_rights_Really")
            {
                if (msg.Text == "complainantwitnesses_rights_Really")
                {
                    await context.PostAsync("That's right!");
                }
                await context.PostAsync("Under the Myanmar Criminal Procedure Code Section 545(a) - (b), if the accused is convicted in court, the person who made the complaint may be able to recover legal costs as well as compensation for loss.");
                await context.PostAsync(msg.ResponseTyping());
                var reply = msg.CreateReply("Want to learn more?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Learnmore_Yes" },
                        new CardAction(){ Title = "No", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Learnmore_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "womenrights")
            {
                var reply = msg.CreateReply("Did you know that there are laws that can protect women against acts of gender - based violence, including harm caused by people considered a family member or intimate partner ? ");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="womenrights_Yes" },
                        new CardAction(){ Title = "Really?", Type=ActionTypes.ImBack, Value="womenrights_Really" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "womenrights_Yes" || msg.Text == "womenrights_Really")
            {
                if (msg.Text == "womenrights_Really")
                {
                    await context.PostAsync("That's right!");
                }
                await context.PostAsync("For example, the Amending Law to the Penal Code (2019) Section 376(2) has expanded the definition of rape to include when a man has non - consensual sexual intercourse with his wife.");
                var reply = msg.CreateReply("Want to learn more?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Learnmore_Yes" },
                        new CardAction(){ Title = "No", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Learnmore_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "childrights")
            {
                var reply = msg.CreateReply("Did you know that every child in Myanmar has the right to have their birth officially registered for free and without discrimination ? ");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="childrights_Yes" },
                        new CardAction(){ Title = "Really?", Type=ActionTypes.ImBack, Value="childrights_Really" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "childrights_Yes" || msg.Text == "childrights_Really")
            {
                if (msg.Text == "childrights_Really")
                {
                    await context.PostAsync("That's right!");
                }
                await context.PostAsync("Under the new Child Rights Law (2019) Section 21, every child has the right to have his or her birth entered into an official birth registration record. After registration, the parent or guardian shall receive a birth certificate.");
                await context.PostAsync(msg.ResponseTyping());
                await context.PostAsync("This certificate will help the child access health care, enroll in school, secure property and land rights, as well as receive other protections granted to persons according to their age.");
                var reply = msg.CreateReply("Want to learn more?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Learnmore_Yes" },
                        new CardAction(){ Title = "No", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Learnmore_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "lgbtqirights")
            {
                var reply = msg.CreateReply("Did you know that there are many laws that are used to discriminate against persons who identify as lesbian, gay, bisexual, transgender, queer or intersex(LBGTQI) in Myanmar?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="lgbtqirights_Yes" },
                        new CardAction(){ Title = "Really?", Type=ActionTypes.ImBack, Value="lgbtqirights_Really" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "lgbtqirights_Yes" || msg.Text == "lgbtqirights_Really")
            {
                if (msg.Text == "lgbtqirights_Really")
                {
                    await context.PostAsync("Unfortunately, yes.");
                }
                await context.PostAsync("For example, provisions under the Police Act (1945) have been used in cases of police misconduct as a means in which to target members of the LGBTQI community.");
                await context.PostAsync(msg.ResponseTyping());
                var reply = msg.CreateReply("Want to learn more?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Learnmore_Yes" },
                        new CardAction(){ Title = "No", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Learnmore_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "landrights")
            {
                var reply = msg.CreateReply("Did you know that the government may legally take your land for a public purpose?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="landrights_Yes" },
                        new CardAction(){ Title = "Really?", Type=ActionTypes.ImBack, Value="landrights_Really" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "landrights_Yes" || msg.Text == "landrights_Really")
            {
                if (msg.Text == "landrights_Really")
                {
                    await context.PostAsync("That's right.");
                }
                await context.PostAsync("The government may take your land and use it for purposes in the public interest.However, you may also be entitled to receive compensation from the government for any loss due to the confiscation of your land.");
                await context.PostAsync(msg.ResponseTyping());
                var reply = msg.CreateReply("Want to learn more?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Learnmore_Yes" },
                        new CardAction(){ Title = "No", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Learnmore_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "labourrights")
            {
                var reply = msg.CreateReply("Did you know that employers are responsible for protecting workers from occupational hazards?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="labourrights_Yes" },
                        new CardAction(){ Title = "Really?", Type=ActionTypes.ImBack, Value="labourrights_Really" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (msg.Text == "labourrights_Yes" || msg.Text == "labourrights_Really")
            {
                if (msg.Text == "labourrights_Really")
                {
                    await context.PostAsync("That's right.");
                }
                await context.PostAsync("According to the Occupational Safety and Health Law(2019) Section 26(d) - (e), employers must protect their employees from occupational hazards including by providing protective equipment and facilities.");
                await context.PostAsync(msg.ResponseTyping());
                var reply = msg.CreateReply("Want to learn more?");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Choose :",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Learnmore_Yes" },
                        new CardAction(){ Title = "No", Type=ActionTypes.ImBack, Value="criminallyaccused_rights_Learnmore_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }

        }

        public async Task HavingProblem(string text = null, Activity msg = null, IDialogContext context = null)
        {
            if (text == "havingproblem")
            {
                var reply = msg.CreateReply("Please swipe or click the arrow to select from one of 3 options");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                       new HeroCard
                      {
                          Title = $"I cannot install or update Lann Pya on my device.",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Install/update problems", value:$"device_problem")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"I'm having other technical issues",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Technical", value:$"technical_problem")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = "Back to main menu",
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack,"Menu", value: $"🤔 Get started")
                          }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "device_problem")
            {
                var reply = context.MakeMessage();
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                      new HeroCard
                      {
                          Text="Are you using an Android device?",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(){ Title = "Yes", Type=ActionTypes.ImBack, Value="android_problem_Yes" },
                        new CardAction(){ Title = "No", Type=ActionTypes.ImBack, Value="android_problem_No" },
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "android_problem_Yes")
            {
                var reply = context.MakeMessage();
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                       new HeroCard
                      {
                          Title = $"My device does not have enough storage space.",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Storage", value:$"space_problem")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"I cannot update Lann Pya",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Lannpya update", value:$"updating_problem")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "space_problem")
            {
                var reply1 = msg.CreateReply("1. Go to settings");
                await context.PostAsync(reply1);
                await context.PostAsync(msg.ResponseTyping());
                var reply2 = msg.CreateReply("2. Select 'Apps'");
                await context.PostAsync(reply2);
                await context.PostAsync(msg.ResponseTyping());
                var reply = msg.CreateReply("3. Free up space by uninstalling unused apps. ");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                       new HeroCard
                      {
                          Title = $"Still not working? Send feedback!",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/feedback.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "Feedback", value:$"http://lannpya.com/ForMessengerBot/SuggestionForm?MsgID={msg.From.Id}&UserName={msg.From.Name}")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = "Back to main menu",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lannpya_logo1.png") },
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack,"Menu", value: $"🤔 Get started")
                          }
                      }.ToAttachment(),
                    //  new HeroCard
                    //  {
                    //      Title = $"Go to About Lannpya",
                    //Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/about_lannpya_chatbot.png") },
                    //Buttons = new List<CardAction>
                    //      {
                    //    new CardAction(ActionTypes.ImBack, "About Lannpya", value:$"aboutlannpya")
                    //}
                    //  }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "updating_problem" || text == "appversion_problem")
            {
                var reply = msg.CreateReply("Go to the Google Playstore to download the most recent version of Lann Pya.");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                       new HeroCard
                      {
                          Title = $"Go to google playstore",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/go_to_playstore.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "Playstore", value:$"https://play.google.com/store/apps/details?id=com.koekoetech.myjustice")
                    }
                      }.ToAttachment(),
                       new HeroCard
                      {
                          Title = $"Still not working? Send feedback!",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/feedback.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "Feedback", value:$"http://lannpya.com/ForMessengerBot/SuggestionForm?MsgID={msg.From.Id}&UserName={msg.From.Name}")
                    }
                      }.ToAttachment(),
                    //  new HeroCard
                    //  {
                    //      Title = "Back to main menu",
                    //      Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lannpya_logo1.png") },
                    //      Buttons = new List<CardAction>
                    //      {
                    //              new CardAction(ActionTypes.ImBack,"Menu", value: $"🤔 Get started")
                    //      }
                    //  }.ToAttachment(),
                    //  new HeroCard
                    //  {
                    //      Title = $"Go to About Lannpya",
                    //Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/about_lannpya_chatbot.png") },
                    //Buttons = new List<CardAction>
                    //      {
                    //    new CardAction(ActionTypes.ImBack, "About Lannpya", value:$"aboutlannpya")
                    //}
                    //  }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "android_problem_No")
            {
                var reply = msg.CreateReply("Lann Pya is currently only available on Android. However, the Lann Pya webpage has been optimized for viewing in all available operating systems.");
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                    new HeroCard
                      {
                          Title = $"Go to our Lannpya website",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/go_to_lannpya_website.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "Website", value:$"https://www.lannpya.com/")
                    }
                      }.ToAttachment(),
                    //   new HeroCard
                    //  {
                    //      Title = $"Still not working? Send feedback!",
                    //Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/feedback.png") },
                    //Buttons = new List<CardAction>
                    //      {
                    //    new CardAction(ActionTypes.OpenUrl, "Feedback", value:$"http://lannpya.com/ForMessengerBot/SuggestionForm?MsgID={msg.From.Id}&UserName={msg.From.Name}")
                    //}
                    //  }.ToAttachment(),
                      new HeroCard
                      {
                          Title = "Back to main menu",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lannpya_logo1.png") },
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack,"Menu", value: $"🤔 Get started")
                          }
                      }.ToAttachment(),
                    //  new HeroCard
                    //  {
                    //      Title = $"Go to About Lannpya",
                    //Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/about_lannpya_chatbot.png") },
                    //Buttons = new List<CardAction>
                    //      {
                    //    new CardAction(ActionTypes.ImBack, "About Lannpya", value:$"aboutlannpya")
                    //}
                    //  }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "technical_problem")
            {
                var reply = context.MakeMessage();
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                    new HeroCard
                      {
                          Title = $"App crashes.",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Crash", value:$"appcrashes_problem")
                    }
                      }.ToAttachment(),
                       new HeroCard
                      {
                          Title = $"I want to change the Language settings.",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Language", value:$"language_problem")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"I want to change the notification settings.",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Notification", value:$"notification_problem")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"I can’t make a phone call through the Lann Pya Directory.",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Phone", value:$"phonecall_problem")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "appcrashes_problem")
            {
                var reply = context.MakeMessage();
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                    new HeroCard
                      {
                          Title = $"Make sure your Android phone is up-to-date",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Android version", value:$"androidversion_problem")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = $"Check if you have the latest version of Lann Pya installed.",
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.ImBack, "Lannpya version", value:$"appversion_problem")
                    }
                      }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
            else if (text == "androidversion_problem" || text == "language_problem" || text == "notification_problem" || text == "phonecall_problem")
            {
                var reply = context.MakeMessage();
                if (text == "androidversion_problem")
                {
                    var reply1 = msg.CreateReply("1. Go to your phone settings");
                    await context.PostAsync(reply1);
                    await context.PostAsync(msg.ResponseTyping());
                    var reply2 = msg.CreateReply("2. Select 'About phone'");
                    await context.PostAsync(reply2);
                    await context.PostAsync(msg.ResponseTyping());
                    var reply3 = msg.CreateReply("3. Select 'Software information' and look for Android version. ");
                    await context.PostAsync(reply3);
                    await context.PostAsync(msg.ResponseTyping());
                    var reply4 = msg.CreateReply("4. Make sure your Android is at least version 4.4.. ");
                    await context.PostAsync(reply4);
                    await context.PostAsync(msg.ResponseTyping());
                    var reply5 = msg.CreateReply("If it needs to be updated, go to settings. Select: 'Software update.' Then click 'Download and install.'");
                    await context.PostAsync(reply5);
                }
                else if (text == "language_problem")
                {
                    var reply1 = msg.CreateReply("1. Open the Lann Pya App");
                    await context.PostAsync(reply1);
                    await context.PostAsync(msg.ResponseTyping());
                    var reply2 = msg.CreateReply("2. Go to settings (the gear icon, top right)");
                    await context.PostAsync(reply2);
                    await context.PostAsync(msg.ResponseTyping());
                    var reply3 = msg.CreateReply("3. Choose the first option: 'Language'");
                    await context.PostAsync(reply3);
                    await context.PostAsync(msg.ResponseTyping());
                    var reply4 = msg.CreateReply("4. Choose a language from the language options and click the blue button.");
                    await context.PostAsync(reply4);
                }
                else if (text == "notification_problem")
                {
                    var reply1 = msg.CreateReply("1. Go to your phone settings");
                    await context.PostAsync(reply1);
                    await context.PostAsync(msg.ResponseTyping());
                    var reply2 = msg.CreateReply("2. Select 'Apps'");
                    await context.PostAsync(reply2);
                    await context.PostAsync(msg.ResponseTyping());
                    var reply3 = msg.CreateReply("3. Select 'Lann Pya'");
                    await context.PostAsync(reply3);
                    await context.PostAsync(msg.ResponseTyping());
                    var reply4 = msg.CreateReply("4. Select 'Notifications'");
                    await context.PostAsync(reply4);
                    await context.PostAsync(msg.ResponseTyping());
                    var reply5 = msg.CreateReply("5. Press: 'Show notifications' to show or to hide notifications on your phone.");
                    await context.PostAsync(reply5);
                }
                else
                {
                    var reply1 = msg.CreateReply("1. Go to your phone settings");
                    await context.PostAsync(reply1);
                    await context.PostAsync(msg.ResponseTyping());
                    var reply2 = msg.CreateReply("2. Select 'Apps'");
                    await context.PostAsync(reply2);
                    await context.PostAsync(msg.ResponseTyping());
                    var reply3 = msg.CreateReply("3. Select 'Lann Pya'");
                    await context.PostAsync(reply3);
                    await context.PostAsync(msg.ResponseTyping());
                    var reply4 = msg.CreateReply("4. Select 'Permissions'");
                    await context.PostAsync(reply4);
                    await context.PostAsync(msg.ResponseTyping());
                    var reply5 = msg.CreateReply("5. Click on 'Phone' to give permission to Lann Pya so that you can make calls through our directory. ");
                    await context.PostAsync(reply5);
                }
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments = new List<Attachment> {
                    new HeroCard
                      {
                          Title = $"Still not working? Send feedback!",
                    Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/feedback.png") },
                    Buttons = new List<CardAction>
                          {
                        new CardAction(ActionTypes.OpenUrl, "Feedback", value:$"http://lannpya.com/ForMessengerBot/SuggestionForm?MsgID={msg.From.Id}&UserName={msg.From.Name}")
                    }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = "Back to main menu",
                          Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/lannpya_logo1.png") },
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack,"Menu", value: $"🤔 Get started")
                          }
                      }.ToAttachment(),
                    //  new HeroCard
                    //  {
                    //      Title = $"Go to About Lannpya",
                    //Images = new List<CardImage> { new CardImage("https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/law/LannpyaBot/about_lannpya_chatbot.png") },
                    //Buttons = new List<CardAction>
                    //      {
                    //    new CardAction(ActionTypes.ImBack, "About Lannpya", value:$"aboutlannpya")
                    //}
                    //  }.ToAttachment()
                  };
                await context.PostAsync(reply);
            }
        }

        private async Task ResumeAfterCalculateTaxEngDialog(IDialogContext context, IAwaitable<object> result)
        {
            var taxName = taxinfo.Split('_')[1];
            var reply = context.MakeMessage();
            reply.Text = "Here is everything about taxes that you should know";
            reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
            reply.Attachments = new List<Attachment> {
                       new HeroCard
                      {
                          Title = $"Pay your {taxName}",
                          Text = "An easy way to pay your taxes. Let's check it out",
                          Images = new List<CardImage> { new CardImage(ResourceHelper.taxexpenditure_img_url) },
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack, "Pay now", value:$"paytax_{taxinfo}")
                          }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = "Tax expenditure",
                          Text = "Learn about where your money is going?",
                          Images = new List<CardImage> { new CardImage(ResourceHelper.tax_img_url) },
                          Buttons = new List<CardAction>
                          {
                             new CardAction(ActionTypes.OpenUrl, "Learn now", value: "http://google.com")
                          }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = "Start over",
                          Text = "Go abck to learn more about other taxes",
                          Images = new List<CardImage> { new CardImage(ResourceHelper.tax_img_url) },
                          Buttons = new List<CardAction>
                          {
                             new CardAction(ActionTypes.ImBack, "🤔 Start over", value: "🤔 Start over")
                          }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = "Get help",
                          Text = "Are you confused",
                          Images = new List<CardImage> { new CardImage(ResourceHelper.tax_img_url) },
                          Buttons = new List<CardAction>
                          {
                             new CardAction(ActionTypes.ImBack, "🙋‍ Get Help", value: "🙋‍ Get Help")
                          }
                      }.ToAttachment()

                  };
            await context.PostAsync(reply);
        }
        private async Task ResumeAfterEngFFDialog(IDialogContext context, IAwaitable<object> result)
        {
            var reply = context.MakeMessage();
            reply.Text = "Something else! What can I help you?";
            reply.Type = ActivityTypes.Message;
            reply.TextFormat = TextFormatTypes.Plain;
            reply.SuggestedActions = new SuggestedActions()
            {
                Actions = (new List<string> { "🤔 Start over", "🙋‍ Get Help" }).Select(a => new CardAction
                {
                    Title = a,
                    Type = ActionTypes.ImBack,
                    Value = a
                }).ToList()
            };

            await context.PostAsync(reply);
            context.Wait(MessageReceivedAsync);
        }
        private async Task ResumeAfterUNFFDialog(IDialogContext context, IAwaitable<object> result)
        {
            var reply = context.MakeMessage();
            reply.Text = "ဘာကူညီပေးရပါမလဲ ခင်ဗျာ";
            reply.Type = ActivityTypes.Message;
            reply.TextFormat = TextFormatTypes.Plain;
            reply.SuggestedActions = new SuggestedActions()
            {
                Actions = (new List<string> { "🤔 ပြန်စမည်" }).Select(a => new CardAction
                {
                    Title = a,
                    Type = ActionTypes.ImBack,
                    Value = a
                }).ToList()
            };

            await context.PostAsync(reply);
            context.Wait(MessageReceivedAsync);
        }
        private async Task ResumeAfterZGFFDialog(IDialogContext context, IAwaitable<object> result)
        {
            var reply = context.MakeMessage();
            reply.Text = "ဘာကူညီေပးရပါမလဲ ခင္ဗ်ာ";
            reply.Type = ActivityTypes.Message;
            reply.TextFormat = TextFormatTypes.Plain;
            reply.SuggestedActions = new SuggestedActions()
            {
                Actions = (new List<string> { "🤔 ျပန္စမည္" }).Select(a => new CardAction
                {
                    Title = a,
                    Type = ActionTypes.ImBack,
                    Value = a
                }).ToList()
            };

            await context.PostAsync(reply);
            context.Wait(MessageReceivedAsync);
        }
    }
}