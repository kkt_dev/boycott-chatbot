﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Connector;
using LannpyaBot.Helper;
using LannpyaBot.Models;

namespace LannpyaBot.Dialogs
{
    [Serializable]
    public class StartDialog_ZG : IDialog<object>
    {
        string city, taxinfo;
        int cityid;
        public async Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync);

            //return Task.CompletedTask;
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<object> result)
        {

            var msg = await result as Activity;
            //var profileObj = CryptologyHelper.EncryptString($"{msg.From.Id},{msg.From.Name}", "myotaw2019");
            if (msg.Text.isWelcome_words())
            {
                var reply = context.MakeMessage();
                reply.Text = $"Hi {msg.From.Name} Welcome to MyoTaw chatbot. Plese tap below to get started. \nမြို့တော် ချက်ဘော့မှ ကြိုဆိုပါတယ်ရှင့်။ လူကြီးမင်းစတင်အသုံးပြုရန် အောက်တွင်နှိပ်ပေးပါ။ \nၿမိဳ့ေတာ္ ခ္က္ေဘာ့မြ ႀကိဳဆိုပါတယ္႐ြင့္။ လူႀကီးမင္းစတင္အသံုး်ပဳရႏ္ ေအာၾကၱငၷြိပ္ေပးပါ။";
                reply.Type = ActivityTypes.Message;
                reply.TextFormat = TextFormatTypes.Plain;
                reply.SuggestedActions = new SuggestedActions()
                {
                    Actions = (new List<string> { "🤔 Get started", "🤔 စတင်မည်", "🤔 စတင္မည္" }).Select(a => new CardAction
                    {
                        Title = a,
                        Type = ActionTypes.ImBack,
                        Value = a
                    }).ToList()
                };
                await context.PostAsync(reply);
            }
            else if (msg.Text.isStart_words())
            {
                await context.Forward(new StartDialog(), this.ResumeAfterEngFFDialog, msg, CancellationToken.None);
                return;
            }
            else if (msg.Text.isStart_words_UN())
            {
                await context.Forward(new StartDialog_UN(), this.ResumeAfterUNFFDialog, msg, CancellationToken.None);
                return;
            }
            

            else if (msg.Text == "🙋‍ Get Help")
            {
                var reply = context.MakeMessage();
                reply.Text = "သင္သိသင့္ေသာ အခြန္ဆိုင္ရာ အေၾကာင္းအရာမ်ား";
                reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                reply.Attachments.Add(
                      new HeroCard
                      {
                          Title = "အကူအညီရယူမည္",
                          Text = $"သင့္ အႀကံေပးခ်က္ကို သိပါရေစ။ အကူအညီရယူႏိုင္ရင္ ဖုန္းေခၚဆိုပါ။",
                          Images = new List<CardImage> { new CardImage(ResourceHelper.tax_img_url) },
                          Buttons = new List<CardAction>
                          {
                              new CardAction(ActionTypes.ImBack, "အႀကံေပးခ်က္", value: "Suggestion"),
                              new CardAction(ActionTypes.OpenUrl, "ဖုန္းေခၚမယ္", value: "tel:+95-9-796926583")
                          }
                      }.ToAttachment());
                await context.PostAsync(reply);
            }
            else if (msg.Text.ToLower().isFeedback_words())
            {

                var form = new FormDialog<SuggestionEngFormFlowDialog>(new SuggestionEngFormFlowDialog(), () => SuggestionEngFormFlowDialog.BuildForm(msg.From.Name, msg.From.Id), FormOptions.None, null);
                await context.Forward(form, ResumeAfterEngFFDialog, msg, CancellationToken.None);
                return;
            }

            context.Wait(MessageReceivedAsync);
        }
        private async Task ResumeAfterCalculateTaxEngDialog(IDialogContext context, IAwaitable<object> result)
        {
            var taxName = taxinfo.Split('_')[1];
            var reply = context.MakeMessage();
            reply.Text = "အခြန္နဲ႔ ပတ္သပ္ၿပီး သင္သိသင့္ေသာ အေၾကာင္းအရာအားလုံး ဒီမွာပါရွိပါတယ္။";
            reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
            reply.Attachments = new List<Attachment> {

                       new HeroCard
                      {
                          Title = $"သင့္ရဲ႕ {taxName} အခြန္ ကို ေပးေဆာင္ပါ။",
                          Text = "ဒီမွာ သင့္အခြန္ကို ေပးေဆာင္ဖို႔ လြယ္ကူအဆင္ေျပေေစတဲ့ နည္းလမ္းပါ။ ၾကည့္ၾကည့္လိုက္ရေအာင္။",
                          Images = new List<CardImage> { new CardImage(ResourceHelper.taxexpenditure_img_url) },
                          Buttons = new List<CardAction>
                          {
                                  new CardAction(ActionTypes.ImBack, "ေပးေဆာင္မည္", value:$"paytax_{taxinfo}")
                          }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = "အခြန္သုံးစြဲစရိတ္",
                          //Text = "Learn about where your money is going?",
                          Text = "လူႀကီးမင္းပိုက္ဆံ ဘယ္ေနရာအတြက္အသုံးခ်ေနလဲဆိုတာ ေလ့လာမယ္။",
                          Images = new List<CardImage> { new CardImage(ResourceHelper.tax_img_url) },
                          Buttons = new List<CardAction>
                          {
                             new CardAction(ActionTypes.OpenUrl, "ေလ့လာမယ္။", value: "http://google.com")
                          }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = "အစကျပန္စမယ္",
                          Text = "အခြန္အေၾကာင္း ပိုမိုေလ့လာမည္။",
                          Images = new List<CardImage> { new CardImage(ResourceHelper.tax_img_url) },
                          Buttons = new List<CardAction>
                          {
                             new CardAction(ActionTypes.ImBack, "🤔 ျပန္စမယ္", value: "🤔 ျပန္စမယ္")
                          }
                      }.ToAttachment(),
                      new HeroCard
                      {
                          Title = "အကူအညီ ရယူမည္။",
                          Text = "သင္ရႈပ္ေထြးေနလား။",
                          Images = new List<CardImage> { new CardImage(ResourceHelper.tax_img_url) },
                          Buttons = new List<CardAction>
                          {
                             new CardAction(ActionTypes.ImBack, "🙋‍ အကူအညီ", value: "🙋‍ အကူအညီ")
                          }
                      }.ToAttachment()

                  };
            await context.PostAsync(reply);
        }
        private async Task ResumeAfterEngFFDialog(IDialogContext context, IAwaitable<object> result)
        {
            var reply = context.MakeMessage();
            reply.Text = "တျခားသိလိုသည့္အေၾကာင္းအရာမ်ားရွိေသးလား! ဘာကူညီေပးရမလဲ?";
            reply.Type = ActivityTypes.Message;
            reply.TextFormat = TextFormatTypes.Plain;
            reply.SuggestedActions = new SuggestedActions()
            {
                Actions = (new List<string> { "🤔 ျပန္စမယ္", "🙋‍ အကူအညီ" }).Select(a => new CardAction
                {
                    Title = a,
                    Type = ActionTypes.ImBack,
                    Value = a
                }).ToList()
            };

            await context.PostAsync(reply);
            context.Wait(MessageReceivedAsync);
        }
        private async Task ResumeAfterUNFFDialog(IDialogContext context, IAwaitable<object> result)
        {
            var reply = context.MakeMessage();
            reply.Text = "ဘာကူညီပေးရပါမလဲ ရှင့်";
            reply.Type = ActivityTypes.Message;
            reply.TextFormat = TextFormatTypes.Plain;
            reply.SuggestedActions = new SuggestedActions()
            {
                Actions = (new List<string> { "🤔 ပြန်စမည်" }).Select(a => new CardAction
                {
                    Title = a,
                    Type = ActionTypes.ImBack,
                    Value = a
                }).ToList()
            };

            await context.PostAsync(reply);
            context.Wait(MessageReceivedAsync);
        }
        //private async Task ResumeAfterPayPropertyTaxEngDialog(IDialogContext context, IAwaitable<object> result)
        //{

        //    var argument = await result as PayTaxEngFormFlow_ZG;

        //    var reply = context.MakeMessage();
        //    reply.Text = "သင္သိေသာ အခြန္ဆိုင္ရာ အေၾကာင္းအရာမ်ား။";
        //    reply.AttachmentLayout = AttachmentLayoutTypes.Carousel;
        //    reply.Attachments = new List<Attachment> {
        //              new HeroCard
        //              {
        //                    Title = "MPU ျဖင့္ေပးေခ်မည္။",
        //                    Subtitle = $"{argument.TaxName} အတြက္ေပးေခ်မႈ စတင္မည္။",
        //                    Text = "စတင္မယ္",
        //                    Images = new List<CardImage> { new CardImage(ResourceHelper.mpu_img_url) },
        //                    Buttons = new List<CardAction>
        //                    {
        //                        new CardAction(ActionTypes.OpenUrl, "ေပးေဆာင္မည္", value:$"http://kktmpupayment.azurewebsites.net/mpupayment/index?userdefined1={argument.TaxId}&amount={argument.Amount}&productDescription={argument.TaxName}&categorycode={argument.UserId}&userdefined2={context.Activity.From.Id}"),
        //                        new CardAction(ActionTypes.OpenUrl, "ဆက္သြယ္ရန္", value:"tel:+95-9796926583")
        //                    }
        //              }.ToAttachment(),
        //              new HeroCard
        //              {
        //                    Title = "Wave Money ျဖင့္ေပးေခ်မည္။",
        //                    Subtitle = $"{argument.TaxName} အတြက္ေပးေခ်မႈ စတင္မည္။",
        //                    Text = "စတင္မယ္",
        //                    Images = new List<CardImage> { new CardImage(ResourceHelper.wavemoney_img_url) },
        //                    Buttons = new List<CardAction>
        //                    {
        //                        new CardAction(ActionTypes.OpenUrl, "ေပးေဆာင္မည္", value:$"http://kktmpupayment.azurewebsites.net/mpupayment/index?userdefined1={argument.TaxId}&amount={argument.Amount}&productDescription={argument.TaxName}&categorycode={argument.UserId}&userdefined2={context.Activity.From.Id}"),
        //                        new CardAction(ActionTypes.OpenUrl, "ဆက္သြယ္ရန္", value:"tel:+95-9796926583")
        //                    }
        //              }.ToAttachment(),

        //          };

        //    await context.PostAsync(reply);
        //    context.Wait(MessageReceivedAsync);
        //}
    }
}