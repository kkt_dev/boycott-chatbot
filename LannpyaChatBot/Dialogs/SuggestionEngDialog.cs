﻿using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Builder.FormFlow.Advanced;
using Microsoft.Bot.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace LannpyaBot.Dialogs
{
    public enum ConfirmOptions
    {
        [Describe("👌 Yes")]
        [Terms(new string[] { "y", "Y", "Yeah", "Yay", "Yes", "👌 Yes" })]
        Yes,
        [Describe("👎 No")]
        [Terms(new string[] { "n", "N", "No", "Nope", "No", "👎 No" })]
        No
    };
    public enum Subjects
    {
        [Describe("General")]
        [Terms(new string[] { "General" })]
        General,
        [Describe("Payment")]
        [Terms(new string[] { "Payment" })]
        Payment,
        [Describe("Tax")]
        [Terms(new string[] { "Tax" })]
        TaxCollection,
        [Describe("Municipal service")]
        [Terms(new string[] { "Municipal service" })]
        MunicipalService
    };
    public enum RateOptions
    {
        [Describe("👌 Not relevant")]
        [Terms(new string[] { "👌 Not relevant", "not relevant", "Funny", "funny", "Not useful", "not useful" })]
        NotRelevant,
        [Describe("👌 Relevant")]
        [Terms(new string[] { "👌 Relevant", "Relevant", "Fine", "fine", "Okay", "okay" })]
        Relevant,
        [Describe("👌 Helpful")]
        [Terms(new string[] { "👌 Helpful", "Helpful", "Great", "great", "Useful", "useful" })]
        Userful,
        [Describe("👌 Very Helpful")]
        [Terms(new string[] { "👌 Very Helpful", "Very useful", "very useful", "So great", "so great" })]
        VeryUseful
    };
    public class SuggestionEngFormFlowDialog
    {
        [Template(TemplateUsage.EnumSelectOne, "If it is your name say 'Yes'. otherwise, say 'No' {||}", ChoiceStyle = ChoiceStyleOptions.Buttons)]
        public ConfirmOptions? NameConfirmation;
        [Template(TemplateUsage.EnumSelectOne, "Please select the subject below you want to complaint or suggest. {||}", ChoiceStyle = ChoiceStyleOptions.Buttons)]
        public Subjects? Subject;
        [Template(TemplateUsage.String, "😊 Okay. For '{Subject}', what do you want to suggest us?")]
        public string Suggestion;
        [Template(TemplateUsage.EnumSelectOne, "We got it. We would like to know how much it is important? {||}", ChoiceStyle = ChoiceStyleOptions.Buttons)]
        public RateOptions? Importance;
        [Template(TemplateUsage.String, "May I know your name?")]
        public string Name;
        [Pattern(@"(<Undefined control sequence>\d)?\s*\d{3}(-|\s*)\d{4}")]
        [Template(TemplateUsage.String, "For the last thing we need your number to contact you. Please start with number (09)")]
        public string PhoneNumber;
        
        public static IForm<SuggestionEngFormFlowDialog> BuildForm(string username, string userid)
        {

            return new FormBuilder<SuggestionEngFormFlowDialog>()
            .Message($"Hi {username}, thanks for giving us feedback. '{username}' is right for your name and okay to use for future?")
            //.Message("Hey welcome! Gogoticket bot is to help you for getting ticket easy. We will improve it overtime. It is beta testing. But you can actually buy it.")
            .Field(nameof(NameConfirmation))
            .Field(new FieldReflector<SuggestionEngFormFlowDialog>(nameof(Name))
                .SetType(typeof(string))
                .SetActive((state) => (state.NameConfirmation == ConfirmOptions.No ? true : false))
                .SetDefine(async (state, field) =>
                {
                    if (state.NameConfirmation == ConfirmOptions.Yes)
                    {
                        field.SetValue(state, username);
                    }
                    field.SetPrompt(new PromptAttribute("So, let me know your name to contact you in future."));
                    return await Task.FromResult(true);
                })
                .SetValidate(async (state, response) =>
                {
                    var result = new ValidateResult { IsValid = true, Value = response };
                    var value = (response as string).Trim();
                    if (value.Length < 1)
                    {
                        result.Feedback = "Please provide your name.";
                        result.IsValid = false;
                    }
                    return await Task.FromResult(result);
                }))
            .Field(nameof(Subject))
            .Field(nameof(Suggestion),active: (state) => { return state.Subject != null ? true : false ; },
                validate: async (state, response) =>
                {
                    var result = new ValidateResult { IsValid = true, Value = response };
                    var Suggestion = (response as string).Trim();
                    if (Suggestion == null || Suggestion.Length < 0)
                    {
                        result.Feedback = "Say something you want to let us know. I mean you can ask any question or complaint about me.";
                        result.IsValid = false;
                    }
                    return result;
                })
            .Field(new FieldReflector<SuggestionEngFormFlowDialog>(nameof(Importance))
              .SetType(null)
              .SetDefine(async (state, field) =>
              {
                  List<string> choices = new List<string>() { "✌️ Just complaint", "✌ Important", "✌️ Seriously important" };
                  foreach (string item in choices)
                  {
                      field
                          .AddDescription(item, item)
                          .AddTerms(item, item);
                  }
                 
                  return await Task.FromResult(true); ;
              }))

            .Field(nameof(PhoneNumber),
                validate: async (state, response) =>
                {
                    var result = new ValidateResult { IsValid = true, Value = response };
                    var value = (response as string).Trim();
                    if (value.Length > 0 && (value[0] < '0' || value[0] > '9'))
                    {
                        result.Feedback = "It should be started with number like (e.g. 09)";
                        result.IsValid = false;
                    }
                    return result;
                })
             .OnCompletion(async (context, state) =>
             {
                 //"Thank you for giving feedback. We will get back to you."
                 await context.PostAsync("Your suggestions and complaints are very useful and I love hearing them.");
             })
             .Prompter(async (context, prompt, state, field) =>
             {
                 IMessageActivity promptMessage = context.MakeMessage();
                 // Handle buttons as quick replies when possible (FB only renders 11 quick replies)
                 if (field != null)
                 {
                     if (prompt.Buttons.Count > 0 && prompt.Buttons.Count <= 11)
                     {
                         // Build a standard prompt with suggested actions. 

                         promptMessage.Text = prompt.Prompt;
                         var actions = prompt.Buttons.Select(button => new CardAction
                         {
                             Type = ActionTypes.ImBack,
                             Title = button.Description,
                             Value = button.Description
                         }).ToList();
                         promptMessage.SuggestedActions = new SuggestedActions(actions: actions);
                     }

                     else if (prompt.Description != null)
                     {
                         var preamble = context.MakeMessage();
                         if (prompt.GenerateMessages(preamble, promptMessage))
                         {
                             await context.PostAsync(preamble);
                         }

                     }
                 }
                 if (prompt != null)
                 {
                     promptMessage.Text = prompt.Prompt ?? default(string);
                 }

                 await context.PostAsync(promptMessage);
                 return prompt;
             })
            .Build();
        }
    }
}