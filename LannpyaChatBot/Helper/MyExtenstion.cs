﻿using LannpyaBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LannpyaBot.Helper
{
    public class MyExtenstion
    {
        public Chatbot_KPI GetEvent(string text = null)
        {
            //string[] result = new string[] { };
            Chatbot_KPI obj = new Chatbot_KPI();
            if (text == "landmap") { obj.EventName = "Land"; obj.EventTitle = "Land"; }
            else if (text == "criminaljustice") { obj.EventName = "Criminal Justice"; obj.EventTitle = "Criminal Justice"; }
            else if (text == "worklawyers") { obj.EventName = "How to work with Lawyers"; obj.EventTitle = "How to work with Lawyers"; }
            else if (text == "citizenship_dt") { obj.EventTitle = "Identity Documents"; obj.EventName = "Identity Documents"; }
            else if (text == "migration") { obj.EventTitle = "Migration"; obj.EventName = "Migration"; }
            else if (text == "findlaws") { obj.EventTitle = "Find Laws"; obj.EventName = "Find Laws"; }
            else if (text == "Feedback") { obj.EventTitle = "Feedback"; obj.EventName = "Feedback"; }
            else if (text == "criminallyaccused_rights") { obj.EventTitle = "Criminally Accused"; obj.EventName = "Criminally Accused"; }
            else if (text == "complainantwitnesses_rights") { obj.EventTitle = "Complainant & Witnesses"; obj.EventName = "Complainant & Witnesses"; }
            else if (text == "womenrights") { obj.EventTitle = "Women's Rights"; obj.EventName = "Women's Rights"; }
            else if (text == "childrights") { obj.EventTitle = "Child Rights"; obj.EventName = "Child Rights"; }
            else if (text == "lgbtqirights") { obj.EventTitle = "LGBTQI Rights"; obj.EventName = "LGBTQI Rights"; }
            else if (text == "landrights") { obj.EventTitle = "Land Rights"; obj.EventName = "Land Rights"; }
            else if (text == "labourrights") { obj.EventTitle = "Labour Rights"; obj.EventName = "Labour Rights"; }
            else if (text == "aboutus") { obj.EventTitle = "About Us"; obj.EventName = "About Us"; }
            else if (text == "howtodownload") { obj.EventTitle = "How to download"; obj.EventName = "How to download"; }
            else if (text == "havingproblem") { obj.EventTitle = "I'm having problems with the app"; obj.EventName = "I'm having problems with the app"; }
            else if (text == "registerapp") { obj.EventTitle = "Register on the app directory"; obj.EventName = "Register on the app directory"; }
            else if (text == "privacypolicy") { obj.EventTitle = "Privacy policy"; obj.EventName = "Privacy policy"; }
            if (text == "know_landmap")
            {
                obj.EventName = "Back to land"; obj.EventTitle = "Land"; obj.IsEventEnd = true; obj.EventType = "Text";
            }
            else if (text == "downloadlp_justice")
            {
                obj.EventName = "Install Lannpya now"; obj.EventTitle = "Criminal Justice"; obj.IsEventEnd = true; obj.EventType = "Download";
            }
            else if (text == "learnmore_justice")
            {
                obj.EventName = "Learn more"; obj.EventTitle = "Criminal Justice"; obj.IsEventEnd = true; obj.EventType = "Text";
            }
            else if (text == "endcriminaljustice")
            {
                obj.EventName = "Back to criminal justice"; obj.EventTitle = "Criminal Justice"; obj.IsEventEnd = true; obj.EventType = "Text";
            }
            else if (text == "predownloadlp_justice")
            {
                obj.EventName = "Find a lawyer"; obj.EventTitle = "Criminal Justice"; obj.IsEventEnd = true; obj.EventType = "Download";
            }
            else if (text == "endworklawyers")
            {
                obj.EventName = "I'd like to learn more about lawyers."; obj.EventTitle = "How to work with Lawyers"; obj.IsEventEnd = true; obj.EventType = "Download";
            }
            else if (text == "hire_worklawyers")
            {
                obj.EventName = "Okay, I'd like to hire a lawyer."; obj.EventTitle = "How to work with Lawyers"; obj.IsEventEnd = true; obj.EventType = "Text";
            }
            else if (text == "continue_Yes_citizenship")
            {
                obj.EventName = "No, thanks. Take me back to the previous question."; obj.EventTitle = "Identity Documents"; obj.IsEventEnd = true; obj.EventType = "Text";
            }
            else if (text == "looking_migration")
            {
                obj.EventName = "I'm looking to migrate abroad"; obj.EventTitle = "Migration"; obj.IsEventEnd = true; obj.EventType = "Text";
            }
            else if (text == "policestation_yes_migration")
            {
                obj.EventName = "Yes, I will report"; obj.EventTitle = "Migration"; obj.IsEventEnd = true; obj.EventType = "Text";
            }
            else if (text == "policestation_no_migration")
            {
                obj.EventName = "No"; obj.EventTitle = "Migration"; obj.IsEventEnd = true; obj.EventType = "Text";
            }
            else if (text == "policestation_help_migration")
            {
                obj.EventName = "I need help"; obj.EventTitle = "Migration"; obj.IsEventEnd = true; obj.EventType = "Text";
            }
            else if (text == "contactofficer_migration")
            {
                obj.EventName = "Contact an ILO liaison officer to make a complaint"; obj.EventTitle = "Migration"; obj.IsEventEnd = true; obj.EventType = "Text";
            }
            else if (text == "learnmyrights")
            {
                obj.EventName = "Learn My Rights"; obj.EventTitle = "Migration"; obj.IsEventEnd = true; obj.EventType = "Text";
            }
            else if (text == "searchdatabase")
            {
                obj.EventName = "Great! I'd like to search for a law on the database."; obj.EventTitle = "Find Law"; obj.IsEventEnd = true; obj.EventType = "Text";
            }
            else if (text == "criminallyaccused_rights_Learnmore_Yes")
            {
                obj.EventName = "Yes"; obj.EventTitle = "Criminally Accused"; obj.IsEventEnd = true; obj.EventType = "Download";
            }
            else if (text == "criminallyaccused_rights_Learnmore_No")
            {
                obj.EventName = "No"; obj.EventTitle = "Criminally Accused"; obj.IsEventEnd = true; obj.EventType = "Text";
            }
            else if (text == "about_lannpya")
            {
                obj.EventName = "LannPya"; obj.EventTitle = "About us"; obj.IsEventEnd = true; obj.EventType = "Text";
            }
            else if (text == "about_myjusstice")
            {
                obj.EventName = "MyJustice"; obj.EventTitle = "About us"; obj.IsEventEnd = true; obj.EventType = "Text";
            }
            else if (text == "about_koekoetech")
            {
                obj.EventName = "Koe Koe Tech"; obj.EventTitle = "About us"; obj.IsEventEnd = true; obj.EventType = "Text";
            }
            else if (text == "about_acknowledgements")
            {
                obj.EventName = "Acknowledgements"; obj.EventTitle = "About us"; obj.IsEventEnd = true; obj.EventType = "Text";
            }
            else if (text == "fisnished_download")
            {
                obj.EventName = "Install Lannpya"; obj.EventTitle = "How to download"; obj.IsEventEnd = true; obj.EventType = "Text";
            }
            else if (text == "space_problem")
            {
                obj.EventName = "My device does not have enough storage space."; obj.EventTitle = "I'm having problems with the app"; obj.IsEventEnd = true; obj.EventType = "Text";
            }
            else if (text == "updating_problem")
            {
                obj.EventName = "I cannot update Lann Pya"; obj.EventTitle = "I'm having problems with the app"; obj.IsEventEnd = true; obj.EventType = "Text";
            }
            else if (text == "android_problem_No")
            {
                obj.EventName = "No"; obj.EventTitle = "I'm having problems with the app"; obj.IsEventEnd = true; obj.EventType = "Text";
            }
            else if (text == "androidversion_problem")
            {
                obj.EventName = "Make sure your Android phone is up-to-date"; obj.EventTitle = "I'm having problems with the app"; obj.IsEventEnd = true; obj.EventType = "Text";
            }
            else if (text == "appversion_problem")
            {
                obj.EventName = "Check if you have the latest version of Lann Pya installed."; obj.EventTitle = "I'm having problems with the app"; obj.IsEventEnd = true; obj.EventType = "Text";
            }
            else if (text == "language_problem")
            {
                obj.EventName = "I want to change the Language settings."; obj.EventTitle = "I'm having problems with the app"; obj.IsEventEnd = true; obj.EventType = "Text";
            }
            else if (text == "notification_problem")
            {
                obj.EventName = "I want to change the notification settings."; obj.EventTitle = "I'm having problems with the app"; obj.IsEventEnd = true; obj.EventType = "Text";
            }
            else if (text == "phonecall_problem")
            {
                obj.EventName = "I can’t make a phone call through the Lann Pya Directory."; obj.EventTitle = "I'm having problems with the app"; obj.IsEventEnd = true; obj.EventType = "Text";
            }
            return obj;
        }

        public string GetEventForImpression(string text = null)
        {
            string result = null;
            if (text.Contains("landmap"))
            {
                result = "Land";
            }
            else if (text.Contains("justice"))
            {
                result = "Criminal Justice";
            }
            else if (text.Contains("worklawyers"))
            {
                result = "How to work with lawyer";
            }
            else if (text.Contains("citizenship"))
            {
                result = "Identity Documents";
            }
            else if (text.Contains("migration"))
            {
                result = "Migration";
            }
            else if (text == "findlaws" || text == "searchdatabase")
            {
                result = "Find Law";
            }
            else if (text == "criminallyaccused_rights" || text == "criminallyaccused_rights_Yes" || text == "criminallyaccused_rights_Really" || text == "criminallyaccused_rights_Learnmore_Yes" || text == "criminallyaccused_rights_Learnmore_No")
            {
                result = "Criminally Accused";
            }
            else if (text == "complainantwitnesses_rights" || text == "complainantwitnesses_rights_Yes" || text == "complainantwitnesses_rights_Really")
            {
                result = "Complainant & Witnesses";
            }
            else if (text == "womenrights" || text == "womenrights_Yes" || text == "womenrights_Really")
            {
                result = "Women's Rights";
            }
            else if (text == "childrights" || text == "childrights_Yes" || text == "childrights_Really")
            {
                result = "Child Rights";
            }
            else if (text == "lgbtqirights" || text == "lgbtqirights_Yes" || text == "lgbtqirights_Really")
            {
                result = "LGBTQI Rights";
            }
            else if (text == "landrights" || text == "landrights_Yes" || text == "landrights_Really")
            {
                result = "Land Rights";
            }
            else if (text == "labourrights" || text == "labourrights_Yes" || text == "labourrights_Really")
            {
                result = "Labour Rights";
            }
            else if (text == "aboutlannpya" || text == "aboutus" || text == "about_lannpya" || text == "about_myjusstice" || text == "about_koekoetech" || text == "about_acknowledgements")
            {
                result = "About us";
            }
            else if (text == "howtodownload" || text == "downloadfromplaystore" || text == "downloadfromfacebook" || text == "stepone_dl_ps" || text == "steptwo_dl_ps" || text == "stepthree_dl_ps" || text == "fisnished_download" || text == "stepone_dl_fb" || text == "steptwo_dl_fb" || text == "stepthree_dl_fb")
            {
                result = "How to download";
            }
            else if (text.Contains("problem"))
            {
                result = "I'm having problems with the app";
            }
            else if (text == "registerapp")
            {
                result = "Register on the app directory";
            }
            else if (text == "privacypolicy")
            {
                result = "Privacy policy";
            }
            return result;
        }
    }
}