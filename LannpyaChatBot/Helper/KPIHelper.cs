﻿using LannpyaBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LannpyaBot.Helper
{
    public class KPIHelper
    {
        public Chatbot_KPI GetEvent(string text = null)
        {
            Chatbot_KPI obj = new Chatbot_KPI();
            //if (text == "whyelectionimportant") { obj.EventName = "Why Election Important?"; obj.EventTitle = "Why Election Important?"; }
            if (text == "fdlc") { obj.EventName = "Food, Drink, Liquor and Cigarettes"; obj.EventTitle = "Food, Drink, Liquor and Cigarettes"; }
            else if (text == "hpservices") { obj.EventName = "Health Products and Services"; obj.EventTitle = "Health Products and Services"; }
            else if (text == "manandretail") { obj.EventTitle = "Manufacturing and Retail"; obj.EventName = "Manufacturing and Retail"; }
            else if (text == "bankandin") { obj.EventTitle = "Banking and Industrial"; obj.EventName = "Banking and Industrial"; }
            else if (text == "enterandtran") { obj.EventTitle = "Entertainment/Tourism and Transport"; obj.EventName = "Entertainment/Tourism and Transport"; }
            else if (text == "commuandmedia") { obj.EventTitle = "Communications and Media"; obj.EventName = "Communications and Media"; }
            else if (text == "consandtrade") { obj.EventTitle = "Construction and Trading"; obj.EventName = "Construction and Trading"; }            
            return obj;
        }

        public string GetEventForImpression(string text = null)
        {
            string result = null;
            if (text == "fdlc")
            {
                result = "Food, Drink, Liquor and Cigarettes";
            }
            else if (text == "hpservices")
            {
                result = "Health Products and Services";
            }
            else if (text == "manandretail")
            {
                result = "Manufacturing and Retail";
            }
            else if (text == "bankandin")
            {
                result = "Banking and Industrial";
            }
            else if (text == "enterandtran")
            {
                result = "Entertainment/Tourism and Transport";
            }
            else if (text == "commuandmedia")
            {
                result = "Communications and Media";
            }
            else if (text == "consandtrade")
            {
                result = "Construction and Trading";
            }
            return result;
        }
    }
}