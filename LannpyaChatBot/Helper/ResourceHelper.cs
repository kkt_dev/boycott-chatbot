﻿using LannpyaBot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LannpyaBot.Helper
{
    public static class ResourceHelper
    {
        public static List<HeroCardObj> Cities = new List<HeroCardObj> {
            new HeroCardObj{ Id=1, Name = "Taunggyi", PhotoUrl ="https://www.pronititravel.com/wp-content/uploads/2017/10/Taunggyi-Balloon-Festival.jpg",Description ="It is the capital and largest city of Shan State, Myanmar (Burma) and lies on the Thazi-Kyaingtong road at an elevation of 4,712 feet, just north of Shwenyaung and Inle Lake within the Myelat region.",Action="🚩 I live here" },
            new HeroCardObj{ Id=2, Name = "Mawlamyaing", PhotoUrl ="https://www.brillianttriangletravelsntours.com/wp-content/uploads/2016/11/thanlwin-bridge-2-870x350.jpg",Description ="Mawlamyaing is formerly Moulmein, is the fourth largest city of Myanmar (Burma),[3][4] 300 km south east of Yangon and 70 km south of Thaton, at the mouth of Thanlwin (Salween) River.",Action="🚩 I live here" },
        };
        public static List<HeroCardObj> Cities_UN = new List<HeroCardObj> {
            new HeroCardObj{ Id=1, Name = "တောင်ကြီး", PhotoUrl ="https://www.pronititravel.com/wp-content/uploads/2017/10/Taunggyi-Balloon-Festival.jpg",Description ="It is the capital and largest city of Shan State, Myanmar (Burma) and lies on the Thazi-Kyaingtong road at an elevation of 4,712 feet, just north of Shwenyaung and Inle Lake within the Myelat region.",Action="🚩 နေထိုင်ပါတယ်" },
            new HeroCardObj{ Id=2, Name = "မော်လမြိုင်", PhotoUrl ="https://www.brillianttriangletravelsntours.com/wp-content/uploads/2016/11/thanlwin-bridge-2-870x350.jpg",Description ="Mawlamyaing is formerly Moulmein, is the fourth largest city of Myanmar (Burma),[3][4] 300 km south east of Yangon and 70 km south of Thaton, at the mouth of Thanlwin (Salween) River.",Action="🚩 နေထိုင်ပါတယ်" },
        };
        public static List<HeroCardObj> Cities_ZG = new List<HeroCardObj> {
            new HeroCardObj{ Id=1, Name ="ေတာင္ႀကီး", PhotoUrl ="https://www.pronititravel.com/wp-content/uploads/2017/10/Taunggyi-Balloon-Festival.jpg",Description ="It is the capital and largest city of Shan State, Myanmar (Burma) and lies on the Thazi-Kyaingtong road at an elevation of 4,712 feet, just north of Shwenyaung and Inle Lake within the Myelat region.",Action="🚩 ေနထိုင္ပါတယ္" },
            new HeroCardObj{ Id=2, Name = "ေမာ္လၿမိဳင္", PhotoUrl ="https://www.brillianttriangletravelsntours.com/wp-content/uploads/2016/11/thanlwin-bridge-2-870x350.jpg",Description ="Mawlamyaing is formerly Moulmein, is the fourth largest city of Myanmar (Burma),[3][4] 300 km south east of Yangon and 70 km south of Thaton, at the mouth of Thanlwin (Salween) River.",Action="🚩 ေနထိုင္ပါတယ္" },
        };
        public static List<HeroCardObj> Licenses = new List<HeroCardObj> {
            new HeroCardObj{ Id=1, Name = "Food license", PhotoUrl = ResourceHelper.foodlicense_img_url,Description ="Food license ?",Action="🤔  Learn more it", ActionUrl = "http://www.google.com" },
            new HeroCardObj{ Id=2, Name = "Danger license", PhotoUrl =ResourceHelper.dangerlicense_img_url,Description ="Danger license ?",Action="🤔 Learn more it",ActionUrl = "http://www.google.com" },
            new HeroCardObj{ Id=3, Name = "Store license", PhotoUrl =ResourceHelper.storelicense_img_url,Description ="Store license ?",Action="🤔 Learn more it",ActionUrl = "http://www.google.com" },
            new HeroCardObj{ Id=4, Name = "Hall license", PhotoUrl =ResourceHelper.halllicense_img_url,Description ="Hall license ?",Action="🤔 Learn more it",ActionUrl = "http://www.google.com" },
            new HeroCardObj{ Id=5, Name = "Hotel license", PhotoUrl =ResourceHelper.hotellicense_img_url,Description ="Hotel license ?",Action="🤔 Learn more it",ActionUrl = "http://www.google.com" },
            new HeroCardObj{ Id=6, Name = "Boarding license", PhotoUrl =ResourceHelper.boardinglicense_img_url,Description ="Boarding license ?",Action="🤔 Learn more it",ActionUrl = "http://www.google.com" },

        };
        public static List<HeroCardObj> Licenses_UN = new List<HeroCardObj> {
            new HeroCardObj{ Id=1, Name = "စားသောက်ကုန်လိုင်စင်", PhotoUrl = ResourceHelper.foodlicense_img_url,Description ="စားသောက်ကုန်လိုင်စင်ဆိုတာဘာလဲ",Action="🤔 လေ့လာမယ်", ActionUrl = "http://www.google.com" },
            new HeroCardObj{ Id=2, Name = "ဘေးအန္တရာယ်အတွက်လိုင်စင်", PhotoUrl =ResourceHelper.dangerlicense_img_url,Description ="ဘေးအရာယ်အတွက်လိုင်စင်ဆိုတာဘာလဲ",Action="🤔 လေ့လာမယ်",ActionUrl = "http://www.google.com" },
            new HeroCardObj{ Id=3, Name = "သိုလှောင်ရုံလိုင်စင်", PhotoUrl =ResourceHelper.storelicense_img_url,Description ="သိုလှောင်ရုံလိုင်စင်ဆိုတာဘာလဲ",Action="🤔 လေ့လာမယ်",ActionUrl = "http://www.google.com" },
            new HeroCardObj{ Id=4, Name = "ခန်းမလိုင်စင်", PhotoUrl =ResourceHelper.halllicense_img_url,Description ="ခန်းမလိုင်စင်ဆိုတာဘာလဲ",Action="🤔 လေ့လာမယ်",ActionUrl = "http://www.google.com" },
            new HeroCardObj{ Id=5, Name = "ပောာ်တယ်လိုင်စင်", PhotoUrl =ResourceHelper.hotellicense_img_url,Description ="ပောာ်တယ်လိုင်စင်ဆိုတာဘာလဲ",Action="🤔 လေ့လာမယ်",ActionUrl = "http://www.google.com" },
            new HeroCardObj{ Id=6, Name = "ဘုတ်လိုင်စင်", PhotoUrl =ResourceHelper.boardinglicense_img_url,Description ="ဘုတ်လိုင်စင်ဆိုတာဘာလဲ",Action="🤔 လေ့လာမယ်",ActionUrl = "http://www.google.com" },

        };
        public static List<HeroCardObj> Licenses_ZG = new List<HeroCardObj> {
            new HeroCardObj{ Id=1, Name = "စားေသာက္ကုန္လိုင္စင္", PhotoUrl = ResourceHelper.foodlicense_img_url,Description ="စားေသာက္ကုန္လိုင္စင္ဆိုတာဘာလဲ",Action="🤔 ေလ့လာမယ္", ActionUrl = "http://www.google.com" },
            new HeroCardObj{ Id=2, Name = "ေဘးအႏၲရာယ္အတြက္လိုင္စင္", PhotoUrl =ResourceHelper.dangerlicense_img_url,Description ="ေဘးအရာယ္အတြက္လိုင္စင္ဆိုတာဘာလဲ",Action="🤔 ေလ့လာမယ္",ActionUrl = "http://www.google.com" },
            new HeroCardObj{ Id=3, Name = "သိုေလွာင္႐ုံလိုင္စင္", PhotoUrl =ResourceHelper.storelicense_img_url,Description ="သိုေလွာင္႐ုံလိုင္စင္ဆိုတာဘာလဲ",Action="🤔 ေလ့လာမယ္",ActionUrl = "http://www.google.com" },
            new HeroCardObj{ Id=4, Name = "ခန္းမလိုင္စင္", PhotoUrl =ResourceHelper.halllicense_img_url,Description ="ခန္းမလိုင္စင္ဆိုတာဘာလဲ",Action="🤔 ေလ့လာမယ္",ActionUrl = "http://www.google.com" },
            new HeroCardObj{ Id=5, Name = "ေပာာ္တယ္လိုင္စင္", PhotoUrl =ResourceHelper.hotellicense_img_url,Description ="ေပာာ္တယ္လိုင္စင္ဆိုတာဘာလဲ",Action="🤔 ေလ့လာမယ္",ActionUrl = "http://www.google.com" },
            new HeroCardObj{ Id=6, Name = "ဘုတ္လိုင္စင္", PhotoUrl =ResourceHelper.boardinglicense_img_url,Description ="ဘုတ္လိုင္စင္ဆိုတာဘာလဲ",Action="🤔 ေလ့လာမယ္",ActionUrl = "http://www.google.com" },

        };
        public static List<HeroCardObj> Taxes = new List<HeroCardObj> {
            new HeroCardObj{ Id=1, Name = "Property Tax", PhotoUrl = ResourceHelper.propertytax_img_url,Description ="What is Property Tax? How to pay tax and many more.",Action="🤔 Get Information", ActionUrl = "http://www.google.com"},
            new HeroCardObj{ Id=2, Name = "Wheel Tax", PhotoUrl =ResourceHelper.wheeltax_img_url,Description ="What is Wheel Tax? How to pay tax and many more.",Action="🤔 Get Information",ActionUrl = "http://www.google.com"},
            new HeroCardObj{ Id=3, Name = "Water Tax", PhotoUrl =ResourceHelper.watertax_img_url,Description ="What is Water Tax? How to pay tax and many more.",Action="🤔 Get Information",ActionUrl = "http://www.google.com"},
            new HeroCardObj{ Id=4, Name = "Signboard Tax", PhotoUrl =ResourceHelper.signboardtax_img_url,Description ="What is Signboard Tax? How to pay tax and many more.",Action="🤔 Get Information",ActionUrl = "http://www.google.com"},
            new HeroCardObj{ Id=5, Name = "Market Tax", PhotoUrl =ResourceHelper.markettax_img_url,Description ="What is Market Tax? How to pay tax and many more.",Action="🤔 Get Information",ActionUrl = "http://www.google.com"},
            new HeroCardObj{ Id=6, Name = "Business license Tax", PhotoUrl =ResourceHelper.businesslicense_img_url,Description ="What is Business license Tax? How to pay tax and many more.",Action="🤔 Get Information",ActionUrl = "http://www.google.com"},

        };
        public static List<HeroCardObj> Taxes_UN = new List<HeroCardObj> {
            new HeroCardObj{ Id=1, Name = "ပစ္စည်းခွန်", PhotoUrl = ResourceHelper.propertytax_img_url,Description ="ပစ္စည်းအခွန်ဆိုတာဘာလဲ။ ဘယ်လိုပေးရမလဲ။ ဘယ်လောက်ပေးရမလဲ။",Action="🤔 သိလိုသည်", ActionUrl = "http://www.google.com"},
            new HeroCardObj{ Id=2, Name = "ဘီးခွန်", PhotoUrl =ResourceHelper.wheeltax_img_url,Description ="ဘီးအခွန်ဆိုတာဘာလဲ။ ဘယ်လိုပေးရမလဲ။ ဘယ်လောက်ပေးရမလဲ။ ",Action="🤔 သိလိုသည်",ActionUrl = "http://www.google.com"},
            new HeroCardObj{ Id=3, Name = "ရေခွန်", PhotoUrl =ResourceHelper.watertax_img_url,Description ="ရေအခွန်ဆိုတာဘာလဲ။ဘယ်လိုပေးရမလဲ။ ဘယ်လောက်ပေးရမလဲ။",Action="🤔 သိလိုသည်",ActionUrl = "http://www.google.com"},
            new HeroCardObj{ Id=4, Name = "ဆိုင်းဘုတ်ခွန်", PhotoUrl =ResourceHelper.signboardtax_img_url,Description ="ဆိုင်းဘုတ်အခွန်ဆိုတာဘာလဲ။ ဘယ်လိုပေးရမလဲ။ ဘယ်လောက်ပေးရမလဲ။",Action="🤔 သိလိုသည်",ActionUrl = "http://www.google.com"},
            new HeroCardObj{ Id=5, Name = "စျေးရောင်းဝယ်ခြင်းခွန်", PhotoUrl =ResourceHelper.markettax_img_url,Description ="စျေးရောင်းဝယ်ခြင်းအခွန်ဆိုတာဘာလဲ။ ဘယ်လိုပေးရမလဲ။ ဘယ်လောက်ပေးရမလဲ။",Action="🤔 သိလိုသည်",ActionUrl = "http://www.google.com"},
            new HeroCardObj{ Id=6, Name = "စီးပွားရေးလိုင်စင်ခွန်", PhotoUrl =ResourceHelper.businesslicense_img_url,Description ="စီးပွားရေးလိုင်စင်အခွန်ဆိုတာဘာလဲ။ ဘယ်လိုပေးရမလဲ။ ဘယ်လောက်ပေးရမလဲ။",Action="🤔 သိလိုသည်",ActionUrl = "http://www.google.com"},

        };
        public static List<HeroCardObj> Taxes_ZG = new List<HeroCardObj> {
            new HeroCardObj{ Id=1, Name = "ပစၥည္းအခြန္", PhotoUrl = ResourceHelper.propertytax_img_url,Description ="ပစၥည္းအခြန္ဆိုတာဘာလဲ။ ဘယ္လိုေပးရမလဲ။ ဘယ္ေလာက္ေပးရမလဲ။",Action="🤔 သိလိုသည္", ActionUrl = "http://www.google.com"},
            new HeroCardObj{ Id=2, Name = "ဘီးအခြန္", PhotoUrl =ResourceHelper.wheeltax_img_url,Description ="ဘီးအခြန္ဆိုတာဘာလဲ။ ဘယ္လိုေပးရမလဲ။ ဘယ္ေလာက္ေပးရမလဲ။ ",Action="🤔 သိလိုသည္",ActionUrl = "http://www.google.com"},
            new HeroCardObj{ Id=3, Name = "ေရအခြန္", PhotoUrl =ResourceHelper.watertax_img_url,Description ="ေရအခြန္ဆိုတာဘာလဲ။ဘယ္လိုေပးရမလဲ။ ဘယ္ေလာက္ေပးရမလဲ။",Action="🤔 သိလိုသည္",ActionUrl = "http://www.google.com"},
            new HeroCardObj{ Id=4, Name = "ဆိုင္းဘုတ္အခြန္", PhotoUrl =ResourceHelper.signboardtax_img_url,Description ="ဆိုင္းဘုတ္အခြန္ဆိုတာဘာလဲ။ ဘယ္လိုေပးရမလဲ။ ဘယ္ေလာက္ေပးရမလဲ။",Action="🤔 သိလိုသည္",ActionUrl = "http://www.google.com"},
            new HeroCardObj{ Id=5, Name = "ေစ်းေရာင္းဝယ္ျခင္း အခြန္", PhotoUrl =ResourceHelper.markettax_img_url,Description ="ေစ်းေရာင္းဝယ္ျခင္းအခြန္ဆိုတာဘာလဲ။ ဘယ္လိုေပးရမလဲ။ ဘယ္ေလာက္ေပးရမလဲ။",Action="🤔 သိလိုသည္",ActionUrl = "http://www.google.com"},
            new HeroCardObj{ Id=6, Name = "စီးပြားေရးလိုင္စင္ အခြန္", PhotoUrl =ResourceHelper.businesslicense_img_url,Description ="စီးပြားေရးလိုင္စင္အခြန္ဆိုတာဘာလဲ။ ဘယ္လိုေပးရမလဲ။ ဘယ္ေလာက္ေပးရမလဲ။",Action="🤔 သိလိုသည္",ActionUrl = "http://www.google.com"},


        };
        public static string foodlicense_img_url
        {
            get { return "https://kktstroage.azureedge.net/yammo/myotaw/propertytax.png"; }
        }
        public static string dangerlicense_img_url
        {
            get { return "https://kktstroage.azureedge.net/yammo/myotaw/dangerlicense.png"; }
        }
        public static string hotellicense_img_url
        {
            get { return "https://kktstroage.azureedge.net/yammo/myotaw/hotellicense.png"; }
        }
        public static string storelicense_img_url
        {
            get { return "https://kktstroage.azureedge.net/yammo/myotaw/storelicense.png"; }
        }
        public static string halllicense_img_url
        {
            get { return "https://kktstroage.azureedge.net/yammo/myotaw/halllicense.png"; }
        }
        public static string boardinglicense_img_url
        {
            get { return "https://kktstroage.azureedge.net/yammo/myotaw/boardinglicense.png"; }
        }
        public static List<HeroCardObj> TaxExpenditures = new List<HeroCardObj> {
            new HeroCardObj{ Id=1, Name = "အခွန် ကုန်ကျစရိတ်", PhotoUrl = ResourceHelper.tax_img_url,Description ="သင့်မြို့တော်ကို ကူညီပေးနေတဲ့ အခွန်က ဘာလဲ။ ",Action="🤔 လေ့လာမယ်", ActionUrl = "http://www.google.com" },
       
        };
        public static bool isStart_words(this string value)
        {
            return new string[] { "🤔 Get started", "Get started", "Start over" }.Contains(value.Trim());
        }
        public static bool isStart_words_UN(this string value)
        {
            return new string[] { "🤔 ပြန်စမည်", "🤔 စတင်မည်" }.Contains(value.Trim());
        }
        public static bool isStart_words_ZG(this string value)
        {
            return new string[] { "🤔 ျပန္စမည္", "🤔 စတင္မည္" }.Contains(value.Trim());
        }
        public static bool isWelcome_words(this string value)
        {
            return new string[] { "hi", "Hi", "get_started_payload", "GET_STARTED_PAYLOAD", "start over", "get started", "start", "hello", "🤔 start over", "ဟိုင်း", "ဟဲ့လို", "get_started_payload_un", "🤔 ပြန်စမည်", "🤔 ျပန္စမည္", "ဟိုင္း", "ဟ့ဲလို", "get_started_payload_zg", "🤔 ျပန္စမည္", "MENU", "Menu","menu" }.Contains(value.Trim().ToLower());
        }

        public static List<string> buildingTypes = new List<string> { "Pyin", "Thout Moe Htayankar", "Thetkalmoe Htayankar", "Nan Kact", "Tike" };
        public static List<StoreyInfo> Stories = new List<StoreyInfo>
        {
            new StoreyInfo{BuildingType = "Pyin", Storey = 1, StoreyName = "One storey" },
            new StoreyInfo{BuildingType = "Pyin", Storey = 2, StoreyName = "Two storey" },
            new StoreyInfo{BuildingType = "Thout Moe Htayankar", Storey = 1, StoreyName = "One storey" },
            new StoreyInfo{BuildingType = "Thout Moe Htayankar", Storey = 2, StoreyName = "Two storey" },
            new StoreyInfo{BuildingType = "Thetkalmoe Htayankar", Storey = 1, StoreyName = "One storey" },
            new StoreyInfo{BuildingType = "Nan Kact", Storey = 1, StoreyName = "One storey" },
            new StoreyInfo{BuildingType = "Nan Kact", Storey = 2, StoreyName = "Two storey" },
            new StoreyInfo{BuildingType = "Tike", Storey = 1, StoreyName = "One storey" },
            new StoreyInfo{BuildingType = "Tike", Storey = 2, StoreyName = "Two storey" },
            new StoreyInfo{BuildingType = "Tike", Storey = 3, StoreyName = "Three storey" },
            new StoreyInfo{BuildingType = "Tike", Storey = 4, StoreyName = "Four storey" },
            new StoreyInfo{BuildingType = "Tike", Storey = 5, StoreyName = "Five storey" },
            new StoreyInfo{BuildingType = "Tike", Storey = 6, StoreyName = "Six storey" },

        };
        public static string propertytax_img_url
        {
            get { return "https://kktstroage.azureedge.net/yammo/myotaw/propertytax.png"; }
        }
        public static string wheeltax_img_url
        {
            get { return "https://kktstroage.azureedge.net/yammo/myotaw/wheeltax.png"; }
        }
        public static string watertax_img_url
        {
            get { return "https://kktstroage.azureedge.net/yammo/myotaw/watertax.png"; }
        }
        public static string signboardtax_img_url
        {
            get { return "https://kktstroage.azureedge.net/yammo/myotaw/signboardtax.png"; }
        }
        public static string markettax_img_url
        {
            get { return "https://kktstroage.azureedge.net/yammo/myotaw/markettax.png"; }
        }
        public static string businesslicense_img_url
        {
            get { return "https://kktstroage.azureedge.net/yammo/myotaw/businesslicensetax.png"; }
        }
        public static string mpu_img_url
        {
            get { return "https://kktstroage.azureedge.net/yammo/myotaw/mpu_logo.png"; }
        }
        public static string wavemoney_img_url
        {
            get { return "https://kktstroage.azureedge.net/yammo/myotaw/wavemoney_logo.png"; }
        }
        public static bool isFeedback_words(this string value)
        {
            return new string[] { "suggestion", "complaint", "feedback", "give feedback", "problem" }.Contains(value.Trim().ToLower());
        }
        
        public static string tax_img_url
        {

            get { return "https://kktstroage.azureedge.net/yammo/myotaw/tax.jpg"; }
        }
        public static string license_img_url
        {
            get { return "https://kktstroage.azureedge.net/yammo/myotaw/license.jpg"; }
        }
        public static string taxexpenditure_img_url
        {
            get { return "https://kktstroage.azureedge.net/yammo/myotaw/tax.jpg"; }
        }
        public static string getapp_img_url
        {
            get { return "https://kktstroage.azureedge.net/yammo/myotaw/getapp.png"; }
        }
        public static string calculatetax_img_url
        {
            get { return "https://kktstroage.azureedge.net/yammo/myotaw/caltax.png"; }
        }
        public static string prepaid_img_url
        {
            get { return "https://portalvhdslvb28rs1c3tmc.blob.core.windows.net/myotaw/PaywithPrepaid.jpg"; }
        }

        public static string TextToReply(string Name = null)
        {
            string replytext = null;
            if (Name== "Detail_Biz") { replytext = "လုပ်ငန်းတစ်ခုစတင်ဆောင်ရွက်မည်ဆိုပါက ဆာင်ရွက်မည့်လုပ်ငန်းအမျိုးအစားပေါ်မူတည်ပြီး စည်ပင်မှ မည်သည့်လိုင်စင်အောက်ပြုလုပ်ခွင့်ပြုကြောင်း သတ်မှတ်ပေးပါမည်။ ထို့နောက် လိုင်စင်အလိုက် လိုအပ်သည့် အထောက်အထားများဖြင့်လျှောက်ထားရမည်။ စည်ပင်မှ လုပ်ငန်းအားကွင်းဆင်းစစ်ဆေးပြီး သတ်မှတ်ချက်များ ကိုက်ညီပါက လုပ်ငန်းလုပ်ကိုင်ခွင့်လိုင်စင်ထုတ်ပေးမည်ဖြစ်သည်။ ထိုလိုင်စင်သည် တစ်နှစ်သက်တမ်းရှိပြီး နှစ်စဉ် ပန်လည်သက်တမ်းတိုးရမည်ဖြစ်သည်။"; }
            else if (Name== "Detail_CalBiz") { replytext = "လုပ်ငန်းလိုင်စင်အမျိုးအစား၊ လုပ်ငန်းအမျိုးအစား၊ ဧရိယာနှင့် အတန်းအစား ပေါ်မူတည်ပြီး တွက်ချက် ကောက်ခံပါသည်။"; }
            else if (Name == "Detail_PayBiz") { replytext = "စည်ပင်သာယာရုံးတွင် သွားရောက်ပေးဆောင်ပြီး သက်တမ်းတိုးလိုင်စင်ကတ်ပါ ထုတ်ယူရမည်ဖြစ်သည်။"; }
            else if (Name == "Detail_Water") { replytext = "စည်ပင်မှရေပေးဝေရေးတွက် ကောက်ခံသောအခွန်ဖြစ်ပါသည်။ လစဉ် စည်ပင်ဝန်ထမ်းများမှ မီတာလာဖတ်ပါမည်။ ထို့နောက် ပြေစာဖြင့် ရေခွန်ကို အိမ်တိုင်ရာ ရောက်လိုက်လံကောက်ခံပါမည်။ မကြာမီတွင် စမတ်မီတာများအစားထိုးသုံးစွဲလာပါက စည်ပင်မှဝန်ထမ်းများမှ ရေမီတာဖတ်ခြင်း၊ လာရောက်ကောက်ခံခြင်း ရှိတော့မည်မဟုတ်ပါ။ ရေသုံးစွဲသည့်မီတာယူနစ်များအလိုအလို အလျောက်တက်နေပြီး စည်ပင်မှလည်း အချိန်နှင့်တပြေးညီ မြင်နေရမည်ဖြစ်သည်။ သုံးစွဲသူ၏ ကြိုတင်ဝယ်ယူ ထားသော အကောင့်မှ လစဉ်ရေခွန်ကျသင့်ငွေကို ဖြတ်တောက်သွားမည့် စနစ်ဖြစ်သည်။"; }
            else if (Name == "Detail_CalWater") { replytext = "ရေမီတာမတပ်ဆင်ထားပါက ပုံသေနှုန်းဖြင့် ပေးဆောင်ရမည်ဖြစ်ပါသည်။ ရေမီတာတပ်ဆင်ထားပါက လစဉ် သုံးစွဲယူနစ်အလိုက် ရေခွန်ပေးဆောင်ရမည် ဖြစ်သည်။"; }
            else if (Name == "Detail_PayWater") { replytext = "လက်ရှိစနစ်တွင် စည်ပင်ဝန်ထမ်းမှ ပြေစာဖြင့် အိမ်တိုင်ရာရောက် လာရောက်ကောက်ခံမည်ဖြစ်သည်။ နောင်တွင်မူ သုံးစွဲသူ၏ ကြိုတင်ဝယ်ယူ ထားသော အကောင့်မှ လစဉ်ရေခွန်ကျသင့်ငွေကို ဖြတ်တောက်သွား မည့် စနစ်ကျင့်သုံးမည်ဖြစ်သည်။"; }
            else if (Name == "Detail_Market") { replytext = "ဈေးများအတွင်း ဆိုင်ခန်းများမှပေးဆောင်ရသော အခွန်အခဖြစ်သည်။"; }
            else if (Name == "Detail_CalMarket") { replytext = "ဆိုင်ခန်းပေါ်မူတည်ပြီး လစဉ်ကြေးသတ်မှတ်ကောက်ခံပါသည်။"; }
            else if (Name == "Detail_PayMarket") { replytext = "စည်ပင်ဝန်ထမ်းများမှ လစဉ် လာရောက်ကောက်ခံပါမည်။"; }
            else if (Name == "Detail_Signboard") { replytext = "မြို့တွင်းစိုက်ထူထားသော ကြော်ငြာဆိုင်းဘုတ်ကြီးများ၊ စီးပွားဖြစ် လမ်းညွှန်ဆိုင်းဘုတ်များနှင့် မိမိနေအိမ်တွင် စီးပွားဖြစ်ထောင်ထားသော ဆိုင်းဘုတ်များအတွက် ကောက်ခံသော အခွန်ငွေဖြစ်သည်။"; }
            else if (Name == "Detail_CalSignboard") { replytext = "ဆိုင်းဘုတ်အမျိုးအစား၊ ဧရိယာ၊ မျက်နှာစာနှင့် အတန်းအစား ပေါ်မူတည်ပြီး တွက်ချက်ကောက်ခံပါသည်။"; }
            else if (Name == "Detail_PaySignboard") { replytext = "တစ်နှစ်လျှင်တစ်ကြိမ် စည်ပင်ရုံးတွင် လာရောက်ပေးဆောင်ရပါသည်။"; }
            else if (Name == "Detail_Wheel") { replytext = "မြို့တွင်းရှိ ကား၊ ဆိုင်ကယ် မှစသည် ဆိုက်ကား၊ နွားလှည်း စသည့် သယ်ယူပို့ဆောင်ရေးယာဉ်များ အထိ ကောက်ခံသော အခွန်အခဖြစ်သည်။"; }
            else if (Name == "Detail_CalWheel") { replytext = "ယာဉ်အမျိုးအစားနှင့် တန်ချိန်ပေါ်မူတည်ပြီး ကောက်ခံပါသည်။"; }
            else if (Name == "Detail_PayWheel") { replytext = "တစ်နှစ်လျှင်တစ်ကြိမ် စည်ပင်ရုံးတွင်သွားရောက်ပေးဆောင်ခြင်း (သို့) စည်ပင်မှတွေ့ဆုံကောက်ခံခြင်း ဖြင့်ပေးဆောင်ရပါသည်။"; }
            return replytext;
        }
    }
}