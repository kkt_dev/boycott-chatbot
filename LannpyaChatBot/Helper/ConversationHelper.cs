﻿using Microsoft.Bot.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LannpyaBot.Helper
{
    public static class ConversationHelper
    {
        public static Activity ResponseTyping(this Activity msg)
        {
            var reply = msg.CreateReply(String.Empty);
            reply.Type = ActivityTypes.Typing;
            return reply;
        }
    }
}